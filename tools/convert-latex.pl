#!/usr/bin/perl -pi.bak

s%\\section\s*{([^\}]*)}%  <sect1>\n    <title>$1</title>%g;
s%\\subsection\s*{([^\}]*)}%  <sect2>\n    <title>$1</title>%g;
s%\\chapter\s*{([^\}]*)}%<chapter>\n  <title>$1</title>%g;
s%\\const\s*{([^\}]*)}%<symbol>$1</symbol>%g;
s%\\argm\s*{([^\}]*)}%<parameter>$1</parameter>%g;
s%\\func\s*{([^\}]*)}%<function>$1\(\)</function>%g;
s%\\emph\s*{([^\}]*)}%<firstterm>$1</firstterm>%g;
s%\\gtk\\?%&gtk;%g;
s%\\signal\s*{([^\}]*)}{\s*([^\}]*)}%<literal>$1::$2</literal>%g;
