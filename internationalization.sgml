<chapter id="i18n">
  <title>Text and Internationalization</title>
  <para>
    Working with text using a widget such as a <classname>GtkLabel</classname>
    or a <classname>GtkEntry</classname> is simple. We can set the
    contents of the widget from a string, and, in the case of entry,
    retrieve the contents back as a string. We don't have to worry
    about how to interpret the string as text, how to wrap the lines
    of the text, how editing should work, and how to take keystrokes
    from the keyboard and turn them into text. However, if we want
    to use text in a more customized fashion, for instance, if we
    want to draw text strings to a <classname>GdkWindow</classname> or
    <classname>GdkPixmap</classname>, then we have to start paying
    attention to these issues.
  </para>
  <para>
    Whenever we start looking at text in detail, we have to worry
    about <firstterm>internationalization</firstterm>; what works for
    text handling in English does not necessarily work in other
    languages. Internationalization is the process of making an
    application able to work with many different languages. There is
    an accompanying process of localization, which is displaying an
    application's text (menus, help text, etc.) in the user's
    language.  (These two terms are often abbreviated I18N and L10N,
    with the numbers referring to the number of characters between the
    initial and final letters.)
  </para>
  <para>
    A number of different aspects of internationalization can
    be identified. First, there must be a mechanism for users
    to input text in the desired language. This may be a matter
    of using an appropriate keyboard layout, or it may involve
    complicated <firstterm>input methods</firstterm> for translating
    the users input into the final text. Second, manipulating
    text must be done in a way that works for text in different
    languages. Finally it must be possible to display the
    text on the screen.
  </para>
  <para>
    While the obvious difference between different writing systems is
    that different writing systems use different sets of characters,
    this is actually only the simplest, and perhaps the easiest
    difference to deal with. To give you some idea of the other
    difference, we'll list a few of the assumptions that a program
    that was written assuming that all users were English speakers
    in the United States might make that wouldn't work for other
    languages, other writing systems, and other locations:
    <itemizedlist>
      <listitem>
	<para>
	  Each byte in the input text corresponds to a single
	  character. Some scripts have many more distinct characters
	  than the roman alphabet, and require more than one
	  byte for each character. When storing such scripts
	  in memory, frequently different number of bytes are
	  used for different characters. &gtk;, we'll see
	  later in this chapter, stores text in the UTF-8 encoding,
	  which can use between 1 and 4 bytes for each character,
	  allowing the characters for all languages in the world
	  to be stored.
	</para>
      </listitem>
      <listitem>
	<para>
	  Each character in the input text corresponds to a single
	  symbol drawn on the screen from the current font. In
	  fact, there is no one-to-one correspondance between
	  characters and symbols or <firstterm>glyphs</firstterm>
	  in a font. In some scripts, such as Arabic, a character
	  is drawn with one shape at the beginning of a word,
	  and a different shape at the middle of a word, or
	  at the end of a word. In other scripts, such as the
	  Devanagari script used for writing Hindi, multiple
	  characters are combined together in writing to form
	  a single compound glyph.
	</para>
      </listitem>
      <listitem>
	<para>
	  The characters in the input text are drawn on the screen
	  from left-to-right. A number of scripts, in particular the
	  Hebrew and Arabic scripts are written from right to
	  left. Because text in these languages is frequently a mix of
	  right-to-left text, and left-to-right text (words in foreign
	  languages, numbers), the ordering of characters in the
	  output (visual order) can be quite different than the
	  ordering in the input text, which is written in logical
	  order (the order in which the text is read or written.)
	</para>
      </listitem>
      <listitem>
	<para>
	  Words are separated by whitespace. Quite a few languages are
	  in fact, written without spaces between words. For some such
	  languages, such as Japanese, the correct algorithms for
	  moving by ``words'' is determined algorithmically. In
	  other languages, such as Thai, dictionary-based lookups
	  are required to break an input stream into words. Similarly,
	  determination of other types of boundaries in an input
	  stream also depends on the script: this includes possible
	  places to break a line, and even the possible places in
	  the text to place a cursor.
	</para>
      </listitem>
      <listitem>
	<para>
	  The keyboard of the user will have the same layout as the
	  keyboard of the programmer. Programmers who expect
	  the '+' key to be the shifted version of the '=' key
	  will find that isn't even true for most European keyboards.
	  Further problems can show up with when dealing with
	  the keyboard layouts used to enter languages such as
	  Russian or Arabic, which have multiple modes - one mode
	  mode for entering Roman characters, and one mode for
	  entering the characters in the user's language.
	</para>
      </listitem>
      <listitem>
	<para>
	  Each character in the language corresponds to a key on
	  the key on the keyboard. In many writing systems, multiple
	  keystrokes are needed to enter a character. A simple
	  example of this is the ``dead accent'' method of entering
	  an accented character. First a ``dead'' key with the accent
	  printed on it is pressed. This produces no immediate effect,
	  and then the key for the base character is pressed, at
	  which point, a single character for the combination of
	  the base character and the accent is entered into the
	  text stream. A more complex example is the handling of
	  input for East Asian languages such as Chinese and
	  Japanese. The user enters the pronunciation of phrase
	  and at the press of a key is shown various possible
	  characters with those pronunciation to choose between.
	</para>
      </listitem>
    </itemizedlist>
  </para>
  <para>
    The natural reaction to the above list of complexities might be to
    throw up ones hands in despair and forget about
    internationalization --- one could easily spend years working on
    handling all these issues. Luckily, most of the work has already
    been done for you. If you are simply using the standard &gtk;
    widgets, then all these issues and more will be taken care of for
    you for text editing and display. If you need to write customized
    text handling routines, you still don't need to worry about all
    the low-level details: the &gtk; libraries contain a number of
    facilities to help with text handling and internationalization
    at a high level.
    <itemizedlist>
      <listitem>
	<para>
	  &glib; contains routines for manipulating Unicode text
	  and converting between different text encodings.
	</para>
      </listitem>
      <listitem>
	<para>
	  The Pango library contains routines for laying out and
	  rendering text in all the world's languages, including
	  facilities for finding word and line breaks in an
	  appropriately internationalized way.
	</para>
      </listitem>
      <listitem>
	<para>
	  The <classname>GtkIMContext</classname> class provides
	  a facility for converting keyboard input into text in a
	  way appropriate for the user's language.
	</para>
      </listitem>
    </itemizedlist>
    These are the facilities that the &gtk; widgets use internally
    to implement internationalized text handling.
  </para>
  <sect1>
    <title>Manipulating Strings</title>
    <para>
      If your native language is a Western-European language, then
      you probably are used to representing each character as a single
      byte. However, this model is not sufficient for most of the
      worlds languages. The languages of East Asia have thousands
      of characters that must be represented. Even if you only are
      dealing with languages with small character sets, as soon
      as you want to handle multiple languages at once; for instance,
      to handle both French and Russian, then you need more than
      256 characters.
    </para>
    <para>
      &gtk; encodes all strings using the ISO-10646 character
      set. ISO-10646 is an international standard which is basically
      as same as the <firstterm>Unicode</firstterm> standard which is
      defined by the Unicode Consortium .  Since Unicode is a more
      pronouncable name and memorable name, it is the more frequently
      used name for the two standards. The type that &gtk; uses to
      represent a single character is <literal>gunichar</literal>
      type, which is a 32-bit wide unsigned character.
    </para>
    <para>
      Now that we know how we represent a single character, how do we
      go about representing a string of characters? The obvious thing
      to do would be to use an array of <literal>gunichar</literal>.
      That is, we would represent strings as a <literal>gunichar
      *</literal>, instead of a <literal>gchar *</literal>. However,
      this solution is very inefficient in using space, since each
      character takes up 4 bytes. An ASCII string would grow to 4
      times its original size.  A more common variant of this approach
      is to use a 16 bit wide type for each character, so the penalty
      is only 2 times, not 4 times. The first 16-bits of the ISO-10646
      encoding space include all the characters used commonly in
      today's living languages, so this solution works well for most
      texts. This approach, however, does not leave room for
      convenient future expansion, and also means that all interfaces
      need to be converted over from taking strings as <literal>gchar
      *</literal> to taking strings as <literal>gunichar *</literal>.
    </para>
    <para>
      For these reasons, &gtk; takes a different approach to
      representing strings. &gtk; represents strings using the
      <firstterm>UTF-8</firstterm> encoding. This encoding uses
      a variable number of bytes to represent each character.
      The characters with values less than 128 in Unicode (which
      are simply the ASCII characters) are represented as
      single bytes. Characters with higher values are represented
      with multiple bytes. The first byte includes information
      about the total number of bytes in the character, and some
      of the bits of the character. Each subsequent byte adds
      more bits. This encoding is quite compact: ASCII text
      takes up the same size in UTF-8 as it if encoded directly;
      most other alphabetic scripts take up two bytes per character,
      and the ideographic scripts of East Asia take up
      three bytes per character.
    </para>
    <para>
      Since a UTF-8 string is just represented as a <literal>char *</literal>,
      and since ASCII characters are just literally represented,
      many algorithms that you would use to manipulate strings
      work unchanged.
    </para>
    <para>
      There are a few things of which you do need to be careful:
    </para>
    <itemizedlist>
      <listitem>
	<para>
	  One character is not always one byte. Code that iterates
	  through a string a byte time may need to be adapted to read
	  through a string a character at a time.
	</para>
      </listitem>
      <listitem>
	<para>
	  There can be invalid strings. Any string that you retrieve
	  from a &gtk; widget will be a valid UTF-8 string, but you
	  should always be ready to deal with invalid UTF-8 data if
	  you read a string from elsewhere.  &glib contains a function:
      <!-- begin_proto="gunicode.h#g_utf8_validate" -->
<funcsynopsis>
  <funcdef>gboolean <function>g_utf8_validate</function></funcdef>
  <paramdef>const gchar *<parameter>str</parameter></paramdef>
  <paramdef>gssize <parameter>max_len</parameter></paramdef>
  <paramdef>const gchar **<parameter>end</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->
	  which checks see if a string is valid UTF-8.
	</para>
      </listitem>
      <listitem>
	<para>
	  You have to be willing to deal with incomplete input. For
	  instance, reading in a file in 1024-byte chunks and
	  appending it to a text widget does not work, since the
	  1024'th byte might not be at the end of a character. In
	  general, you should use functions like
	  <function>g_io_channel_read_line()</function> that handle
	  such incomplete input for you.
	</para>
      </listitem>
    </itemizedlist>
  </sect1>
  <sect1>
    <title>Converting between encodings</title>
    <para>
      &glib; also provides functions for conversion between different
      encodings. There are specialized functions for converting
      between the more common encodings: &gtk;'s standard encoding,
      UTF-8, the encoding for filenames on the filesystem, the
      encoding of the current locale:
      
      <!-- begin_proto="gconvert.h#g_filename_to_utf8" -->
<funcsynopsis>
  <funcdef>gchar *<function>g_filename_to_utf8</function></funcdef>
  <paramdef>const gchar *<parameter>opsysstring</parameter></paramdef>
  <paramdef>gssize <parameter>len</parameter></paramdef>
  <paramdef>gsize *<parameter>bytes_read</parameter></paramdef>
  <paramdef>gsize *<parameter>bytes_written</parameter></paramdef>
  <paramdef>GError **<parameter>error</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->
      <!-- begin_proto="gconvert.h#g_filename_from_utf8" -->
<funcsynopsis>
  <funcdef>gchar *<function>g_filename_from_utf8</function></funcdef>
  <paramdef>const gchar *<parameter>utf8string</parameter></paramdef>
  <paramdef>gssize <parameter>len</parameter></paramdef>
  <paramdef>gsize *<parameter>bytes_read</parameter></paramdef>
  <paramdef>gsize *<parameter>bytes_written</parameter></paramdef>
  <paramdef>GError **<parameter>error</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->
      <!-- begin_proto="gconvert.h#g_locale_to_utf8" -->
<funcsynopsis>
  <funcdef>gchar *<function>g_locale_to_utf8</function></funcdef>
  <paramdef>const gchar *<parameter>opsysstring</parameter></paramdef>
  <paramdef>gssize <parameter>len</parameter></paramdef>
  <paramdef>gsize *<parameter>bytes_read</parameter></paramdef>
  <paramdef>gsize *<parameter>bytes_written</parameter></paramdef>
  <paramdef>GError **<parameter>error</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->
      <!-- begin_proto="gconvert.h#g_locale_from_utf8" -->
<funcsynopsis>
  <funcdef>gchar *<function>g_locale_from_utf8</function></funcdef>
  <paramdef>const gchar *<parameter>utf8string</parameter></paramdef>
  <paramdef>gssize <parameter>len</parameter></paramdef>
  <paramdef>gsize *<parameter>bytes_read</parameter></paramdef>
  <paramdef>gsize *<parameter>bytes_written</parameter></paramdef>
  <paramdef>GError **<parameter>error</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      There is also a more general function for converting between
      any two locales, specified by name.

      <!-- begin_proto="gconvert.h#g_convert" -->
<funcsynopsis>
  <funcdef>gchar *<function>g_convert</function></funcdef>
  <paramdef>const gchar *<parameter>str</parameter></paramdef>
  <paramdef>gssize <parameter>len</parameter></paramdef>
  <paramdef>const gchar *<parameter>to_codeset</parameter></paramdef>
  <paramdef>const gchar *<parameter>from_codeset</parameter></paramdef>
  <paramdef>gsize *<parameter>bytes_read</parameter></paramdef>
  <paramdef>gsize *<parameter>bytes_written</parameter></paramdef>
  <paramdef>GError **<parameter>error</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      When using any of these functions, you should be careful to
      check for errors: errors frequently occur because the input is not
      valid in the source encoding, or the characters in the input
      are not representable in the the destination. When an error occurs,
      the conversion function returns <symbol>NULL</symbol> and the
      error is stored in the location passed in in the <parameter>error</parameter>
      parameter.
    </para>
    <para>
      The functions above provide for conversions of single strings. When
      converting an entire file as you read it in, it is more efficient
      and convenient to use the conversion functionality provided
      by <structname>GIOChannel</structname>. The encoding for a
      <structname>GIOChannel</structname> can be set with
      <function>g_io_channel_set_encoding()</function> <!-- FIXME: Check this -->.
      When reading date in from the channel, data will be converted
      from the source encoding to UTF-8. When writing to the channel,
      the data will be converted from UTF-8 to the destination encoding.
    </para>
  </sect1>
  <sect1>
    <title>Handling Input</title>
    <para>
      The writing systems of East Asia (often known as CJK,
      for China, Japan and Korea) include thousands of
      characters. Since keyboards with that many keys would be
      impractical, the user generally enters a pronunciation, then
      picks among characters with that pronunciation. This translation
      is handled by an <firstterm>input method</firstterm>.
      Input methods in &gtk; can take various forms. A simple
      input method for handling compose sequences for European
      characters is built into &gtk;, other input methods
      are accessed with loadable modules. Typically, when
      a complicated input method is needed, as for Asian languages,
      the majority of the input method is implemented as a separate
      program, and the module loaded by &gtk; simply communicates
      with the external program.
    </para>
    <para>
      While there is considerable flexibility in how input methods in &gtk;,
      most existing input methods for X are written to communicate via
      the X Input Method protocol, so &gtk; contains support for
      this system. Generally if the user has an input method for
      their language installed on the system, they will be able
      to use with &gtk;.
    </para>
    <para>
      A typical application will not have to communicate directly with
      input methods. The standard editing widgets in &gtk; already
      contain support for input methods, and will accept in any
      language without intervention from the programmer.
    </para>
  </sect1>
  <sect1>
    <title>Displaying Output</title>
    <para>
      One of the unusual features of &gtk; is its very complete
      support for internationalized output. The underlying support
      for text display in &gtk; comes from the &pango; library.
      We've seen some usage of &pango; already when specifying
      fonts. 
    </para>
    <para>
      The most commonly used object in &pango; is the <structname>PangoLayout</structname>
      object. A <structname>PangoLayout</structname> object provides
      operations for laying out and rendering a piece of text.
      These operations include:
      <simplelist>
	<member>Wrapping the text into lines at a given width</member>
	<member>Translating from (x,y) position to a character location in the text</member>
	<member>Translating from character location to a (x,y) location</member>
	<member>Interpreting cursor motion with the text</member>
	<member>Rendering the text on the screen.</member>
      </simplelist>
      <structname>PangoLayout</structname> objects are used internally
      in every widget inside &gtk; that deals with text. A <classname>GtkLabel</classname>
      widget simply creates a single <structname>PangoLayout</structname>
      from its string and draws that to the screen. The text in
      an entry widget is drawn using a <structname>PangoLayout</structname>,
      and <structname>PangoLayout</structname> functions are used to
      handle cursor movement and selection as well. The multiline
      <classname>GtkTextView</classname> widget again uses a
      <structname>PangoLayout</structname> for each paragraph.
    </para>
  </sect1>
  <sect1>
    <title>Rearranging the interface for right-to-left languages</title>
    <para>
      The languages that &gtk; can handle include languages that
      are written from right-to-left instead of from left-to-right.
      Properly displaying an interface for such a language requires
      not only translating the strings of the interface, but also
      rearranging the interface to match. For a left-to-right language,
      we typically label an entry widget with a label widget on
      its left. For a right-to-left language, the label widget
      should go on the right.
    </para>
    <para>
      The layout mechanisms in &gtk; are very suited to automatically
      handling such situations, since widgets are laid out logically,
      instead of specifying there positions physically. &gtk;
      manages to rearrange most interfaces properly without intervention
      from the application programmer. When child widgets are packed
      into a <classname>GtkHBox</classname> widget, then for
      a left-to-right language, the packing procedes from left-to-right,
      and for a right-to-left language, the packing procedes from
      right-to-left. All the normal &gtk; containers, such as
      <classname>GtkTable</classname>or <classname>GtkMenuBar</classname>
      contain such logic.
    </para>
    <para>
      However, there are some cases where the default reversal is not
      wanted. As an example of such case, consider a table of buttons
      where the left-most button is used to choose left-justification,
      the middle center-justification, and the right-justification. It
      would not be correct for this table to be flipped, since then we
      would have left-justification on the right. The layout of this
      table is not logical, it is visual. To handle these cases, &gtk;
      allows overriding the global default direction using the
      function:
    </para>
      <!-- begin_proto="gtkwidget.h#gtk_widget_set_direction" -->
<funcsynopsis>
  <funcdef>void <function>gtk_widget_set_direction</function></funcdef>
  <paramdef>GtkWidget *<parameter>widget</parameter></paramdef>
  <paramdef>GtkTextDirection <parameter>dir</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->
  </sect1>
</chapter>

<!-- Local Variables: -->
<!-- sgml-parent-document: ("main.sgml" "part" "chapter")  -->
<!-- End: -->
