<chapter id="interacting">
  <title>Interacting With The User</title>
  <para>
    In this chapter, we are going to learn about some of the more basic
    widgets used in &gtk; applications.  
  </para>
  
  <sect1>
    <title>Widget Introduction</title>
    <para>
      widget intro..
    </para>
  </sect1>

  <sect1>
    <title>Labels</title>
    <para>
      Labels are they mainstay of most graphical user interfaces.  While the interfaces
      are certainly graphical, text is still required to let the user know how to get
      things done.  Labels in &gtk; are very simple on the surface, but there are a lot
      of advanced features and abilities that require explaining.
    </para>

    <para>
      First, the basics.  A label is created with the function:
      <!-- begin_proto="gtklabel.h#gtk_label_new" -->
<funcsynopsis>
  <funcdef>GtkWidget *<function>gtk_label_new</function></funcdef>
  <paramdef>const char *<parameter>str</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto="gtklabel.h#gtk_label_new" -->
      where the <parameter>str</parameter> parameter refers to the string to set the label to.
      You may also change that string at any time using the function:
      <!-- begin_proto="gtklabel.h#gtk_label_set_text" -->
<funcsynopsis>
  <funcdef>void <function>gtk_label_set_text</function></funcdef>
  <paramdef>GtkLabel *<parameter>label</parameter></paramdef>
  <paramdef>const char *<parameter>str</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto="gtklabel.h#gtk_label_set_text" -->
    </para>

    <para>
      If you wish the user to be able to select the text of that label,
      you can toggle this with the function:
      <!-- begin_proto="gtklabel.h#gtk_label_set_selectable" -->
<funcsynopsis>
  <funcdef>void <function>gtk_label_set_selectable</function></funcdef>
  <paramdef>GtkLabel *<parameter>label</parameter></paramdef>
  <paramdef>gboolean <parameter>setting</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto="gtklabel.h#gtk_label_set_selectable" -->
      where the <parameter>setting</parameter> parameter should be <symbol>TRUE</symbol>
      or <symbol>FALSE</symbol>.
    </para>

    <para>
      New for &gtk; 2.0, is the ability to use markup within a label instead
      of just straight text.  The markup includes the ability to specify a number
      of common settings such as color, size, italics, bold, superscript, subscript
      etc.  In order to use this functionality, you must set the markup of the
      label with the function:
      <!-- begin_proto="gtklabel.h#gtk_label_set_markup" -->
<funcsynopsis>
  <funcdef>void <function>gtk_label_set_markup</function></funcdef>
  <paramdef>GtkLabel *<parameter>label</parameter></paramdef>
  <paramdef>const gchar *<parameter>str</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto="gtklabel.h#gtk_label_set_markup" -->
    </para>
  </sect1>

  <sect1>
    <title>Buttons</title>
    <para>
      Buttons are a very basic widget, and are very easy to learn.  You have
      already seen most of the interface for buttons just in the few examples
      we have shown you.  However, we will still go through them here, and describe
      them in more detail.
    </para>
    <para>
      As described in the previous chapter, many widget in &gtk; are containers.  
      The <classname>GtkButton</classname> widget is one such widget.  It can be
      used to hold any other &gtk; widgets you wish.  One could even place a button
      within a button, but that would just be plain silly!  It is however very
      useful to be able to place images and/or text within a button, which is a very
      common requirement.
    </para>
    <para>
      To create an empty button, use the following function:
      
      <!-- begin_proto="gtkbutton.h#gtk_button_new_with_label" -->
<funcsynopsis>
  <funcdef>GtkWidget *<function>gtk_button_new_with_label</function></funcdef>
  <paramdef>const gchar *<parameter>label</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto="gtkbutton.h#gtk_button_new_with_label" -->

      You can then go about using <function>gtk_container_add</function> to place a
      widget in it.  This of course, includes the various container widgets as described
      in the previous chapter.
    </para>

    <para>
      As <classname>GtkButton</classname> is derived from the <classname>GtkContainer</classname>
      class, the function:

      <!-- begin_proto="gtkcontainer.h#gtk_container_set_border_width" -->
<funcsynopsis>
  <funcdef>void <function>gtk_container_set_border_width</function></funcdef>
  <paramdef>GtkContainer *<parameter>container</parameter></paramdef>
  <paramdef>guint <parameter>border_width</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto="gtkcontainer.h#gtk_container_set_border_width" -->

      may be used to specify the &quot;border width&quot; of the button.  This will set
      how many pixels of padding to place around the containing widget within the button.
    </para>
    <para>
      Placing a label within a button is such a commonly used paradigm that an interface
      was created to easily accomplish this.  This function creates a new button
      and places a <classname>GtkLabel</classname> inside it for you.  Note that there
      is no way to modify this internally stored label once the button is created.
      If you require the ability to change the label, you will have to create an empty
      button, and place the label within it yourself.  The function used to create a
      button with a label is:
      
      <!-- begin_proto="gtkbutton.h#gtk_button_new_with_label" -->
<funcsynopsis>
  <funcdef>GtkWidget *<function>gtk_button_new_with_label</function></funcdef>
  <paramdef>const gchar *<parameter>label</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto="gtkbutton.h#gtk_button_new_with_label" -->
    </para>

    <para>
      You will notice in some graphical applications, that buttons exist which are not
      drawn with borders until the mouse pointer is over them.  This &quot;border&quot;
      is more formally referred to as the <firstterm>relief</firstterm>.  The drawing
      of the relief can be controlled using the function:
      
      <!-- begin_proto="gtkbutton.h#gtk_button_set_relief" -->
<funcsynopsis>
  <funcdef>void <function>gtk_button_set_relief</function></funcdef>
  <paramdef>GtkButton *<parameter>button</parameter></paramdef>
  <paramdef>GtkReliefStyle <parameter>newstyle</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto="gtkbutton.h#gtk_button_set_relief" -->

      The <type>GtkReleifStyle</type> is an enumeration which can be one of:
      <variablelist>
        <varlistentry>
          <term>
            GTK_RELIEF_NORMAL
          </term>
          <listitem>
	  <para>
            This is the standard button appearance.  The releif is drawn at
            all times.
          </para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>
            GTK_RELIEF_HALF
          </term>
          <listitem>
	  <para>
            A button using this setting will only have the relief drawn when the mouse
            pointer is within the boundary of the button.
          </para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>
            GTK_RELIEF_NONE
          </term>
          <listitem>
	  <para>
            umm?
          </para>
          </listitem>
        </varlistentry>
      </variablelist>
    </para>

    <para>
      In addition to the ubiquitous <literal>::clicked</literal> literal, the button
      also supports <literal>::enter</literal>, and <literal>::leave</literal> literals, which
      are triggered with the mouse pointer enters and leaves the button respectively.
      While most &gtk; themes do various sorts of highlighting when the mouse pointer
      enters a button, this can be used to produce &quot;rollover&quot; style effects
      in excess of the default.
    </para> 

    <!-- IAN: I think I need a pile of screenshots here, to visually explain each setting
         (releif and border_width) -->
  </sect1>


  <sect1>
    <title>Example Application</title>
    <para>
      example application
    </para>
  </sect1>
  
</chapter>

<!-- Local Variables: -->
<!-- sgml-parent-document: ("main.sgml" "part" "partintro")  -->
<!-- End: -->
