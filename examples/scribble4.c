#include <gtk/gtk.h>
#include <stdio.h>

/* Array of colors the user can pick from */

static GdkColor colors[8] = {
  { 0, 0x0000, 0x0000, 0x0000 },	/* black */
  { 0, 0xFFFF, 0xFFFF, 0xFFFF },	/* white */
  { 0, 0xFFFF, 0x0000, 0x0000 },	/* red */
  { 0, 0x0000, 0xFFFF, 0x0000 },	/* green */
  { 0, 0x0000, 0x0000, 0xFFFF },	/* blue */
  { 0, 0x0000, 0xFFFF, 0xFFFF },	/* cyan */
  { 0, 0xFFFF, 0x0000, 0xFFFF },	/* magenta */
  { 0, 0xFFFF, 0xFFFF, 0x0000 } 	/* yellow */
};

static int ncolors = sizeof(colors) / sizeof(colors[0]);

/* Backing pixmap for drawing area */
static GdkPixmap *pixmap = NULL;
/*< begin_listing="change_color0" >*/
static GdkColor *draw_color = NULL;
static GdkGC *draw_gc = NULL;
/*< end_listing="change_color0" >*/

/* Create a new backing pixmap of the appropriate size */
static gint
configure_event (GtkWidget *widget, GdkEventConfigure *event)
{
  GdkPixmap *new_pixmap;
  
  new_pixmap = gdk_pixmap_new(widget->window,
			      widget->allocation.width,
			      widget->allocation.height,
			      -1);

  gdk_draw_rectangle (new_pixmap,
		      widget->style->white_gc,
		      TRUE,
		      0, 0,
		      widget->allocation.width,
		      widget->allocation.height);

  if (pixmap)
    {
      gint width, height;

      gdk_window_get_size (pixmap, &width, &height);
      gdk_draw_drawable (new_pixmap,
			 widget->style->white_gc,
			 pixmap,
			 0, 0, 0, 0, width, height);
      
      gdk_pixmap_unref(pixmap);
    }

  pixmap = new_pixmap;

  return TRUE;
}

/* Redraw the screen from the backing pixmap */
static gint
expose_event (GtkWidget *widget, GdkEventExpose *event)
{
  gdk_draw_drawable (widget->window,
		     widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
		     pixmap,
		     event->area.x, event->area.y,
		     event->area.x, event->area.y,
		     event->area.width, event->area.height);

  return FALSE;
}

/*< begin_listing="draw_brush" >*/
/* Draw a rectangle on the screen */
static void
draw_brush (GtkWidget     *widget,
	    gdouble        x,
	    gdouble        y,
	    gdouble        pressure,
	    GdkInputSource source)
{
  GdkRectangle update_rect;
  gint diameter = pressure * 10;

  update_rect.x = x - diameter;
  update_rect.y = y - diameter;
  update_rect.width = 2 * diameter;
  update_rect.height = 2 * diameter;
  gdk_draw_rectangle (pixmap,
		      source == GDK_SOURCE_ERASER ? 
		           widget->style->white_gc : 
		           draw_gc,
		      TRUE,
		      update_rect.x, update_rect.y,
		      update_rect.width, update_rect.height);
  gdk_window_invalidate_rect (widget->window, &update_rect, FALSE);
}
/*< end_listing="draw_brush" >*/

/* Draw a number on the screen */
static void
draw_counter (GtkWidget *widget, gdouble x, gdouble y)
{
  GdkRectangle update_rect;

  static int counter = 0;
  char buffer[32];

  GdkFont *font = widget->style->font;

  counter++;
  sprintf (buffer, "%d", counter);

  update_rect.width = gdk_string_width (font, buffer);
  update_rect.height = font->ascent + font->descent;
  
  x -= update_rect.width / 2;
  y += update_rect.height/2 - font->descent;

  update_rect.x = x;
  update_rect.y = y - font->ascent;
  
  gdk_draw_string (pixmap, font, draw_gc,
		   x, y, buffer);
  gtk_widget_draw (widget, &update_rect);
}

/* Callback when the user presses a button */
static gint
button_press_event (GtkWidget *widget, GdkEventButton *event)
{
  if (pixmap != NULL)
    {
      switch (event->button)
	{
	case 1:
	  draw_brush (widget, event->x, event->y,
		      event->pressure, event->source);
	  break;
	case 2:
	  draw_counter (widget, event->x, event->y);
	  break;
      }
    }

  return TRUE;
}

/* Callback when the user moves the mouse */
static gint
motion_notify_event (GtkWidget *widget, GdkEventMotion *event)
{
  if (event->is_hint)
    gdk_window_get_pointer (event->window, NULL, NULL, NULL);
    
  if (event->state & GDK_BUTTON1_MASK && pixmap != NULL)
    draw_brush (widget, event->x, event->y, event->pressure, event->source);
  
  return TRUE;
}

/*< begin_listing="change_color1" >*/
/* Change the current color and graphics context */
void
change_color (GtkWidget *widget, GdkColor *color)
{
  GdkColormap *cmap = gtk_widget_get_colormap (widget);
  
  if (draw_color)
    gdk_colors_free (cmap, &draw_color->pixel, 1, 0);
  else
    draw_color = g_new (GdkColor, 1);

  *draw_color = *color;

  gdk_color_alloc (cmap, draw_color);
  
  if (!draw_gc)
    draw_gc = gdk_gc_new (widget->window);

  gdk_gc_set_foreground (draw_gc, draw_color);
}
/*< end_listing="change_color1" >*/

/* Create the menu of colors */
GtkWidget *
create_colors ()
{
  int i;
  
  GtkWidget *option_menu;
  GtkWidget *menu;

  menu = gtk_menu_new ();
  
  for (i=0; i<ncolors; i++)
    {
      GtkStyle *style;
      GtkWidget *menu_item;
      GtkWidget *darea;
      
      menu_item = gtk_menu_item_new ();

      darea = gtk_drawing_area_new ();
      gtk_widget_ensure_style (darea);

      style = gtk_style_copy (darea->style);
      style->bg[GTK_STATE_NORMAL] = colors[i];

      gtk_widget_set_style (darea, style);
      gtk_widget_set_usize (darea, 100, 15);

      gtk_container_add (GTK_CONTAINER (menu_item), darea);
      /* We need to call gtk_widget_show_all() separately
       * for each menu item, since we don't want to show
       * the menu yet.
       */
      gtk_widget_show_all (menu_item);

      gtk_signal_connect (GTK_OBJECT (menu_item), "activate", 
			  GTK_SIGNAL_FUNC (change_color), &colors[i]);

      gtk_menu_append (GTK_MENU (menu), menu_item);
    }

  option_menu = gtk_option_menu_new ();
  gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), menu);

  return option_menu;
}

/*< begin_listing="create_input_dialog" >*/
void
create_input_dialog (void)
{
  static GtkWidget *inputd = NULL;

  if (!inputd)
    {
      inputd = gtk_input_dialog_new();

      gtk_signal_connect (GTK_OBJECT(inputd), "destroy",
			  GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			  &inputd);
      gtk_signal_connect_object (GTK_OBJECT(GTK_INPUT_DIALOG(inputd)->close_button),
				 "clicked",
				 GTK_SIGNAL_FUNC (gtk_widget_hide),
				 GTK_OBJECT(inputd));
      gtk_widget_hide (GTK_INPUT_DIALOG(inputd)->save_button);
    }

  gtk_widget_show (inputd);
}
/*< end_listing="create_input_dialog" >*/

int
main (int argc, char *argv[])
{
  static GdkColor initial_color = { 0, 0, 0, 0 };

  GtkWidget *window;
  GtkWidget *drawing_area;
  GtkWidget *vbox;

  GtkWidget *button;

  GdkCursor *cursor;

  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_name (window, "Test Input");

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

  gtk_box_pack_start (GTK_BOX (vbox), create_colors(), FALSE, FALSE, 0);

  /* Create the drawing area */

  drawing_area = gtk_drawing_area_new ();
  gtk_drawing_area_size (GTK_DRAWING_AREA (drawing_area), 200, 200);
  gtk_box_pack_start (GTK_BOX (vbox), drawing_area, TRUE, TRUE, 0);

  /* Signals used to handle backing pixmap */

  gtk_signal_connect (GTK_OBJECT (drawing_area), "expose_event",
		      (GtkSignalFunc) expose_event, NULL);
  gtk_signal_connect (GTK_OBJECT(drawing_area), "configure_event",
		      (GtkSignalFunc) configure_event, NULL);

  /* Set up the drawing GC after the window is created */

  gtk_signal_connect (GTK_OBJECT (drawing_area), "realize",
		      (GtkSignalFunc) change_color, &initial_color);

  /* Event signals */

  gtk_signal_connect (GTK_OBJECT (drawing_area), "motion_notify_event",
		      (GtkSignalFunc) motion_notify_event, NULL);
  gtk_signal_connect (GTK_OBJECT (drawing_area), "button_press_event",
		      (GtkSignalFunc) button_press_event, NULL);

  gtk_widget_set_events (drawing_area, GDK_EXPOSURE_MASK
			 | GDK_LEAVE_NOTIFY_MASK
			 | GDK_BUTTON_PRESS_MASK
			 | GDK_POINTER_MOTION_MASK
			 | GDK_POINTER_MOTION_HINT_MASK);

  /* The following call enables tracking and processing of extension
     events for the drawing area */
  gtk_widget_set_extension_events (drawing_area, GDK_EXTENSION_EVENTS_CURSOR);

  /* Set a cursor for the drawing area */

  /*< begin_listing="cursor" >*/
  gtk_widget_realize (drawing_area);
  cursor = gdk_cursor_new (GDK_PENCIL);
  gdk_window_set_cursor (drawing_area->window, cursor);
  
  gdk_cursor_unref (cursor);
  /*< end_listing="cursor" >*/

  /* Create a button to bring up an input device configuration dialog */
  button = gtk_button_new_with_label ("Input Dialog");
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (create_input_dialog), NULL);

  /* .. And a quit button */
  button = gtk_button_new_with_label ("Quit");
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy),
			     GTK_OBJECT (window));

  gtk_widget_show_all (window);

  gtk_main ();

  return 0;
}
