#include <gtk/gtk.h>

/*< begin_listing="drag-source0" >*/
static void 
drag_data_get (GtkWidget         *widget,
	       GdkDragContext    *context,
	       GtkSelectionData  *selection_data,
	       guint              info,
	       guint              time)
{
  gtk_selection_data_set (selection_data, GDK_TARGET_STRING, 8,
			  "Hello", strlen ("Hello"));
}
/*< end_listing="drag-source0" >*/

/*< begin_listing="drag-target0" >*/
static void 
drag_data_received (GtkWidget          *widget,
		    GdkDragContext     *context,
		    gint                x,
		    gint                y,
		    GtkSelectionData   *selection_data,
		    guint               info,
		    guint               time)
{
  g_print ("Drop of text \"%s\" received.\n", selection_data->data);
}
/*< end_listing="drag-target0" >*/


int 
main (int argc, char **argv)
{
  /*< begin_listing="drag-targets" >*/
  static GtkTargetEntry target = { "STRING", 0, 0 };
  /*< end_listing="drag-targets" >*/
  
  GtkWidget *button, *vbox, *window;
  
  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
  
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (window), vbox);
  
  /* Set up a drag source
   */
  /*< begin_listing="drag-source1" >*/
  button = gtk_button_new_with_label ("Drag Here");
  /*< end_listing="drag-source1" >*/
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

  /*< begin_listing="drag-source2" >*/
  gtk_drag_source_set (button,
		       GDK_BUTTON1_MASK,
		       &target, 1,
		       GDK_ACTION_COPY);
  gtk_signal_connect (GTK_OBJECT (button), "drag_data_get",
		      GTK_SIGNAL_FUNC (drag_data_get), NULL);
  /*< end_listing="drag-source2" >*/

  /* Set up a drop target
   */
/*< begin_listing="drag-target1" >*/
  button = gtk_button_new_with_label ("Drop Here");
/*< end_listing="drag-target1" >*/
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

/*< begin_listing="drag-target2" >*/
  gtk_drag_dest_set (button,
		     GTK_DEST_DEFAULT_ALL,
		     &target, 1,
		     GDK_ACTION_COPY);
  gtk_signal_connect (GTK_OBJECT (button), "drag_data_received",
		      GTK_SIGNAL_FUNC (drag_data_received), NULL);
/*< end_listing="drag-target2" >*/

  gtk_widget_show_all (window);

  gtk_main ();
  
  return 0;
}
