#include <gtk/gtk.h>

/*< begin_listing="event-handler" >*/
gboolean
event_handler (GtkWidget      *widget,
	       GdkEvent       *event,
	       gpointer        data)
{
  switch (event->type)
    {
    case GDK_BUTTON_PRESS:
      if (event->button.button == 1)
	{
	  gtk_widget_destroy (data);
	  return TRUE;
	}
      break;
    case GDK_BUTTON_RELEASE:
      /* Do something else */
      break;
    default:
    }

  return FALSE;
}
/*< end_listing="event-handler" >*/

int main (int argc, char **argv)
{
  GtkWidget *window;
  GtkWidget *eventbox;
  GtkWidget *label;

  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_container_border_width (GTK_CONTAINER (window), 10);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

  eventbox = gtk_event_box_new ();
  gtk_widget_set_events (eventbox, GDK_BUTTON_PRESS_MASK);
  gtk_container_add (GTK_CONTAINER (window), eventbox);

  label = gtk_label_new ("Click Me");
  gtk_container_add (GTK_CONTAINER (eventbox), label);

/*< begin_listing="connect-event" >*/
  gtk_signal_connect (GTK_OBJECT (eventbox), "event",
		      GTK_SIGNAL_FUNC (event_handler), 
		      window);
/*< end_listing="connect-event" >*/

  gtk_widget_show_all (window);
  gtk_main ();

  return 0;
}
