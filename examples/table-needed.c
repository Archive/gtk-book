#include <gtk/gtk.h>

GtkWidget *
make_label (const gchar *str)
{
  GtkWidget *label = gtk_label_new (str);
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);

  return label;
}

int
main (int argc, char **argv)
{
  GtkWidget *window, *table;

  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), "Phone Information");
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

  table = gtk_table_new (3, 2, FALSE);
  gtk_container_add (GTK_CONTAINER (window), table);

  gtk_table_set_row_spacings (GTK_TABLE (table), 4);
  gtk_table_set_col_spacings (GTK_TABLE (table), 4);
  gtk_container_set_border_width (GTK_CONTAINER (table), 8);

  gtk_table_attach (GTK_TABLE (table), make_label ("Name:"),
		    /* x */                 /* y */
/* attach */	    0, 1,                   0, 1,    
/* options */	    GTK_FILL,               0,
/* padding */	    0,                      0);      

  gtk_table_attach (GTK_TABLE (table), gtk_entry_new(),
		    /* x */                 /* y */
/* attach */	    1, 2,                   0, 1,    
/* options */	    GTK_EXPAND | GTK_FILL,  0,       
/* padding */	    0,                      0);      

  gtk_table_attach (GTK_TABLE (table), make_label ("Extension:"),
		    /* x */                 /* y */
/* attach */	    0, 1,                   1, 2,    
/* options */	    GTK_FILL,               0,
/* padding */	    0,                      0);      

  gtk_table_attach (GTK_TABLE (table), gtk_entry_new(),
		    /* x */                 /* y */
/* attach */	    1, 2,                   1, 2,    
/* options */	    GTK_EXPAND | GTK_FILL,  0,       
/* padding */	    0,                      0);      

  gtk_table_attach (GTK_TABLE (table), make_label ("Department:"),
		    /* x */                 /* y */
/* attach */	    0, 1,                   2, 3,    
/* options */	    GTK_FILL,               0,
/* padding */	    0,                      0);      

  gtk_table_attach (GTK_TABLE (table), gtk_combo_new (),
		    /* x */                 /* y */
/* attach */	    1, 2,                   2, 3,    
/* options */	    GTK_EXPAND | GTK_FILL,  0,       
/* padding */	    0,                      0);      

  gtk_widget_show_all (window);

  gtk_main();

  return 0;
}
