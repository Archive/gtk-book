#include <gtk/gtk.h>

/* The destroy callback.  This is called when the "destroy" event
 * is processed.  This occurs when the widget is being destroyed. */
void destroy_callback (GtkWidget *widget, gpointer data)
{
   g_print ("Exiting.. \\n");
   gtk_main_quit ();
}

int main (int argc, char *argv[])
{
   /* GtkWidget is the storage type for widgets */
   GtkWidget *window;

   /* Initializes &gdk; and GTK.  Arguments from the command line
    * are parsed and removed.  Anything on the command line that is
    * not recognized will remain in argc and argv. */
   gtk_init (&amp;argc, &amp;argv);
    
   /* Creates a new top level window */
   window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
   
   /* This sets up the "destroy_event" signal handler.  This event will 
    * be sent to the window when it is about to be destroyed. */
   gtk_signal_connect (GTK_OBJECT (window), "destroy",
                       GTK_SIGNAL_FUNC (destroy_callback), NULL);

   /* The window is now initialized, but it won't be displayed on the screen
    * until we explicitly show it here. */
   gtk_widget_show (window);
   
   /* The main Gtk loop.  Program control waits here until
    * an event occurs.   At that point, if a handler was set up for
    * the event, the handler is called. */
   gtk_main ();
   
   return (0);
}
