#include <glib.h>

#ifndef __ENTRY_H__
#define __ENTRY_H__

/*< begin_listing="AddressEntry" >*/
typedef struct  {
  gchar *firstname;
  gchar *lastname;
  gchar *email;
  gchar *phone;
  gint   birth_day;
  gint   birth_month;
  gchar *address;
  gchar *comments;
} AddressEntry;
/*< end_listing="AddressEntry" >*/

AddressEntry * address_entry_new       (void);
void           address_entry_free      (AddressEntry *entry);
void           address_entry_append    (AddressEntry *entry,
					GString      *string);
void           address_entry_set_month (AddressEntry *entry,
					const char   *month_str);



const gchar * address_entry_get_month_name (int month);

GScanner* address_entry_scanner_new (const gchar *input_name);



gboolean address_entries_parse (GScanner    *scanner,
				GList      **result);
gboolean address_entries_save  (GList       *entries,
				const char  *filename,
				gchar      **error_string);


#endif /* __ENTRY_H__ */


