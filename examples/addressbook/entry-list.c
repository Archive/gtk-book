#include "entry-list.h"
#include "addressbook-utils.h"

enum {
  CLIST_COL_FIRSTNAME,
  CLIST_COL_LASTNAME,
  CLIST_COL_PHONE,
  CLIST_COL_EMAIL,
  CLIST_N_COLS
};

static gchar *clist_column_names[CLIST_N_COLS] =
{
  "First",
  "Last",
  "Phone",
  "EMail",
};

static guint clist_column_widths[CLIST_N_COLS] =
{
  40, 60, 60, 100,
};

/*< begin_listing="entry_list_clipboard" >*/
static GString *clipboard = NULL;     /* Entry currently on the clipboard */
/*< end_listing="entry_list_clipboard" >*/

static void do_popup      (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);

/*< begin_listing="popup-item-array" >*/
typedef enum {
  POPUP_CUT,
  POPUP_COPY,
  POPUP_PASTE
} PopupAction;

static GtkItemFactoryEntry popup_menu_items[] =
{
  { "/Cu_t",   NULL, do_popup, POPUP_CUT,   NULL },
  { "/_Copy",  NULL, do_popup, POPUP_COPY,  NULL },
  { "/_Paste", NULL, do_popup, POPUP_PASTE, NULL }, 
};

static int npopup_menu_items = sizeof (popup_menu_items) / sizeof (popup_menu_items[0]);
/*< end_listing="popup-item-array" >*/

void
entry_list_update (EntryList   *elist,
		   AddressEntry *entry)
{
  gint row;

  GtkCList *clist = GTK_CLIST (elist->clist);

  row = gtk_clist_find_row_from_data (clist, entry);
  g_return_if_fail (row != -1);

  gtk_clist_set_text (clist, row, CLIST_COL_FIRSTNAME, entry->firstname ? entry->firstname : "");
  gtk_clist_set_text (clist, row, CLIST_COL_LASTNAME, entry->lastname ? entry->lastname : "");
  gtk_clist_set_text (clist, row, CLIST_COL_PHONE, entry->phone ? entry->phone : "");
  gtk_clist_set_text (clist, row, CLIST_COL_EMAIL, entry->email ? entry->email : "");

  elist->is_dirty = TRUE;
}

/*< begin_listing="select_row_cb" >*/
static void
select_row_cb (GtkCList       *clist,
	       gint            row,
	       gint            column,
	       GdkEventButton *event)
{
  AddressEntry *entry = gtk_clist_get_row_data (clist, row);
  EntryList *elist = gtk_object_get_data (GTK_OBJECT (clist), "entry-list");

  elist->current_row = row;
  elist->entry_select_func (entry, elist->cb_data);
}
/*< end_listing="select_row_cb" >*/

void
entry_list_add_entries (EntryList  *elist,
			GList       *new_entries,
			gboolean     select,
			gint         start_row)
{
  GList *tmp_list;
  GtkCList *clist = GTK_CLIST (elist->clist);
  
  gint row = start_row;
  
  if (!new_entries)
    return;

  gtk_clist_freeze (clist);

  tmp_list = new_entries;
  while (tmp_list)
    {
      gchar *data[CLIST_N_COLS];
      AddressEntry *entry = tmp_list->data;
      guint i;

      for (i = 0; i < CLIST_N_COLS; i++)
	data[i] = NULL;
      
      row = gtk_clist_insert (clist, row + 1, data);
      gtk_clist_set_row_data_full (clist, row, entry,
				   (GtkDestroyNotify)address_entry_free);
      entry_list_update (elist, entry); /* Ugh, n^2 */

      if (select)
	gtk_clist_select_row (clist, row, 0);
      
      tmp_list = tmp_list->next;
    }

  if (select)
    {
      gtk_clist_unselect_all (clist);
      gtk_clist_select_row (clist, row, 0);
    }

  gtk_clist_thaw (clist);

  elist->is_dirty = TRUE;
}

static void
append_selection (EntryList *elist, GString *string)
{
  GList *tmp_list;

  tmp_list = GTK_CLIST (elist->clist)->selection;
  while (tmp_list)
    {
      gint row;
      AddressEntry *entry;
      
      row = GPOINTER_TO_INT (tmp_list->data);
      entry = gtk_clist_get_row_data (GTK_CLIST (elist->clist), row);
      
      address_entry_append (entry, string);
      
      tmp_list = tmp_list->next;
    }
}

int
compare_rows_descending (gconstpointer a, gconstpointer b)
{
  int inta = GPOINTER_TO_INT (a);
  int intb = GPOINTER_TO_INT (b);

  return (inta > intb) ? -1 : ((inta < intb) ? 1 : 0);
}

void           
entry_list_cut (EntryList *elist)
{
  GList *tmp_list;

  entry_list_copy (elist);

  /* We need to make a copy of our list in descending order so that
   * when we iterate through the list, we don't disturb the
   * following rows.
   */
  tmp_list = g_list_copy (GTK_CLIST (elist->clist)->selection);
  tmp_list = g_list_sort (tmp_list, compare_rows_descending);
  
  while (tmp_list)
    {
      gint row = GPOINTER_TO_INT (tmp_list->data);

      if (elist->current_row > row)
	elist->current_row--;

      gtk_clist_remove (GTK_CLIST (elist->clist), row);

      tmp_list = tmp_list->next;
    }

  if (GTK_CLIST (elist->clist)->rows)
    gtk_clist_select_row (GTK_CLIST (elist->clist), 
			  elist->current_row >= 0 ? elist->current_row : 0, 0);
}

/*< begin_listing="entry_list_copy" >*/
void           
entry_list_copy (EntryList *elist)
{
  if (GTK_CLIST (elist->clist)->selection)
    {
      if (!clipboard)
	clipboard = g_string_new (NULL);
      else
	g_string_truncate (clipboard, 0);

      append_selection (elist, clipboard);

      gtk_selection_owner_set (elist->clist,
			       gdk_atom_intern ("CLIPBOARD", FALSE),
			       get_event_time ());
    }
}
/*< end_listing="entry_list_copy" >*/

/*< begin_listing="entry_list_paste" >*/
void 
entry_list_paste (EntryList *elist)
{
  gtk_selection_convert (elist->clist,
			 gdk_atom_intern ("CLIPBOARD", FALSE),
			 gdk_atom_intern ("_GTK_ADDRESS_BOOK", FALSE),
			 get_event_time());
}
/*< end_listing="entry_list_paste" >*/

static void
do_popup_copy (EntryList *elist, AddressEntry *entry)
{
  if (!clipboard)
    clipboard = g_string_new (NULL);
  else
    g_string_truncate (clipboard, 0);
  
  address_entry_append (entry, clipboard);
}

static void
do_popup_cut (EntryList *elist, AddressEntry *entry)
{
  gint row;
  
  do_popup_copy (elist, entry);

  row = gtk_clist_find_row_from_data (GTK_CLIST (elist->clist), entry);

  gtk_clist_remove (GTK_CLIST (elist->clist), row);
}

static void
do_popup_paste (EntryList *elist, AddressEntry *entry)
{
  entry_list_paste (elist);
}

static void
do_popup (gpointer callback_data,
	  guint callback_action,
	  GtkWidget *widget)
{
  EntryList *elist = callback_data;
  AddressEntry *entry = gtk_item_factory_popup_data_from_widget (widget);

  switch (callback_action)
    {
    case POPUP_CUT:
      do_popup_cut (elist, entry);
      break;
    case POPUP_COPY:
      do_popup_copy (elist, entry);
      break;
    case POPUP_PASTE:
      do_popup_paste (elist, entry);
      break;
    }
}

static void
clist_button_press_cb (GtkCList       *clist,
		       GdkEventButton *event)
{
  EntryList *elist = gtk_object_get_data (GTK_OBJECT (clist), "entry-list");

  if (event->window == clist->clist_window && event->button == 3)
    {
      gint row;

      if (gtk_clist_get_selection_info (clist,
					event->x, event->y,
					&row, NULL))
	{
	  AddressEntry *entry = gtk_clist_get_row_data (clist, row);

	  gtk_clist_unselect_all (clist);
	  gtk_clist_select_row (clist, row, 0);
	  gtk_item_factory_popup_with_data (elist->popup_ifactory, entry, NULL,
					    event->x_root, event->y_root,
					    event->button, event->time);
	}
    }
}

/* Handler called to supply contents of clipboard
 */
/*< begin_listing="selection_get" >*/
void
selection_get (GtkWidget        *widget,
	       GtkSelectionData *selection_data,
	       guint             info,
	       guint             time,
	       gpointer          data)
{
  if (clipboard)
    gtk_selection_data_set (selection_data, GDK_SELECTION_TYPE_STRING,
			    8, clipboard->str, clipboard->len);
}
/*< end_listing="selection_get" >*/

/* Handler called when clipboard data is received, or request
 * for data was rejected.
 */
/*< begin_listing="selection_received" >*/
static void
selection_received  (GtkWidget         *widget,
		     GtkSelectionData  *selection_data,
		     guint              time)
{
  GScanner *scanner;
  GList *new_entries;
  EntryList *elist = gtk_object_get_data (GTK_OBJECT (widget), "entry-list");

  if ((selection_data->length < 0) ||
      (selection_data->type != GDK_SELECTION_TYPE_STRING))
    return;			/* Retrieval failed */


  
  scanner = address_entry_scanner_new ("pasted text");
      
  g_scanner_input_text (scanner, selection_data->data, selection_data->length);
      
  if (address_entries_parse (scanner, &new_entries))
    {
      entry_list_add_entries (elist, new_entries, TRUE, elist->current_row);
      g_list_free (new_entries);
    }
  
  g_scanner_destroy (scanner);
  
  if (GTK_CLIST (elist->clist)->rows && !GTK_CLIST (elist->clist)->selection)
    gtk_clist_select_row (GTK_CLIST (elist->clist), 0, 0);
}
/*< end_listing="selection_received" >*/

/*< begin_listing="entry_list_new-simple0" >*/
EntryList *
entry_list_new (EntrySelectFunc entry_select_func,
		gpointer        data)
{
  EntryList *elist;
/*< end_listing="entry_list_new-simple0" >*/
  
  guint i;
  /*< begin_listing="adding_targets0" >*/
  static GtkTargetEntry targets[] = {
    { "_GTK_ADDRESS_BOOK", 0, 0 },
    { "text/plain", 0, 0 },
    { "STRING", 0, 0 },
  };
  static int n_targets = 3;
  /*< end_listing="adding_targets0" >*/

/*< begin_listing="entry_list_new-simple1" >*/
  elist = g_new (EntryList, 1);

  elist->current_row = -1;
  elist->entry_select_func = entry_select_func;
  elist->cb_data = data;
  elist->is_dirty = FALSE;
  
  elist->clist = gtk_clist_new_with_titles (CLIST_N_COLS, clist_column_names);
  gtk_object_set_data_full (GTK_OBJECT (elist->clist), "entry-list",
			    elist, (GDestroyNotify)g_free);
/*< end_listing="entry_list_new-simple1" >*/
  
  for (i = 0; i < CLIST_N_COLS; i++)
    gtk_clist_set_column_width (GTK_CLIST (elist->clist), i, clist_column_widths[i]);

  gtk_clist_set_selection_mode (GTK_CLIST (elist->clist), GTK_SELECTION_EXTENDED);
  
  gtk_widget_set_usize (elist->clist, 200, -1);

  gtk_signal_connect (GTK_OBJECT (elist->clist), "select_row",
		      GTK_SIGNAL_FUNC (select_row_cb), elist);
  gtk_signal_connect (GTK_OBJECT (elist->clist), "button_press_event",
		      GTK_SIGNAL_FUNC (clist_button_press_cb), elist);
  
  elist->popup_ifactory = gtk_item_factory_new (GTK_TYPE_MENU, "<popup>", NULL);
  gtk_item_factory_create_items (elist->popup_ifactory,
				 npopup_menu_items, popup_menu_items, elist);
  
  /* Set up handler for clipboard */

  /*< begin_listing="adding_targets1" >*/
  gtk_selection_add_targets (elist->clist,
			     gdk_atom_intern ("CLIPBOARD", FALSE),
			     targets, n_targets);

  gtk_signal_connect (GTK_OBJECT (elist->clist),
		      "selection_get",
		      GTK_SIGNAL_FUNC (selection_get),
		      NULL);
  /*< end_listing="adding_targets1" >*/

  gtk_signal_connect (GTK_OBJECT (elist->clist),
		      "selection_received",
		      GTK_SIGNAL_FUNC (selection_received),
		      elist);

  return elist;
}

AddressEntry *
entry_list_get_current_entry (EntryList *elist)
{
  if (elist->current_row != -1)
    return gtk_clist_get_row_data (GTK_CLIST (elist->clist), elist->current_row);
  else
    return NULL;
}

void
entry_list_foreach (EntryList *elist, GFunc func, gpointer cb_data)
{
  GList *tmp_list = GTK_CLIST (elist->clist)->row_list;

  while (tmp_list)
    {
      gpointer row_data = ((GtkCListRow *)tmp_list->data)->data;
      tmp_list = tmp_list->next;
      
      (*func) (row_data, cb_data);
    }
}
