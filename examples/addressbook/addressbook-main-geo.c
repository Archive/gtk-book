#include <gtk/gtk.h>

GtkWidget *window;
  
GtkWidget *firstname_entry;
GtkWidget *lastname_entry;
GtkWidget *email_entry;
GtkWidget *phone_entry;
  
GtkWidget *day_spinner;
GtkWidget *month_combo;
  
GtkWidget *address_text;
GtkWidget *comments_text;

/**********************************
 * Creation of the User Interface *
 **********************************/

static gint
delete_callback (GtkWidget *widget, GdkEvent *event, gpointer data)
{
  gtk_main_quit();

  return TRUE;
}

/*< begin_listing="make_pair" >*/
static GtkWidget *
make_pair (gchar *labeltext, GtkWidget *widget)
{
  GtkWidget *util_hbox;
  GtkWidget *label;

  util_hbox = gtk_hbox_new (FALSE, 2);

  label = gtk_label_new (labeltext);
  gtk_box_pack_start (GTK_BOX (util_hbox), label,  FALSE, FALSE, 0);

  gtk_box_pack_start (GTK_BOX (util_hbox), widget, TRUE, TRUE, 0);

  return util_hbox;
}
/*< end_listing="make_pair" >*/

/*< begin_listing="make_text_frame" >*/
GtkWidget *
make_text_frame (gchar *labeltext, GtkWidget **text)
{
  GtkWidget *frame;
  GtkWidget *util_vbox;
  GtkWidget *scroll_win;
  GtkWidget *label;

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);

  util_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (frame), util_vbox);
  gtk_container_border_width (GTK_CONTAINER (util_vbox), 5);
  
  /*< begin_listing="gtk_misc_set_alignment" >*/
  label = gtk_label_new (labeltext);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  /*< end_listing="gtk_misc_set_alignment" >*/
  gtk_box_pack_start (GTK_BOX (util_vbox), label, FALSE, FALSE, 0);

  scroll_win = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll_win),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start (GTK_BOX (util_vbox), scroll_win, TRUE, TRUE, 0);

  *text = gtk_text_new (NULL, NULL);
  gtk_text_set_editable (GTK_TEXT (*text), TRUE);
  gtk_container_add (GTK_CONTAINER (scroll_win), *text);

  return frame;
}
/*< end_listing="make_text_frame" >*/

void
create_address_book (void)
{
  GtkWidget *vbox;
  GtkWidget *vpaned;
  GtkWidget *frame;
  GtkWidget *alignment;
  GtkWidget *table;
  GtkAdjustment *adj;
  GtkWidget *util_hbox;
  GtkWidget *util_vbox;

  GList *month_list;
  int i;
  
  static const gchar *months[] = {
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  };
  
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), "AddressBook");
  gtk_signal_connect (GTK_OBJECT (window), "delete_event", 
		      GTK_SIGNAL_FUNC (delete_callback), NULL);

  /*< begin_listing="gtk_window_set_default_size" >*/
  gtk_window_set_default_size (GTK_WINDOW (window), -1, 500);
  /*< end_listing="gtk_window_set_default_size" >*/
  
  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);

  gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);

  util_vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), util_vbox);
  gtk_container_border_width (GTK_CONTAINER (util_vbox), 10);

  /*     Name */

  frame = gtk_frame_new ("Name");
  gtk_box_pack_start (GTK_BOX (util_vbox), frame, FALSE, FALSE, 0);

  util_hbox = gtk_hbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), util_hbox);
  gtk_container_border_width (GTK_CONTAINER (util_hbox), 5);

  /*< begin_listing="make-pair-ex" >*/
  firstname_entry = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_pair ("First:", firstname_entry),
		      TRUE, TRUE, 0);
  /*< end_listing="make-pair-ex" >*/
  
  lastname_entry = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_pair ("Last:", lastname_entry),
		      TRUE, TRUE, 0);

  /*< begin_listing="table-enforce" >*/
  table = gtk_table_new (1, 3, TRUE);
  gtk_table_set_col_spacings (GTK_TABLE (table), 5);
  gtk_box_pack_start (GTK_BOX (util_vbox), table, FALSE, FALSE, 0);

  /*     Email  */

  email_entry = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (table), 
		    make_pair ("Email:", email_entry),
		    0, 2,                                0, 1,
		    GTK_FILL | GTK_EXPAND | GTK_SHRINK,  0,
		    0,                                   0);

  /*     Phone */

  phone_entry = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (table), 
		    make_pair ("Phone:", phone_entry),
		    2, 3,    		                 0, 1,
		    GTK_FILL | GTK_EXPAND | GTK_SHRINK,  0,
		    0,   		                 0);
  gtk_widget_set_usize (phone_entry, 100, -1);
  /*< end_listing="table-enforce" >*/
  
  /*     Birthday */

  /*< begin_listing="birthday-alignment" >*/
  alignment = gtk_alignment_new (0.0, 0.5, 0.0, 0.0);
  gtk_box_pack_start (GTK_BOX (util_vbox), alignment, FALSE, FALSE, 0);

  frame = gtk_frame_new ("Birthday");
  gtk_container_add (GTK_CONTAINER (alignment), frame);
  /*< end_listing="birthday-alignment" >*/

  util_hbox = gtk_hbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), util_hbox);
  gtk_container_border_width (GTK_CONTAINER (util_hbox), 5);

  month_combo = gtk_combo_new();
  month_list = NULL;
  for (i=1; i<=12; i++)
    month_list = g_list_append (month_list, (char *)months[i]);
  gtk_combo_set_popdown_strings (GTK_COMBO (month_combo), month_list);
  gtk_combo_set_value_in_list (GTK_COMBO (month_combo), TRUE, TRUE);

  g_list_free (month_list);

  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_pair ("Month:", month_combo),
		      TRUE, TRUE, 0);


  adj = GTK_ADJUSTMENT (gtk_adjustment_new (0, 0, 31, 1, 1, 0));
  day_spinner = gtk_spin_button_new (adj, 1, 0);
  gtk_widget_set_usize (day_spinner, 40, -1);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (day_spinner), TRUE);
  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_pair ("Day:", day_spinner),
		      TRUE, TRUE, 0);

  /*< begin_listing="vpaned" >*/
  vpaned = gtk_vpaned_new ();
  gtk_box_pack_start (GTK_BOX (vbox), vpaned, TRUE, TRUE, 0);

  /*     Address text widget */
  gtk_paned_pack1 (GTK_PANED (vpaned), 
		   make_text_frame ("Address:", &address_text),
		   TRUE, FALSE);

  /*     Comments text widget */
  gtk_paned_pack2 (GTK_PANED (vpaned), 
		  make_text_frame ("Comments:", &comments_text),
		  TRUE, FALSE);
  /*< end_listing="vpaned" >*/
  
  gtk_widget_show_all (window);
}

int
main (int    argc,
      char **argv)
{
  gtk_init (&argc, &argv);

  create_address_book ();

  gtk_main ();

  return 0;
}
