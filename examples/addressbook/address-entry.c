#include <stdlib.h>
#include "addressbook-utils.h"
#include "address-entry.h"

enum {
  TOKEN_INVALID = G_TOKEN_LAST,
  TOKEN_ENTRY,
  TOKEN_FIRSTNAME,
  TOKEN_LASTNAME,
  TOKEN_EMAIL,
  TOKEN_PHONE,
  TOKEN_BIRTH_DAY,
  TOKEN_BIRTH_MONTH,
  TOKEN_ADDRESS,
  TOKEN_COMMENTS,
};
#define	FIELD_TOKEN_FIRST TOKEN_FIRSTNAME
#define	FIELD_TOKEN_LAST  TOKEN_COMMENTS

static struct
{
  gchar *name;
  gint token;
} symbols[] = {
  { "entry", TOKEN_ENTRY },
  { "firstname", TOKEN_FIRSTNAME },
  { "lastname", TOKEN_LASTNAME },
  { "phone", TOKEN_PHONE },
  { "email", TOKEN_EMAIL },
  { "address", TOKEN_ADDRESS },
  { "birth_day", TOKEN_BIRTH_DAY },
  { "birth_month", TOKEN_BIRTH_MONTH },
  { "comments", TOKEN_COMMENTS },
};
static guint nsymbols = sizeof (symbols) / sizeof (symbols[0]);

static const gchar *months[] = {
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
};


/* allocate and initialize a new adress entry structure */
AddressEntry*
address_entry_new (void)
{
  AddressEntry *entry;

  entry = g_new0 (AddressEntry, 1);
  entry->firstname = NULL;
  entry->lastname = NULL;
  entry->email = NULL;
  entry->phone = NULL;
  entry->birth_day = 0;
  entry->birth_month = 0;
  entry->address = NULL;
  entry->comments = NULL;

  return entry;
}

/* free an address entry */
void
address_entry_free (AddressEntry *entry)
{
  g_free (entry->firstname);
  g_free (entry->lastname);
  g_free (entry->email);
  g_free (entry->phone);
  g_free (entry->address);
  g_free (entry->comments);
  g_free (entry);
}

const gchar *
address_entry_get_month_name (int month)
{
  if (month >= 1 && month <= 12)
    return months[month-1];
  else
    return NULL;
}

/************************************
 * Functions for reading in entries *
 ************************************/

static guint
str_to_month (const gchar *str)
{
  guint i;
  
  for (i = 0; i < 12; i++)
    {
      if (!g_strcasecmp (months[i], str))
	return i + 1;
    }

  return 0;
}

static void
scanner_msg_func (GScanner      *scanner,
		  gchar	        *message,
		  gint		 error)
{
  gchar *label_str = g_strdup_printf (error ?
				        "Fatal error parsing %s:\n        %s" :
				        "Error parsing %s:\n        %s",
				      scanner->input_name, message);
  msgbox_run (NULL, label_str, NULL, "OK", 1);
  g_free (label_str);
  
  if (error)
    exit (1);
}

GScanner*
address_entry_scanner_new (const gchar *input_name)
{
  GScanner *scanner;
  gint i;

  scanner = g_scanner_new (NULL);
  scanner->config->symbol_2_token = TRUE;
  scanner->input_name = input_name;

  for (i = 0; i < nsymbols; i++)
    g_scanner_add_symbol (scanner, symbols[i].name, GINT_TO_POINTER (symbols[i].token));

  scanner->msg_handler = scanner_msg_func;
  
  return scanner;
}

static GTokenType
parse_entry (GScanner     *scanner,
	     AddressEntry *entry)
{
  GTokenType token;

  token = g_scanner_get_next_token (scanner);
  if (token != G_TOKEN_LEFT_CURLY)
    return G_TOKEN_LEFT_CURLY;

  token = g_scanner_get_next_token (scanner);
  while (token != G_TOKEN_RIGHT_CURLY)
    {
      GTokenType field_token;

      /* inside the entry definition we only accept fieldnames as
       * defined in the `symbols' array, all of which have
       * a token within the limits of FIELD_TOKEN_FIRST and
       * FIELD_TOKEN_LAST.
       */
      if (token < FIELD_TOKEN_FIRST ||
	  token > FIELD_TOKEN_LAST)
	return G_TOKEN_SYMBOL;

      field_token = token;

      token = g_scanner_get_next_token (scanner);
      if (token != G_TOKEN_EQUAL_SIGN)
	return G_TOKEN_EQUAL_SIGN;
      
      if (field_token == TOKEN_BIRTH_DAY)
	{
	  token = g_scanner_get_next_token (scanner);
	  if (token != G_TOKEN_INT)
	    return G_TOKEN_INT;
	  
	  entry->birth_day = CLAMP (scanner->value.v_int, 1, 31);
	  if (entry->birth_day != scanner->value.v_int)
	    g_scanner_warn (scanner, "birth day `%ld' out of range", scanner->value.v_int);
	}
      else if (field_token == TOKEN_BIRTH_MONTH)
	{
	  guint month;
	  
	  token = g_scanner_get_next_token (scanner);
	  if (token != G_TOKEN_IDENTIFIER)
	    return G_TOKEN_IDENTIFIER;

	  month = str_to_month (scanner->value.v_identifier);
	  if (month == 0)
	    g_scanner_warn (scanner, "invalid month specification \"%s\"", scanner->value.v_string);
	  else
	    entry->birth_month = month;
	}
      else
	{
	  token = g_scanner_get_next_token (scanner);
	  if (token != G_TOKEN_STRING)
	    return G_TOKEN_STRING;
	  
	  switch (field_token)
	    {
	    case TOKEN_FIRSTNAME:
	      entry->firstname = g_strdup (scanner->value.v_string);
	      break;
	      
	    case TOKEN_LASTNAME:
	      entry->lastname = g_strdup (scanner->value.v_string);
	      break;
	      
	    case TOKEN_EMAIL:
	      entry->email = g_strdup (scanner->value.v_string);
	      break;
	      
	    case TOKEN_PHONE:
	      entry->phone = g_strdup (scanner->value.v_string);
	      break;
	      
	    case TOKEN_BIRTH_DAY:
	      entry->birth_day = atoi (scanner->value.v_string);
	      break;
	      
	    case TOKEN_ADDRESS:
	      entry->address = g_strdup (scanner->value.v_string);
	      break;
	      
	    case TOKEN_COMMENTS:
	      entry->comments = g_strdup (scanner->value.v_string);
	      break;
	      
	    default:
	      g_assert_not_reached ();
	      break;
	    }
	}

      token = g_scanner_get_next_token (scanner);
    }

  return G_TOKEN_NONE;
}

gboolean
address_entries_parse (GScanner *scanner,
		       GList   **result)
{
  GTokenType expected_token;
      
  *result = NULL;

  /* parse until either the end of the input is reached, or
   * until expected_token is not equal to G_TOKEN_NONE, in
   * which case the parsing code encountered an error condition.
   */
  expected_token = G_TOKEN_NONE;
  while (expected_token == G_TOKEN_NONE &&
	 !g_scanner_eof (scanner))
    {
      GTokenType token;

      token = g_scanner_get_next_token (scanner);
      if (token == TOKEN_ENTRY)
	{
	  AddressEntry *new_entry = NULL;
	  
	  new_entry = address_entry_new ();
	  
	  expected_token = parse_entry (scanner, new_entry);
	  if (expected_token == G_TOKEN_NONE)
	    *result = g_list_append (*result, new_entry);
	  else
	    address_entry_free (new_entry);
	}
      else if (token != G_TOKEN_EOF)
	{
	  /* we (currently?) only feature "entry {...}" portions, and
	   * would expect the file's end otherwise.
	   */
	  expected_token = G_TOKEN_EOF;
	}
    }
  
  if (expected_token != G_TOKEN_NONE)
    {
      /* we use identifiers for months only, thus we pass
       * identifier_spec as "month name".
       * we use symbols for field names only, thus we pass
       * symbol_spec as "field name".
       * since we abort on unexpected tokens, we pass is_error
       * as TRUE.
       */
      g_scanner_unexp_token (scanner,
			     expected_token,
			     "month name",
			     "field name",
			     NULL,
			     NULL,
			     FALSE);
    }

  return *result != NULL;
}

/*************************************
 * Functions for writing out entries *
 *************************************/

static void
append_text_field (GString *string,
		   const gchar   *field_name,
		   const gchar   *field,
		   gboolean append_quoted)
{
  if (field)
    {
      const gchar *ch;

      g_string_append (string, "  ");
      g_string_append (string, field_name);
      g_string_append (string, append_quoted ? " = \"" : " = ");

      /* walk each character of the field's contents and escape
       * special characters
       */
      for (ch = field; *ch; ch++)
	{
	  switch (*ch)
	    {
	    case '\\':
	      g_string_append (string, "\\\\");
	      break;
	    case '\n':
	      g_string_append (string, "\\n");
	      break;
	    default:
	      g_string_append_c (string, *ch);
	    }
	}
  
      g_string_append (string, append_quoted ? "\"\n" : "\n");
    }
}

void
address_entry_append (AddressEntry *entry,
		      GString      *string)
{
  g_string_append (string, "entry {\n");

  append_text_field (string, "firstname", entry->firstname, TRUE);
  append_text_field (string, "lastname", entry->lastname, TRUE);
  append_text_field (string, "phone", entry->phone, TRUE);
  append_text_field (string, "email", entry->email, TRUE);
  append_text_field (string, "address", entry->address, TRUE);

  if (entry->birth_day)
    g_string_sprintfa (string, "  birth_day = %d\n", entry->birth_day);

  if (entry->birth_month)
    append_text_field (string, "birth_month", months[entry->birth_month - 1], FALSE);

  append_text_field (string, "comments", entry->comments, TRUE);

  g_string_append (string, "}\n\n");
}


void     
address_entry_set_month (AddressEntry *entry,
			 const char   *month_str)
{
  gint month = str_to_month (month_str);
  if (month != 0)
    entry->birth_month = month;
}

