#include "entry-list.h"
#include "addressbook-utils.h"

enum {
  CLIST_COL_FIRSTNAME,
  CLIST_COL_LASTNAME,
  CLIST_COL_PHONE,
  CLIST_COL_EMAIL,
  CLIST_N_COLS
};

static gchar *clist_column_names[CLIST_N_COLS] =
{
  "First",
  "Last",
  "Phone",
  "EMail",
};

static guint clist_column_widths[CLIST_N_COLS] =
{
  40, 60, 60, 100,
};

/* Entry currently on the clipboard */
GString *clipboard = NULL;

static void do_popup      (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);

/*< begin_listing="popup-item-array" >*/
typedef enum {
  POPUP_CUT,
  POPUP_COPY,
  POPUP_PASTE
} PopupAction;

static GtkItemFactoryEntry popup_menu_items[] =
{
  { "/Cu_t",   NULL, do_popup, POPUP_CUT,   NULL },
  { "/_Copy",  NULL, do_popup, POPUP_COPY,  NULL },
  { "/_Paste", NULL, do_popup, POPUP_PASTE, NULL }, 
};

static int npopup_menu_items = sizeof (popup_menu_items) / sizeof (popup_menu_items[0]);
/*< end_listing="popup-item-array" >*/

void
entry_list_update (EntryList   *elist,
		   AddressEntry *entry)
{
  gint row;

  GtkCList *clist = GTK_CLIST (elist->clist);

  row = gtk_clist_find_row_from_data (clist, entry);
  g_return_if_fail (row != -1);

  gtk_clist_set_text (clist, row, CLIST_COL_FIRSTNAME, entry->firstname ? entry->firstname : "");
  gtk_clist_set_text (clist, row, CLIST_COL_LASTNAME, entry->lastname ? entry->lastname : "");
  gtk_clist_set_text (clist, row, CLIST_COL_PHONE, entry->phone ? entry->phone : "");
  gtk_clist_set_text (clist, row, CLIST_COL_EMAIL, entry->email ? entry->email : "");

  elist->is_dirty = TRUE;
}

/*< begin_listing="select_row_cb" >*/
static void
select_row_cb (GtkCList       *clist,
	       gint            row,
	       gint            column,
	       GdkEventButton *event)
{
  AddressEntry *entry = gtk_clist_get_row_data (clist, row);
  EntryList *elist = gtk_object_get_data (GTK_OBJECT (clist), "entry-list");

  elist->current_row = row;
  elist->entry_select_func (entry, elist->cb_data);
}
/*< end_listing="select_row_cb" >*/

void
entry_list_add_entries (EntryList  *elist,
			GList       *new_entries,
			gboolean     select,
			gint         start_row)
{
  GList *tmp_list;
  GtkCList *clist = GTK_CLIST (elist->clist);
  
  gint row = start_row;
  
  if (!new_entries)
    return;

  gtk_clist_freeze (clist);

  tmp_list = new_entries;
  while (tmp_list)
    {
      gchar *data[CLIST_N_COLS];
      AddressEntry *entry = tmp_list->data;
      guint i;

      for (i = 0; i < CLIST_N_COLS; i++)
	data[i] = NULL;
      
      row = gtk_clist_insert (clist, row + 1, data);
      gtk_clist_set_row_data_full (clist, row, entry,
				   (GtkDestroyNotify)address_entry_free);
      entry_list_update (elist, entry); /* Ugh, n^2 */

      if (select)
	gtk_clist_select_row (clist, row, 0);
      
      tmp_list = tmp_list->next;
    }

  if (select)
    {
      gtk_clist_unselect_all (clist);
      gtk_clist_select_row (clist, row, 0);
    }

  gtk_clist_thaw (clist);

  elist->is_dirty = TRUE;
}

void           
entry_list_cut (EntryList *elist)
{
  g_print ("Cut\n");
}

void           
entry_list_copy (EntryList *elist)
{
  g_print ("Copy\n");
}

void 
entry_list_paste (EntryList *elist)
{
  g_print ("Paste\n");
}

/*< begin_listing="do_popup_copy" >*/
static void
do_popup_copy (EntryList *elist, AddressEntry *entry)
{
  g_print ("Copying: %s\n", entry->last_name);
}
/*< end_listing="do_popup_copy" >*/

static void
do_popup_cut (EntryList *elist, AddressEntry *entry)
{
  g_print ("Cutting: %s\n", entry->last_name);
}

static void
do_popup_paste (EntryList *elist, AddressEntry *entry)
{
  g_print ("Pasting: %s\n", entry->last_name);
}

/*< begin_listing="do_popup" >*/
static void
do_popup (gpointer callback_data,
	  guint callback_action,
	  GtkWidget *widget)
{
  EntryList *elist = callback_data;
  AddressEntry *entry = gtk_item_factory_popup_data_from_widget (widget);

  switch (callback_action)
    {
    case POPUP_CUT:
      do_popup_cut (elist, entry);
      break;
    case POPUP_COPY:
      do_popup_copy (elist, entry);
      break;
    case POPUP_PASTE:
      do_popup_paste (elist, entry);
      break;
    }
}
/*< end_listing="do_popup" >*/

/*< begin_listing="clist_button_press_cb" >*/
static void
clist_button_press_cb (GtkCList       *clist,
		       GdkEventButton *event)
{
  EntryList *elist = gtk_object_get_data (GTK_OBJECT (clist), "entry-list");

  if (event->window == clist->clist_window && event->button == 3)
    {
      gint row;

      if (gtk_clist_get_selection_info (clist,
					event->x, event->y,
					&row, NULL))
	{
	  AddressEntry *entry = gtk_clist_get_row_data (clist, row);

	  gtk_clist_unselect_all (clist);
	  gtk_clist_select_row (clist, row, 0);
	  gtk_item_factory_popup_with_data (elist->popup_ifactory, entry, NULL,
					    event->x_root, event->y_root,
					    event->button, event->time);
	}
    }
}
/*< end_listing="clist_button_press_cb" >*/

EntryList *
entry_list_new (EntrySelectFunc entry_select_func,
		 gpointer        data)
{
  EntryList *elist;
  
  guint i;

  elist = g_new (EntryList, 1);

  elist->current_row = -1;
  elist->entry_select_func = entry_select_func;
  elist->cb_data = data;
  elist->is_dirty = FALSE;
  
  elist->clist = gtk_clist_new_with_titles (CLIST_N_COLS, clist_column_names);
  gtk_object_set_data_full (GTK_OBJECT (elist->clist), "entry-list",
			    elist, (GDestroyNotify)g_free);
  
  for (i = 0; i < CLIST_N_COLS; i++)
    gtk_clist_set_column_width (GTK_CLIST (elist->clist), i, clist_column_widths[i]);

  gtk_clist_set_selection_mode (GTK_CLIST (elist->clist), GTK_SELECTION_EXTENDED);
  
  gtk_widget_set_usize (elist->clist, 200, -1);

  gtk_signal_connect (GTK_OBJECT (elist->clist), "select_row",
		      GTK_SIGNAL_FUNC (select_row_cb), elist);
/*< begin_listing="button-press-connect" >*/
  gtk_signal_connect (GTK_OBJECT (elist->clist), "button_press_event",
		      GTK_SIGNAL_FUNC (clist_button_press_cb), elist);
/*< end_listing="button-press-connect" >*/

/*< begin_listing="popup-ifactory-create" >*/
  elist->popup_ifactory = gtk_item_factory_new (GTK_TYPE_MENU, "<popup>", NULL);
  gtk_item_factory_create_items (popup_ifactory,
				 npopup_menu_items, popup_menu_items, elist);
/*< end_listing="popup-ifactory-create" >*/
  
  return elist;
}

AddressEntry *
entry_list_get_current_entry (EntryList *elist)
{
  if (elist->current_row != -1)
    return gtk_clist_get_row_data (GTK_CLIST (elist->clist), elist->current_row);
  else
    return NULL;
}

void
entry_list_foreach (EntryList *elist, GFunc func, gpointer cb_data)
{
  GList *tmp_list = GTK_CLIST (elist->clist)->row_list;

  while (tmp_list)
    {
      gpointer row_data = ((GtkCListRow *)tmp_list->data)->data;
      tmp_list = tmp_list->next;
      
      (*func) (row_data, cb_data);
    }
}
