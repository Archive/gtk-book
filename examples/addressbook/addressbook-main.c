#include <sys/param.h>
#include <gtk/gtk.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include "address-entry.h"
#include "entry-list.h"
#include "addressbook-utils.h"

typedef struct  {
  gchar *filename;

  /* CList of entries */
  EntryList *entry_list;

  /* Factory for creating menus */
  GtkItemFactory *item_factory;
  GtkAccelGroup *accel_group;
  
  /* widgets */
  GtkWidget *window;
  
  GtkWidget *firstname_entry;
  GtkWidget *lastname_entry;
  GtkWidget *email_entry;
  GtkWidget *phone_entry;
  
  GtkWidget *day_spinner;
  GtkWidget *month_combo;
  
  GtkWidget *address_text;
  GtkWidget *comments_text;
} AddressBook;

static void do_new        (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);
static void do_open       (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);
static void do_save       (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);
static void do_save_as    (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);
static void do_close      (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);
static void do_quit       (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);
static void do_about      (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);
static void do_new_entry  (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);
static void do_back       (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);
static void do_forward    (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);
static void do_cut        (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);
static void do_copy       (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);
static void do_paste      (gpointer        callback_data,
			   guint           callback_action,
			   GtkWidget      *widget);

AddressBook *create_address_book (void);
void         close_address_book  (AddressBook *book);

/*
 * Toolbar Data
 */
/*< begin_listing="toolbar-array" >*/
ToolbarItem toolbar_items[] = {
  { "Back", "Previous entry", "pixmaps/back.xpm", do_back },
  { "Forward", "Next entry", "pixmaps/forward.xpm", do_forward },
  { "New", "Create a new entry", "pixmaps/new.xpm", do_new_entry },
  { "Cut", "Cut entry", "pixmaps/cut.xpm", do_cut },
  { "Copy", "Copy entry", "pixmaps/copy.xpm", do_copy },
  { "Paste", "Paste entry", "pixmaps/paste.xpm", do_paste }
};

static const int ntoolbar_items = sizeof (toolbar_items) / sizeof (toolbar_items[0]);
/*< end_listing="toolbar-array" >*/

/*
 * Menu Data
 */
/*< begin_listing="item-array" >*/
static GtkItemFactoryEntry menu_items[] =
{
  /* menu-path		 accelerator    callback + action item-type */
  { "/_File",		 NULL,		NULL, 0,	  "<Branch>" },
  { "/File/_New",	 "<Control>N",	do_new, 0,	  NULL },
  { "/File/_Open",	 "<Control>O",	do_open, 0,	  NULL },
  { "/File/_Save", 	 "<Control>S",	do_save, 0,	  NULL },
  { "/File/Save _As...", NULL,		do_save_as, 0,	  NULL },
  { "/File/sep1",	 NULL,		NULL, 0,	  "<Separator>" },
  { "/File/_Close",	 "<Control>W",	do_close, 0,	  NULL },
  { "/File/E_xit",	 "<Control>Q",	do_quit, 0,	  NULL },

  { "/_Entry",		 NULL,	        NULL, 0,	  "<Branch>" },
  { "/Entry/_New",	 NULL,		do_new_entry, 0,  NULL },
  { "/Entry/_Back",	 NULL,	        do_back, 0,	  NULL },
  { "/Entry/_Forward",	 NULL,	        do_forward, 0,	  NULL },
  { "/Entry/Cu_t",	 "<Control>X",	do_cut, 0,	  NULL },
  { "/Entry/_Copy",	 "<Control>C",	do_copy, 0,	  NULL },
  { "/Entry/_Paste",	 "<Control>V",	do_paste, 0,	  NULL }, 

  { "/_Help",		 NULL,		NULL, 0,	  "<LastBranch>" },
  { "/Help/_About",	 NULL,		do_about, 0,	  NULL },
};

static const int nmenu_items = sizeof (menu_items) / sizeof (menu_items[0]);
/*< end_listing="item-array" >*/

GSList *address_books = NULL;

const gchar *
get_pretty_name (const gchar *filename)
{
  return filename ? g_basename (filename) : "(Untitled)";
}

static void
set_file_name (AddressBook *book, const gchar *filename)
{
  char *title;

  if (filename != book->filename)
    {
      g_free (book->filename);
      book->filename = g_strdup (filename);
    }
  
  title = g_strdup_printf ("AddressBook - %s", get_pretty_name (filename));

  gtk_window_set_title (GTK_WINDOW (book->window), title);
  g_free (title);
}
  
static gboolean
open_file (const gchar *filename)
{
  GList *new_entries;
  gint fd;
  GScanner *scanner;
  AddressBook *book;
  
  fd = open (filename, O_RDONLY);
  
  if (fd < 0)
    {
      g_error ("Can't open %s: %s\n", filename, g_strerror(errno));
      return FALSE;
    }

  book = create_address_book();
  
  scanner = address_entry_scanner_new (filename);
  
  g_scanner_input_file (scanner, fd);

  if (address_entries_parse (scanner, &new_entries))
    {
      entry_list_add_entries (book->entry_list, new_entries, FALSE, 0);
      g_list_free (new_entries);

      if (GTK_CLIST (book->entry_list->clist)->rows)
	gtk_clist_select_row (GTK_CLIST (book->entry_list->clist), 0, 0);

      book->entry_list->is_dirty = FALSE;
      set_file_name (book, filename);
    }

  g_scanner_destroy (scanner);
  close (fd);

  return TRUE;
}

static gboolean
save_file (AddressBook *book, const gchar *filename)
{
  FILE *f_out;
  GString *gstring;

  f_out = fopen (filename, "w");
  if (!f_out)
    {
      g_warning ("failed to open \"%s\": %s", filename, g_strerror (errno));
      return FALSE;
    }

  gstring = g_string_new ("# addressbook data file\n\n");
  entry_list_foreach (book->entry_list, (GFunc)address_entry_append, gstring);

  fputs (gstring->str, f_out);

  g_string_free (gstring, TRUE);

  fclose (f_out);

  set_file_name (book, filename);
  book->entry_list->is_dirty = FALSE;
  
  return TRUE;
}

/******************
 * Menu Callbacks *
 ******************/

static void 
do_new (gpointer   callback_data,
	guint      callback_action,
	GtkWidget *widget)
{
  create_address_book ();
}

static gboolean
open_ok (const char *filename, gpointer data)
{
  AddressBook *book = data;

  if (open_file (filename))
    {
      if (book->filename == NULL && !book->entry_list->is_dirty)
	close_address_book (book);
      
      return TRUE;
    }
  else
    return FALSE;
}

static void 
do_open (gpointer   callback_data,
	 guint      callback_action,
	 GtkWidget *widget)
{
  AddressBook *book = callback_data;

  filesel_run (GTK_WINDOW (book->window),
	       "Open AddressBook", book->filename,
	       open_ok, book);
}

static gboolean
save_as_ok (const char *filename, gpointer data)
{
  AddressBook *book = data;

  return save_file (book, filename);
}

static void
do_save_as (gpointer   callback_data,
	    guint      callback_action,
	    GtkWidget *widget)
{
  AddressBook *book = callback_data;
  
  filesel_run (GTK_WINDOW (book->window),
	       "Save AddressBook As...", book->filename,
	       save_as_ok, book);
}

/*< begin_listing="do_save" >*/
static void 
do_save (gpointer   callback_data,
	 guint      callback_action,
	 GtkWidget *widget)
{
  AddressBook *book = callback_data;

  if (book->filename)
    save_file (book, book->filename);
  else
    do_save_as (callback_data, callback_action, widget);
}
/*< end_listing="do_save" >*/

static gboolean
maybe_close_book (AddressBook *book)
{
  gboolean really_close;

  if (!book->entry_list->is_dirty)
    really_close = TRUE;
  else
    {
      /*< begin_listing="msgbox-example" >*/
      gchar *message = g_strdup_printf ("Discard unsaved changes to \"%s\"?",
					get_pretty_name (book->filename));
      really_close = msgbox_run (GTK_WINDOW (book->window), message, "Discard", "Cancel", 1);
      g_free (message);
      /*< end_listing="msgbox-example" >*/
    }
      
  if (really_close)
    close_address_book (book);

  return really_close;
}

static void 
do_quit (gpointer   callback_data,
	 guint      callback_action,
	 GtkWidget *widget)
{
  GSList *tmp_list = address_books;
  while (tmp_list)
    {
      AddressBook *book = tmp_list->data;
      tmp_list = tmp_list->next;
      
      if (!maybe_close_book (book))
	break;
    }
}

static void 
do_close (gpointer   callback_data,
	  guint      callback_action,
	  GtkWidget *widget)
{
  AddressBook *book = callback_data;

  maybe_close_book (book);
}

static void 
do_about (gpointer   callback_data,
	  guint      callback_action,
	  GtkWidget *widget)
{
  AddressBook *book = callback_data;

  msgbox_run (GTK_WINDOW (book->window),
	      "GTK+ Addressbook Example\n"
	      "Copyright Owen Taylor, Tim Janik, and Ian Main, 1999\n"
	      "\n"
	      "From The GTK+ Book, published by O'Reilly and Associates",
	      NULL, "OK", 1);
}

static void 
do_new_entry (gpointer   callback_data,
	      guint      callback_action,
	      GtkWidget *widget)
{
  AddressBook *book = callback_data;
  GList *tmp_list;
  
  AddressEntry *entry = address_entry_new();
  entry->firstname = g_strdup ("unknown");
  entry->lastname = g_strdup ("unknown");

  tmp_list = g_list_prepend (NULL, entry);
  entry_list_add_entries (book->entry_list, tmp_list, TRUE,
			  book->entry_list->current_row);
  g_list_free (tmp_list);
}

static void 
do_back (gpointer   callback_data,
	 guint      callback_action,
	 GtkWidget *widget)
{
  AddressBook *book = callback_data;
  GtkCList *clist = GTK_CLIST (book->entry_list->clist);
  
  if (book->entry_list->current_row > 0)
    {
      gtk_clist_unselect_all (clist);
      gtk_clist_select_row (clist, book->entry_list->current_row - 1, 0);
    }
}

static void 
do_forward (gpointer   callback_data,
	    guint      callback_action,
	    GtkWidget *widget)
{
  AddressBook *book = callback_data;
  GtkCList *clist = GTK_CLIST (book->entry_list->clist);

  if (clist->rows > 0 &&
      book->entry_list->current_row < clist->rows - 1)
    {
      gtk_clist_unselect_all (clist);
      gtk_clist_select_row (clist, book->entry_list->current_row + 1, 0);
    }
}

static void 
do_cut (gpointer   callback_data,
	guint      callback_action,
	GtkWidget *widget)
{
  AddressBook *book = callback_data;

  entry_list_cut (book->entry_list);
}

/*< begin_listing="do_copy" >*/
static void
do_copy (gpointer   callback_data,
	 guint      callback_action,
	 GtkWidget *widget)
{
  AddressBook *book = callback_data;

  entry_list_copy (book->entry_list);
}
/*< end_listing="do_copy" >*/

/*< begin_listing="do_paste" >*/
static void 
do_paste (gpointer   callback_data,
	  guint      callback_action,
	  GtkWidget *widget)
{
  AddressBook *book = callback_data;

  entry_list_paste (book->entry_list);
}
/*< end_listing="do_paste" >*/

static void
editable_set_quietly (AddressBook *book,
		      GtkEditable *editable,
		      const char  *new_text)
{
  int position = 0;

  gtk_signal_handler_block_by_data (GTK_OBJECT (editable), book);

  gtk_editable_delete_text (editable, 0, -1);
  if (new_text)
    gtk_editable_insert_text (editable, new_text, strlen (new_text), &position);
  gtk_signal_handler_unblock_by_data (GTK_OBJECT (editable), book);
}

static void
entry_selected (AddressEntry  *entry,
		gpointer       data)
{
  AddressBook *book = data;

  gchar buffer[32];

  editable_set_quietly (book, GTK_EDITABLE (book->firstname_entry),
			entry->firstname);
  editable_set_quietly (book, GTK_EDITABLE (book->lastname_entry),
			entry->lastname);
  editable_set_quietly (book, GTK_EDITABLE (book->email_entry),
			entry->email);
  editable_set_quietly (book, GTK_EDITABLE (book->phone_entry),
			entry->phone);

  g_snprintf (buffer, 32, "%d", entry->birth_day);
  editable_set_quietly (book, GTK_EDITABLE (book->day_spinner),
			buffer);
  
  editable_set_quietly (book,
			GTK_EDITABLE (GTK_COMBO (book->month_combo)->entry),
			address_entry_get_month_name (entry->birth_month));
  editable_set_quietly (book, GTK_EDITABLE (book->address_text),
			entry->address);
  editable_set_quietly (book, GTK_EDITABLE (book->comments_text),
			entry->comments);
}

static gint
delete_callback (GtkWidget *widget, GdkEvent *event, AddressBook *book)
{
  maybe_close_book (book);
  return TRUE;
}

/***********************************************
 * Callbacks for Cut 'n Paste and Drag 'n Drop *
 ***********************************************/

/* Handler called when a drop is received
 */
/*< begin_listing="filename-drop" >*/
void
app_drag_data_received (GtkWidget        *widget,
			GdkDragContext   *context,
			gint              x,
			gint              y,
			GtkSelectionData *selection_data,
			guint             info,
			guint             time)
{
  gchar *text = (gchar *)selection_data->data;
  gchar *filename = NULL;
  size_t length = strlen (text);
  gchar *p;
  gchar *tmp;

  /* Extract the first URI from the uri-list
   */
  if ((p = strchr (text, '\r')))
    length = p - text;
  if ((p = strchr (text, '\n')))
    length = MIN (length, p - text);

  if (strncmp (text, "file://", 7) == 0)
    {
      /* File URL including hostname. We want to load the file 
       * only if it is a local URL.
       */
      gchar hostname[MAXHOSTNAMELEN+1];
      gethostname (hostname, MAXHOSTNAMELEN);
      hostname[MAXHOSTNAMELEN] = '\0';

      if (strncmp (text + 7, hostname, strlen (hostname)) == 0 &&
	  *(text + 7 + strlen (hostname)) == '/')
	{
	  filename = text + 7 + strlen(hostname) + 1;
	  length -= 7 + strlen(hostname) + 1;
	}
    }
  else if (strncmp (text, "file:/", 6) == 0)
    {
      /* File URL without hostname */
      filename = text + 5;
      length -= 5;
    }

  if (filename)
    {
      /* We have a filename */
      tmp = g_strndup (filename, length);
      open_file (tmp);
      g_free (tmp);
    }
}
/*< end_listing="filename-drop" >*/

/**********************************
 * Creation of the User Interface *
 **********************************/

static void
month_combo_changed_cb (GtkWidget   *widget,
			AddressBook *book)
{
  AddressEntry *entry;
  gchar        *month_str;

  entry = entry_list_get_current_entry (book->entry_list);
  if (entry)
    {
      month_str = gtk_editable_get_chars (GTK_EDITABLE (widget), 0, -1);
      address_entry_set_month (entry, month_str);
      g_free (month_str);
    }
}

static void
day_spinner_changed_cb (GtkWidget   *widget,
			AddressBook *book)
{
  AddressEntry *entry;
  gchar        *day_str;

  entry = entry_list_get_current_entry (book->entry_list);
  if (entry)
    {
      day_str = gtk_editable_get_chars (GTK_EDITABLE (widget), 0, -1);
      entry->birth_day = atoi (day_str);
      g_free (day_str);
    }
}

static void
editable_changed_cb (GtkEditable *editable,
			AddressBook *book)
{
  AddressEntry *entry;
  guint         field_offset;

  field_offset = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (editable),
							"field-offset"));

  entry = entry_list_get_current_entry (book->entry_list);
  if (entry)
    {
      gchar **field_p = (gchar**) ((gchar*) entry + field_offset);

      g_free (*field_p);
      *field_p = gtk_editable_get_chars (editable, 0, -1);

      entry_list_update (book->entry_list, entry);
    }
}

void
prepare_editable (AddressBook *book,
		  GtkEditable *editable,
		  guint	       field_offset)
{
  add_editable_revert (editable);

  gtk_signal_connect (GTK_OBJECT (editable),
		      "changed",
		      GTK_SIGNAL_FUNC (editable_changed_cb),
		      book);

  gtk_object_set_data (GTK_OBJECT (editable), "field-offset",
		       GUINT_TO_POINTER (field_offset));
}

GtkWidget*
make_field_entry (AddressBook *book,
		  gchar       *labeltext,
		  GtkWidget  **entry,
		  guint	       field_offset)
{
  *entry = gtk_entry_new ();

  prepare_editable (book, GTK_EDITABLE (*entry), field_offset);
  
  return make_pair (labeltext, book->accel_group, *entry);
}

void
phone_insert_text_cb (GtkEditable *editable,
		      const gchar *text,
		      gint         length,
		      gint        *position,
		      gpointer     data)
{
  int i, j;
  gchar *result = g_new (gchar, length);
  static const char *valid = " -()0123456789x";

  for (i=0, j=0; i<length; i++)
    if (strchr (valid, text[i]))
      result[j++] = text[i];

  gtk_signal_handler_block_by_func (GTK_OBJECT (editable),
				    GTK_SIGNAL_FUNC (phone_insert_text_cb),
				    data);
  gtk_editable_insert_text (editable, result, j, position);
  gtk_signal_handler_unblock_by_func (GTK_OBJECT (editable),
				      GTK_SIGNAL_FUNC (phone_insert_text_cb),
				      data);

  gtk_signal_emit_stop_by_name (GTK_OBJECT (editable), "insert_text");

  g_free (result);
}

void
close_address_book (AddressBook *book)
{
  address_books = g_slist_remove (address_books, book);

  gtk_widget_destroy (book->window);
  gtk_object_unref (GTK_OBJECT (book->item_factory));
  gtk_accel_group_unref (book->accel_group);

  g_free (book->filename);
  g_free (book);

  if (!address_books)
    gtk_main_quit ();
}

AddressBook *
create_address_book (void)
{
  AddressBook *book;

  GtkWidget *main_vbox;
  GtkWidget *vbox;
  GtkWidget *hpaned;
  GtkWidget *vpaned;
  GtkWidget *frame;
  GtkWidget *alignment;
  GtkWidget *table;
  GtkAdjustment *adj;
  GtkWidget *util_hbox;
  GtkWidget *util_vbox;
  GtkWidget *toolbar;
  GtkWidget *scrolled_window;

  GList *month_list;

  static GtkTargetEntry app_target = {
    "text/uri-list", 0, 0
  };

  gint i;

  book = g_new (AddressBook, 1);
  book->filename = NULL;
  
  book->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (book->window), "AddressBook");
  gtk_window_set_default_size (GTK_WINDOW (book->window), -1, 500);
  
  set_file_name (book, NULL);	/* Set the window title */
  gtk_signal_connect (GTK_OBJECT (book->window), "delete_event", 
		      GTK_SIGNAL_FUNC (delete_callback), book);

  gtk_drag_dest_set (book->window, GTK_DEST_DEFAULT_ALL, &app_target, 1, GDK_ACTION_COPY);
  gtk_signal_connect (GTK_OBJECT (book->window), "drag_data_received",
		      GTK_SIGNAL_FUNC (app_drag_data_received), NULL);

		      
  
  /* Fill in the contents of the main window */

  main_vbox = gtk_vbox_new (FALSE, 0);

  gtk_container_add (GTK_CONTAINER (book->window), main_vbox);

  /* Create the menu */

  /*< begin_listing="create-menu" >*/
  book->accel_group = gtk_accel_group_new ();
  book->item_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR, "<main>", book->accel_group);
  gtk_item_factory_create_items (book->item_factory, nmenu_items, menu_items, book);

  gtk_window_add_accel_group (GTK_WINDOW (book->window), book->accel_group);

  gtk_box_pack_start (GTK_BOX (main_vbox),
		      gtk_item_factory_get_widget (book->item_factory, "<main>"),
		      FALSE, FALSE, 0);
  /*< end_listing="create-menu" >*/

  hpaned = gtk_hpaned_new ();
  gtk_box_pack_start (GTK_BOX (main_vbox), hpaned, TRUE, TRUE, 0);

  /* Address clist */

  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  book->entry_list = entry_list_new (entry_selected, book);
  
  gtk_container_add (GTK_CONTAINER (scrolled_window), book->entry_list->clist);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_AUTOMATIC);
  
  gtk_paned_add1 (GTK_PANED (hpaned), scrolled_window);

  /* The details */

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_paned_add2 (GTK_PANED (hpaned), vbox);

  /*< begin_listing="toolbar-create" >*/
  toolbar = toolbar_create (toolbar_items, ntoolbar_items, book);
  gtk_box_pack_start (GTK_BOX (vbox), toolbar, FALSE, FALSE, 0);
  /*< end_listing="toolbar-create" >*/

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);

  gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);

  util_vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), util_vbox);
  gtk_container_set_border_width (GTK_CONTAINER (util_vbox), 10);

  /*     Name */

  frame = gtk_frame_new ("Name");
  gtk_box_pack_start (GTK_BOX (util_vbox), frame, FALSE, FALSE, 0);

  util_hbox = gtk_hbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), util_hbox);
  gtk_container_set_border_width (GTK_CONTAINER (util_hbox), 5);
  
  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_field_entry (book, "Fi_rst:",
					&book->firstname_entry,
					GTK_STRUCT_OFFSET (AddressEntry, firstname)),
		      TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_field_entry (book, "_Last:",
					&book->lastname_entry,
					GTK_STRUCT_OFFSET (AddressEntry, lastname)),
		      TRUE, TRUE, 0);

  table = gtk_table_new (1, 3, TRUE);
  gtk_table_set_col_spacings (GTK_TABLE (table), 5);
  gtk_box_pack_start (GTK_BOX (util_vbox), table, FALSE, FALSE, 0);

  /*     Email  */

  gtk_table_attach (GTK_TABLE (table), 
		    make_field_entry (book, "Ema_il:",
				      &book->email_entry,
				      GTK_STRUCT_OFFSET (AddressEntry, email)),
		    0, 2,
		    0, 1,
		    GTK_FILL | GTK_EXPAND | GTK_SHRINK,
		    0, 0,
		    0);

  /*     Phone */
  gtk_table_attach (GTK_TABLE (table), 
		    make_field_entry (book, "_Phone:",
				      &book->phone_entry,
				      GTK_STRUCT_OFFSET (AddressEntry, phone)),
		    2, 3, 0, 1, GTK_FILL | GTK_EXPAND | GTK_SHRINK, 0, 0, 0);
  gtk_widget_set_usize (book->phone_entry, 100, -1);
  gtk_signal_connect (GTK_OBJECT (book->phone_entry),
		      "insert_text",
		      GTK_SIGNAL_FUNC (phone_insert_text_cb),
		      NULL);
  
  /*     Birthday */

  alignment = gtk_alignment_new (0.0, 0.5, 0.0, 0.0);
  gtk_box_pack_start (GTK_BOX (util_vbox), alignment, FALSE, FALSE, 0);

  frame = gtk_frame_new ("Birthday");
  gtk_container_add (GTK_CONTAINER (alignment), frame);

  util_hbox = gtk_hbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), util_hbox);
  gtk_container_set_border_width (GTK_CONTAINER (util_hbox), 5);

  book->month_combo = gtk_combo_new();
  month_list = NULL;
  for (i=1; i<=12; i++)
    month_list = g_list_append (month_list,
				(char *)address_entry_get_month_name (i));
  gtk_combo_set_popdown_strings (GTK_COMBO (book->month_combo), month_list);
  gtk_combo_set_value_in_list (GTK_COMBO (book->month_combo), TRUE, TRUE);

  add_editable_revert (GTK_EDITABLE (GTK_COMBO (book->month_combo)->entry));

  gtk_signal_connect (GTK_OBJECT (GTK_COMBO (book->month_combo)->entry), "changed",
		      GTK_SIGNAL_FUNC (month_combo_changed_cb), book);

  g_list_free (month_list);

  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_pair ("_Month:", book->accel_group, book->month_combo),
		      TRUE, TRUE, 0);


  adj = GTK_ADJUSTMENT (gtk_adjustment_new (0, 0, 31, 1, 1, 0));
  book->day_spinner = gtk_spin_button_new (adj, 1, 0);
  gtk_widget_set_usize (book->day_spinner, 40, -1);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (book->day_spinner), TRUE);
  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_pair ("_Day:", book->accel_group, book->day_spinner),
		      TRUE, TRUE, 0);

  add_editable_revert (GTK_EDITABLE (book->day_spinner));
  gtk_signal_connect (GTK_OBJECT (book->day_spinner), "changed",
		      GTK_SIGNAL_FUNC (day_spinner_changed_cb), book);

  vpaned = gtk_vpaned_new ();
  gtk_box_pack_start (GTK_BOX (vbox), vpaned, TRUE, TRUE, 0);

  /*     Address text widget */

  gtk_paned_pack1 (GTK_PANED (vpaned), 
		   make_text_frame ("_Address:", book->accel_group, &book->address_text),
		   TRUE, FALSE);
  prepare_editable (book, GTK_EDITABLE (book->address_text),
		    GTK_STRUCT_OFFSET (AddressEntry, address));
  
  /*     Comments text widget */
  
  gtk_paned_pack2 (GTK_PANED (vpaned), 
		   make_text_frame ("_Comments:", book->accel_group, &book->comments_text),
		   TRUE, FALSE);
  prepare_editable (book, GTK_EDITABLE (book->comments_text),
		    GTK_STRUCT_OFFSET (AddressEntry, comments));

  gtk_widget_show_all (book->window);

  address_books = g_slist_prepend (address_books, book);
  
  return book;
}

/*< begin_listing="addressbook_message_handler" >*/
void
addressbook_message_handler (const char    *domain,
			     GLogLevelFlags level,
			     const char    *message,
			     gpointer       user_data)
{
  gchar *error_str;

  error_str = g_strconcat (level & G_LOG_FLAG_FATAL ? "Fatal Error\n" : "Error:\n",
			   message, NULL);
  msgbox_run (NULL, error_str, "OK", NULL, 0);
  g_free (error_str);
}
/*< end_listing="addressbook_message_handler" >*/

int
main (int    argc,
      char **argv)
{
  gchar *accel_file_name;
  
  /*< begin_listing="gtk_rc_add_default_file" >*/
  gchar *rc_file = g_strconcat (g_get_home_dir(), "/", ".addressbook.gtkrc", NULL);
  gtk_rc_add_default_file (rc_file);
  g_free (rc_file);
  /*< end_listing="gtk_rc_add_default_file" >*/

  gtk_set_locale ();
  gtk_init (&argc, &argv);

/*< begin_listing="set_log_handler" >*/
  g_log_set_handler (addressbook_log_domain,
		     G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL,
		     addressbook_message_handler, NULL);
/*< end_listing="set_log_handler" >*/
  
  /*< begin_listing="gtk_item_factory_parse_rc" >*/
  accel_file_name = g_strconcat (g_get_home_dir(), "/", ".addressbook.accels", NULL);
  gtk_item_factory_parse_rc (accel_file_name);
  /*< end_listing="gtk_item_factory_parse_rc" >*/
  
  if (!open_file ("./testbook.adr"))
    create_address_book ();

  gtk_main ();

  /*< begin_listing="gtk_item_factory_dump_rc" >*/
  gtk_item_factory_dump_rc (accel_file_name, NULL, TRUE);
  g_free (accel_file_name);
  /*< end_listing="gtk_item_factory_dump_rc" >*/

  return 0;
}
