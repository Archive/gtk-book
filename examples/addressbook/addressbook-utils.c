#include "addressbook-utils.h"
#include <gdk/gdkkeysyms.h>

const char *addressbook_log_domain = "Addressbook";

/*< begin_listing="toolbar_activate_cb" >*/
static void
toolbar_activate_cb (GtkWidget *widget,
		     gpointer   user_data)
{
  GtkItemFactoryCallback1 callback = gtk_object_get_data (GTK_OBJECT (widget), "toolbar-callback");

  (*callback) (user_data, 0, widget);
}
/*< end_listing="toolbar_activate_cb" >*/

/*< begin_listing="toolbar_append_item" >*/
static void
toolbar_append_item (GtkToolbar  *toolbar,
		     ToolbarItem *item,
		     gpointer     callback_data)
{
  GtkWidget *pwidget = NULL;
/*< begin_listing="create_pixmap" >*/
  GdkPixmap *pixmap;
  GdkBitmap *mask;
  GtkWidget *item_widget;
  
  pixmap = gdk_pixmap_colormap_create_from_xpm (NULL,
						gtk_widget_get_colormap (GTK_WIDGET (toolbar)),
						&mask,
						NULL,
						item->pixmapfile);
/*< end_listing="create_pixmap" >*/
  if (pixmap)
    {
      pwidget = gtk_pixmap_new (pixmap, mask);
      gdk_pixmap_unref (pixmap);
      gdk_bitmap_unref (mask);
    }

  item_widget = gtk_toolbar_append_item (toolbar, 
					 item->text, item->tooltip_text, NULL,
					 pwidget, toolbar_activate_cb, callback_data);

  gtk_object_set_data (GTK_OBJECT (item_widget), "toolbar-callback", item->callback);
}
/*< end_listing="toolbar_append_item" >*/

/*< begin_listing="toolbar_create" >*/
GtkWidget *
toolbar_create (ToolbarItem    *items,
		gint            nitems,
		gpointer        callback_data)
{
  int i;
  
  GtkWidget *toolbar = gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL,
					GTK_TOOLBAR_BOTH);

  for (i=0; i<nitems; i++)
    toolbar_append_item (GTK_TOOLBAR (toolbar), &items[i], callback_data);

  return toolbar;
}
/*< end_listing="toolbar_create" >*/

#if 0
GtkWidget *
make_text_frame (gchar *labeltext, GtkWidget **text)
{
  GtkWidget *frame;
  GtkWidget *util_vbox;
  GtkWidget *scrollbar;
  GtkWidget *util_hbox;
  GtkWidget *label;

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);

  util_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (frame), util_vbox);
  gtk_container_border_width (GTK_CONTAINER (util_vbox), 5);
  
  label = gtk_label_new (labeltext);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (util_vbox), label, FALSE, FALSE, 0);

  util_hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (util_vbox), util_hbox, TRUE, TRUE, 0);

  *text = gtk_text_new (NULL, NULL);
  gtk_text_set_editable (GTK_TEXT (*text), TRUE);
  gtk_box_pack_start (GTK_BOX (util_hbox), *text, TRUE, TRUE, 0);

  scrollbar = gtk_vscrollbar_new (GTK_TEXT (*text)->vadj);
  gtk_box_pack_start (GTK_BOX (util_hbox), scrollbar, FALSE, FALSE, 0);

  return frame;
}
#endif /* 0 */

/*< begin_listing="make_text_frame" >*/
GtkWidget *
make_text_frame (gchar *labeltext,
		 GtkAccelGroup  *accel_group,
		 GtkWidget **text)
{
  GtkWidget *frame;
  GtkWidget *util_vbox;
  GtkWidget *scroll_win;
  GtkWidget *label;
  guint keysym;

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);

  util_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (frame), util_vbox);
  gtk_container_border_width (GTK_CONTAINER (util_vbox), 5);
  
  label = gtk_label_new (NULL);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);

  gtk_box_pack_start (GTK_BOX (util_vbox), label, FALSE, FALSE, 0);

  scroll_win = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll_win),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start (GTK_BOX (util_vbox), scroll_win, TRUE, TRUE, 0);

  *text = gtk_text_new (NULL, NULL);
  gtk_text_set_editable (GTK_TEXT (*text), TRUE);
  gtk_container_add (GTK_CONTAINER (scroll_win), *text);

  keysym = gtk_label_parse_uline (GTK_LABEL (label), labeltext);
  if (keysym)
    gtk_accel_group_add (accel_group, keysym, GDK_MOD1_MASK, 0,
			 GTK_OBJECT (*text), "grab-focus");
  
  return frame;
}
/*< end_listing="make_text_frame" >*/

/*< begin_listing="make_pair" >*/
GtkWidget *
make_pair (gchar *labeltext,
	   GtkAccelGroup  *accel_group,
	   GtkWidget *widget)
{
  GtkWidget *util_hbox;
  GtkWidget *label;
  guint keysym;

  util_hbox = gtk_hbox_new (FALSE, 2);

  label = gtk_label_new (NULL);
  gtk_box_pack_start (GTK_BOX (util_hbox), label,  FALSE, FALSE, 0);

  gtk_box_pack_start (GTK_BOX (util_hbox), widget, TRUE, TRUE, 0);

  keysym = gtk_label_parse_uline (GTK_LABEL (label), labeltext);
  if (keysym)
    gtk_accel_group_add (accel_group, keysym, GDK_MOD1_MASK, 0,
			 GTK_OBJECT (widget), "grab-focus");

  return util_hbox;
}
/*< end_listing="make_pair" >*/

/* Get the timestamp of the current event. Actually, the only thing
 * we really care about below is the key event
 */
guint32
get_event_time (void)
{
  GdkEvent *event;
  guint32 result = GDK_CURRENT_TIME;
  
  event = gtk_get_current_event();
  if (event)
    {
      result = gdk_event_get_time (event);
      gdk_event_free (event);
    }
  
  return result;
}

static void
filesel_ok_cb (GtkWidget *button, GtkWidget *filesel)
{
  FileselOKFunc ok_func = gtk_object_get_data (GTK_OBJECT (filesel), "ok-func");
  gpointer data = gtk_object_get_data (GTK_OBJECT (filesel), "ok-data");
  
  gtk_widget_hide (filesel);
  
  if ((*ok_func) (gtk_file_selection_get_filename (GTK_FILE_SELECTION (filesel)), data))
    gtk_widget_destroy (filesel);
  else
    gtk_widget_show (filesel);
}

void
filesel_run (GtkWindow    *parent, 
	     const char   *title,
	     const char   *start_file,
	     FileselOKFunc func,
	     gpointer      data)
{
  GtkWidget *filesel = gtk_file_selection_new (title);

  if (parent)
    gtk_window_set_transient_for (GTK_WINDOW (filesel), parent);

  if (start_file)
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (filesel), start_file);

  
  gtk_object_set_data (GTK_OBJECT (filesel), "ok-func", func);
  gtk_object_set_data (GTK_OBJECT (filesel), "ok-data", data);

  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->ok_button),
		      "clicked",
		      GTK_SIGNAL_FUNC (filesel_ok_cb), filesel);
  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->cancel_button),
			     "clicked",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy), GTK_OBJECT (filesel));

  gtk_signal_connect (GTK_OBJECT (filesel), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
  gtk_window_set_modal (GTK_WINDOW (filesel), TRUE);

  gtk_widget_show (filesel);
  gtk_main ();
}

/*< begin_listing="msgbox_run" >*/
static void
msgbox_yes_cb (GtkWidget *widget, gboolean *result)
{
  *result = TRUE;
  gtk_object_destroy (GTK_OBJECT (gtk_widget_get_toplevel (widget)));
}

/*< begin_listing="escape-closes0" >*/
static gboolean
msgbox_key_press_cb (GtkWidget *widget, GdkEventKey *event, gpointer data)
{
  if (event->keyval == GDK_Escape)
    {
      gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "key_press_event");
      gtk_object_destroy (GTK_OBJECT (widget));
      return TRUE;
    }

  return FALSE;
}
/*< end_listing="escape-closes0" >*/

gboolean
msgbox_run (GtkWindow  *parent,
	    const char *message,
	    const char *yes_button,
	    const char *no_button,
	    gint default_index)
{
  gboolean result = FALSE;
  GtkWidget *dialog;
  GtkWidget *button;
  GtkWidget *label;
  GtkWidget *vbox;
  GtkWidget *button_box;
  GtkWidget *separator;

  g_return_val_if_fail (message != NULL, FALSE);
  g_return_val_if_fail (default_index >= 0 && default_index <= 1, FALSE);
  
  /* Create a dialog
   */
  dialog = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  if (parent)
    gtk_window_set_transient_for (GTK_WINDOW (dialog), parent);
  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);

  /* Quit our recursive main loop when the dialog is destroyed.
   */
  gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

  /* Catch Escape key presses and have them destroy the dialog
   */
/*< begin_listing="escape-closes1" >*/
  gtk_signal_connect (GTK_OBJECT (dialog), "key_press_event",
		      GTK_SIGNAL_FUNC (msgbox_key_press_cb), NULL);
/*< end_listing="escape-closes1" >*/

  /* Fill in the contents of the widget
   */
  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (dialog), vbox);
  
  label = gtk_label_new (message);
  gtk_misc_set_padding (GTK_MISC (label), 12, 12);
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);

  separator = gtk_hseparator_new ();
  gtk_box_pack_start (GTK_BOX (vbox), separator, FALSE, FALSE, 0);

  button_box = gtk_hbutton_box_new ();
  gtk_box_pack_start (GTK_BOX (vbox), button_box, FALSE, FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (button_box), 8);
  

  /* When Yes is clicked, call the msgbox_yes_cb
   * This sets the result variable and destroys the dialog
   */
  if (yes_button)
    {
      button = gtk_button_new_with_label (yes_button);
      GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
      gtk_container_add (GTK_CONTAINER (button_box), button);

      if (default_index == 0)
	gtk_widget_grab_default (button);
      
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
			  GTK_SIGNAL_FUNC (msgbox_yes_cb), &result);
    }

  /* When No is clicked, destroy the dialog
   */
  if (no_button)
    {
      button = gtk_button_new_with_label (no_button);
      GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
      gtk_container_add (GTK_CONTAINER (button_box), button);
      
      if (default_index == 1)
	gtk_widget_grab_default (button);
      
      gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
				 GTK_SIGNAL_FUNC (gtk_object_destroy), GTK_OBJECT (dialog));
    }

  gtk_widget_show_all (dialog);

  /* Run a recursive main loop until a button is clicked
   * or the user destroys the dialog through the window mananger */
  gtk_main ();

  return result;
}
/*< end_listing="msgbox_run" >*/

/*< begin_listing="add-editable-revert" >*/
static gboolean
editable_focus_in_cb (GtkWidget *widget, GdkEventFocus *key)
{
  gchar *str = gtk_editable_get_chars (GTK_EDITABLE (widget), 0, -1);
  gtk_object_set_data_full (GTK_OBJECT (widget), "editable-revert-data",
				str, (GDestroyNotify)g_free);
  return TRUE;
}

static gboolean
editable_key_press_cb (GtkWidget *widget, GdkEventKey *event)
{
  if (gtk_bindings_activate (GTK_OBJECT (widget), event->keyval, event->state))
    {
      gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "key_press_event");
      return TRUE;
    }
  else
    return FALSE;
}

static void
revert_cb (GtkEditable *editable, gpointer data)
{
  gchar *text = gtk_object_get_data (GTK_OBJECT (editable), "editable-revert-data");
  gint position = 0;

  if (text)
    {
      gtk_editable_delete_text (editable, 0, -1);
      gtk_editable_insert_text (editable, text, strlen (text), &position);
    }
}

void
add_editable_revert (GtkEditable *editable)
{
  static gboolean initialized = FALSE;

  g_return_if_fail (GTK_IS_EDITABLE (editable));
  
  if (!initialized)
    {
      GtkBindingSet *binding_set;
      initialized = TRUE;

      g_signal_newc ("revert", GTK_TYPE_EDITABLE, G_SIGNAL_ACTION,
		     0, NULL, NULL,
		     gtk_marshal_NONE__NONE,
		     G_TYPE_NONE /* 'void' return */,
		     0 /* No extra arguments */);
      
      binding_set = gtk_binding_set_by_class (gtk_type_class (GTK_TYPE_EDITABLE));
      
      gtk_binding_entry_add_signal (binding_set, GDK_Escape, 0, "revert", 0);
    }

  gtk_signal_connect (GTK_OBJECT (editable), "focus_in_event",
		      GTK_SIGNAL_FUNC (editable_focus_in_cb), NULL);
  gtk_signal_connect (GTK_OBJECT (editable), "revert",
		      GTK_SIGNAL_FUNC (revert_cb), NULL);
  gtk_signal_connect (GTK_OBJECT (editable), "key_press_event",
		      GTK_SIGNAL_FUNC (editable_key_press_cb), NULL);
}
/*< end_listing="add-editable-revert" >*/
