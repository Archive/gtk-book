#include <gtk/gtk.h>
#include "address-entry.h"

typedef void (*EntrySelectFunc) (AddressEntry  *entry,
				 gpointer       data);

/*< begin_listing="EntryList" >*/
typedef struct _EntryList EntryList;

struct _EntryList
{
  GtkWidget      *clist;
  gint            current_row;
  GtkItemFactory *popup_ifactory;

  EntrySelectFunc entry_select_func;
  gpointer cb_data;

  gboolean is_dirty;
};
/*< end_listing="EntryList" >*/

EntryList *    entry_list_new               (EntrySelectFunc  entry_select_func,
					     gpointer         data);
void           entry_list_update            (EntryList       *elist,
					     AddressEntry    *entry);
void           entry_list_add_entries       (EntryList       *elist,
					     GList           *new_entries,
					     gboolean         select,
					     gint             start_row);
void           entry_list_copy              (EntryList       *elist);
void           entry_list_cut               (EntryList       *elist);
void           entry_list_paste             (EntryList       *elist);
AddressEntry * entry_list_get_current_entry (EntryList       *elist);
void           entry_list_foreach           (EntryList       *elist,
					     GFunc            func,
					     gpointer         cb_data);










