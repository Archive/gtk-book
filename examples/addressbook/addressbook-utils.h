#include <gtk/gtk.h>

/*< begin_listing="ToolbarItem" >*/
typedef struct {
  gchar *text;
  gchar *tooltip_text;
  gchar *pixmapfile;
  GtkItemFactoryCallback1 callback;
} ToolbarItem;
/*< end_listing="ToolbarItem" >*/

typedef gboolean (*FileselOKFunc) (const char *filename, gpointer data);

GtkWidget * toolbar_create      (ToolbarItem    *items,
				 gint            nitems,
				 gpointer        callback_data);
GtkWidget * make_text_frame     (gchar          *labeltext,
				 GtkAccelGroup  *accel_group,
				 GtkWidget     **text);
GtkWidget * make_pair           (gchar          *labeltext,
				 GtkAccelGroup  *accel_group,
				 GtkWidget      *widget);
guint32     get_event_time      (void);
void        filesel_run         (GtkWindow      *parent,
				 const char     *title,
				 const char     *start_file,
				 FileselOKFunc   func,
				 gpointer        data);
gboolean    msgbox_run          (GtkWindow      *parent,
				 const char     *message,
				 const char     *yes_button,
				 const char     *no_button,
				 gint            default_index);
void        add_editable_revert (GtkEditable    *editable);

extern const char *addressbook_log_domain;
