/* this file is meant for code snippets that should
 * compile but aren't part of some broader context
 */


#include "part_iii/myphrase.h"
#include <gtk/gtk.h>

extern void myphrase_test_1 (void);
void
myphrase_example (void)
{
  /*< begin_listing="MY_PHRASE-example" >*/
  GtkWidget *phrase;

  phrase = my_phrase_new ("A new widget is born.");
  gtk_widget_show (phrase);
  my_phrase_set_emphasize (MY_PHRASE (phrase), TRUE);
  /*< end_listing="MY_PHRASE-example" >*/
}
