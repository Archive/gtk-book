
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


typedef struct  {
  gchar *firstname;
  gchar *lastname;
  gchar *email;
  gchar *phone;
} AddressEntry;


/* Global widgets */

GtkWidget *firstname_entry;
GtkWidget *lastname_entry;
GtkWidget *email_entry;
GtkWidget *phone_entry;


typedef struct {
  gchar *text;
  gchar *tooltip_text;
  gchar *pixmapfile;
  GtkSignalFunc callback;
} ToolBarItem;



/**********************************
 * Callbacks                      *
 **********************************/

static void 
do_quit (GtkWidget *widget, gpointer data)
{
  gtk_main_quit ();
}


static gint
delete_callback (GtkWidget *widget, GdkEvent *event, gpointer data)
{
  do_quit (widget, NULL);
  return TRUE;
}

static void
do_print (GtkWidget *widget, gpointer data)
{
  g_print ("\n");
  g_print ("First Name:\t%s\n", gtk_entry_get_text (GTK_ENTRY(firstname_entry)));
  g_print ("Last Name:\t%s\n", gtk_entry_get_text (GTK_ENTRY(lastname_entry)));
  g_print ("Phone Number:\t%s\n", gtk_entry_get_text (GTK_ENTRY(phone_entry)));
  g_print ("Email Address:\t%s\n", gtk_entry_get_text (GTK_ENTRY(email_entry)));
  g_print ("\n");
}


/**********************************
 * Creation of the User Interface *
 **********************************/


GtkWidget *
make_pair (gchar *labeltext, GtkWidget *widget)
{
  GtkWidget *util_hbox;
  GtkWidget *label;

  util_hbox = gtk_hbox_new (FALSE, 2);

  label = gtk_label_new (labeltext);
  gtk_box_pack_start (GTK_BOX (util_hbox), label,  FALSE, FALSE, 0);

  gtk_box_pack_start (GTK_BOX (util_hbox), widget, TRUE, TRUE, 0);

  return util_hbox;
}


GtkWidget *
make_text_frame (gchar *labeltext, GtkWidget **text)
{
  GtkWidget *frame;
  GtkWidget *util_vbox;
  GtkWidget *scrollbar;
  GtkWidget *util_hbox;
  GtkWidget *label;

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);

  util_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (frame), util_vbox);
  gtk_container_border_width (GTK_CONTAINER (util_vbox), 5);
  
  label = gtk_label_new (labeltext);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (util_vbox), label, FALSE, FALSE, 0);

  util_hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (util_vbox), util_hbox, TRUE, TRUE, 0);

  *text = gtk_text_new (NULL, NULL);
  gtk_text_set_editable (GTK_TEXT (*text), TRUE);
  gtk_box_pack_start (GTK_BOX (util_hbox), *text, TRUE, TRUE, 0);

  scrollbar = gtk_vscrollbar_new (GTK_TEXT (*text)->vadj);
  gtk_box_pack_start (GTK_BOX (util_hbox), scrollbar, FALSE, FALSE, 0);

  return frame;
}


static void
field_entry_apply (GtkEntry *entry_widget,
		   guint     field_offset)
{

  g_print ("Applied:\t\"%s\"\n", gtk_entry_get_text (entry_widget));

  return;
}

GtkWidget*
make_field_entry (gchar      *labeltext,
		  GtkWidget **entry,
		  guint	      field_offset)
{
  *entry = gtk_entry_new ();

  gtk_signal_connect (GTK_OBJECT (*entry),
		      "activate",
		      GTK_SIGNAL_FUNC (field_entry_apply),
		      GUINT_TO_POINTER (field_offset));

  return make_pair (labeltext, *entry);
}



int
main (int    argc,
      char **argv)
{
  GtkWidget *window;
  GtkWidget *main_vbox;
  GtkWidget *frame;
  GtkWidget *util_hbox;
  GtkWidget *util_vbox;
  GtkWidget *button;


  gtk_init (&argc, &argv);

  /* gtk_rc_parse (NULL); */
  
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), "AddressBook");
  gtk_window_set_policy (GTK_WINDOW (window), TRUE, TRUE, FALSE);
  gtk_signal_connect (GTK_OBJECT (window), "delete_event", 
		      GTK_SIGNAL_FUNC (delete_callback), NULL);
  gtk_quit_add_destroy (1, GTK_OBJECT (window));
  
  /* Now fill in the contents of the main window */

  main_vbox = gtk_vbox_new (FALSE, 0);

  gtk_container_add (GTK_CONTAINER (window), main_vbox);

  
  /* frame for utils. */
  
  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);

  gtk_box_pack_start (GTK_BOX (main_vbox), frame, FALSE, FALSE, 0);

  util_vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), util_vbox);
  gtk_container_border_width (GTK_CONTAINER (util_vbox), 10);
  
  
  /* Name text entries */

  frame = gtk_frame_new ("Name");
  gtk_box_pack_start (GTK_BOX (util_vbox), frame, FALSE, FALSE, 0);

  util_hbox = gtk_hbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), util_hbox);
  gtk_container_border_width (GTK_CONTAINER (util_hbox), 5);
  
  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_field_entry ("First:",
					&firstname_entry,
					GTK_STRUCT_OFFSET (AddressEntry, firstname)),
		      TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_field_entry ("Last:",
					&lastname_entry,
					GTK_STRUCT_OFFSET (AddressEntry, lastname)),
		      TRUE, TRUE, 0);

  /* Contact info text entry */

  frame = gtk_frame_new ("Contact Info");
  gtk_box_pack_start (GTK_BOX (util_vbox), frame, FALSE, FALSE, 0);

  util_hbox = gtk_hbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), util_hbox);
  gtk_container_border_width (GTK_CONTAINER (util_hbox), 5);
  
  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_field_entry ("Email:",
					&email_entry,
					GTK_STRUCT_OFFSET (AddressEntry, email)),
		      TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_field_entry ("Phone:",
					&phone_entry,
					GTK_STRUCT_OFFSET (AddressEntry, phone)),
		      TRUE, TRUE, 0);


  /* Quit and Print buttons */
  util_hbox = gtk_hbox_new (TRUE, 5);
  gtk_box_pack_start (GTK_BOX (main_vbox), util_hbox, FALSE, FALSE, 0);
  
  button = gtk_button_new_with_label ("Quit");
  gtk_box_pack_start (GTK_BOX (util_hbox), button, FALSE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (do_quit), NULL);

  button = gtk_button_new_with_label ("Print");
  gtk_box_pack_start (GTK_BOX (util_hbox), button, FALSE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (do_print), NULL);

  gtk_widget_show_all (window);

  gtk_main ();

  return 0;
}

