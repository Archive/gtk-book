#include <sys/param.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

typedef struct  {
  gchar *firstname;
  gchar *lastname;
  gchar *email;
  gchar *phone;
  gint   birth_day;
  gint   birth_month;
  gchar *address;
  gchar *comments;
} AddressEntry;

enum {
  TARGET_STRING,
  TARGET_ADDRESS_BOOK
};

/* Entry currently on the clipboard */
GString *clipboard = NULL;

GList *entries = NULL;

/* Global widgets */

GtkWidget *window;
GtkWidget *the_clist;
gint       clist_current_row = -1;

GtkWidget *firstname_entry;
GtkWidget *lastname_entry;
GtkWidget *email_entry;
GtkWidget *phone_entry;

GtkWidget *day_spinner;
GtkWidget *month_combo;

GtkWidget *address_text;
GtkWidget *comments_text;

static void do_new (void);
static void do_open (void);
static void do_save (void);
static void do_save_as (void);
static void do_quit (void);
static void do_about (void);
static void do_new_entry (void);
static void do_back (void);
static void do_forward (void);
static void do_cut (void);
static void do_copy (void);
static void do_paste (void);

static void do_popup (gpointer callback_data, guint callback_action, GtkWidget *widget);

static void select_row_cb (GtkCList *clist, gint row, gint column, GdkEventButton *event);

typedef struct {
  gchar *text;
  gchar *tooltip_text;
  gchar *pixmapfile;
  GtkSignalFunc callback;
} ToolBarItem;

static gchar *the_file_name = "./testbook.adr";

ToolBarItem toolbar_items[] = {
  { "Back", "Previous entry", "pixmaps/back.xpm", GTK_SIGNAL_FUNC (do_new_entry) },
  { "Forward", "Next entry", "pixmaps/forward.xpm", GTK_SIGNAL_FUNC (do_back) },
  { "New", "Create a new entry", "pixmaps/new.xpm", GTK_SIGNAL_FUNC (do_forward) },
  { "Cut", "Cut entry", "pixmaps/cut.xpm", GTK_SIGNAL_FUNC (do_cut) },
  { "Copy", "Copy entry", "pixmaps/copy.xpm", GTK_SIGNAL_FUNC (do_copy) },
  { "Paste", "Paste entry", "pixmaps/paste.xpm", GTK_SIGNAL_FUNC (do_paste) }
};

static int ntoolbar_items = sizeof (toolbar_items) / sizeof (toolbar_items[0]);

/*< begin_listing="item-array" >*/
static GtkItemFactoryEntry menu_items[] =
{
  /* menu-path		 accelerator    callback + action item-type */
  { "/_File",		 NULL,		NULL, 0,	  "<Branch>" },
  { "/File/_New",	 "<Control>N",	do_new, 0,	  NULL },
  { "/File/_Open",	 "<Control>O",	do_open, 0,	  NULL },
  { "/File/_Save", 	 "<Control>S",	do_save, 0,	  NULL },
  { "/File/Save _As...", NULL,		do_save_as, 0,	  NULL },
  { "/File/sep1",	 NULL,		NULL, 0,	  "<Separator>" },
  { "/File/_Quit",	 "<Control>Q",	do_quit, 0,	  NULL },

  { "/_Entry",		 NULL,	        NULL, 0,	  "<Branch>" },
  { "/Entry/_New",	 NULL,		do_new_entry, 0,  NULL },
  { "/Entry/_Back",	 NULL,		do_back, 0,	  NULL },
  { "/Entry/_Forward",	 NULL,		do_forward, 0,	  NULL },
  { "/Entry/Cu_t",	 "<Control>C",	do_cut, 0,	  NULL },
  { "/Entry/_Copy",	 "<Control>X",	do_copy, 0,	  NULL },
  { "/Entry/_Paste",	 "<Control>V",	do_paste, 0,	  NULL }, 

  { "/_Help",		 NULL,		NULL, 0,	  "<LastBranch>" },
  { "/Help/_About",	 NULL,		do_about, 0,	  NULL },
};

static int nmenu_items = sizeof (menu_items) / sizeof (menu_items[0]);
/*< end_listing="item-array" >*/

/*< begin_listing="popup-item-array" >*/
typedef enum {
  POPUP_CUT,
  POPUP_COPY,
  POPUP_PASTE
} PopupAction;

static GtkItemFactoryEntry popup_menu_items[] =
{
  { "Cu_t",   NULL  do_popup, POPUP_CUT,   NULL },
  { "_Copy",  NULL, do_popup, POPUP_COPY,  NULL },
  { "_Paste", NULL, do_popup, POPUP_PASTE, NULL }, 
}

static int npopup_menu_items = sizeof (menu_items) / sizeof (menu_items[0]);
/*< end_listing="popup-item-array" >*/

enum {
  TOKEN_INVALID = G_TOKEN_LAST,
  TOKEN_ENTRY,
  TOKEN_FIRSTNAME,
  TOKEN_LASTNAME,
  TOKEN_EMAIL,
  TOKEN_PHONE,
  TOKEN_BIRTH_DAY,
  TOKEN_BIRTH_MONTH,
  TOKEN_ADDRESS,
  TOKEN_COMMENTS,
};
#define	FIELD_TOKEN_FIRST TOKEN_FIRSTNAME
#define	FIELD_TOKEN_LAST  TOKEN_COMMENTS

enum {
  CLIST_COL_FIRSTNAME,
  CLIST_COL_LASTNAME,
  CLIST_COL_PHONE,
  CLIST_COL_EMAIL,
  CLIST_N_COLS
};

static gchar *clist_column_names[CLIST_N_COLS] =
{
  "First",
  "Last",
  "Phone",
  "EMail",
};

static guint clist_column_widths[CLIST_N_COLS] =
{
  40, 60, 60, 100,
};

static struct
{
  gchar *name;
  gint token;
} symbols[] = {
  { "entry", TOKEN_ENTRY },
  { "firstname", TOKEN_FIRSTNAME },
  { "lastname", TOKEN_LASTNAME },
  { "phone", TOKEN_PHONE },
  { "email", TOKEN_EMAIL },
  { "address", TOKEN_ADDRESS },
  { "birth_day", TOKEN_BIRTH_DAY },
  { "birth_month", TOKEN_BIRTH_MONTH },
  { "comments", TOKEN_COMMENTS },
};
static guint nsymbols = sizeof (symbols) / sizeof (symbols[0]);

static gchar *months[] = {
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
};


/* allocate and initialize a new adress entry structure */
static AddressEntry*
address_entry_new (void)
{
  AddressEntry *entry;

  entry = g_new0 (AddressEntry, 1);
  entry->firstname = NULL;
  entry->lastname = NULL;
  entry->email = NULL;
  entry->phone = NULL;
  entry->birth_day = 0;
  entry->birth_month = 0;
  entry->address = NULL;
  entry->comments = NULL;

  return entry;
}

/* free an address entry */
static void
address_entry_free (AddressEntry *entry)
{
  g_free (entry->firstname);
  g_free (entry->lastname);
  g_free (entry->email);
  g_free (entry->phone);
  g_free (entry->address);
  g_free (entry->comments);
  g_free (entry);
}

/* Get the timestamp of the current event. Actually, the only thing
 * we really care about below is the key event
 */
static guint32
get_event_time (void)
{
  GdkEvent *event;
  guint32 result = GDK_CURRENT_TIME;
  
  event = gtk_get_current_event();
  if (event)
    {
      result = gdk_event_get_time (event);
      gdk_event_free (event);
    }
  
  return result;
}

/************************************
 * Functions for reading in entries *
 ************************************/

static guint
str_to_month (gchar *str)
{
  guint i;
  
  for (i = 0; i < 12; i++)
    {
      if (!g_strcasecmp (months[i], str))
	return i + 1;
    }

  return 0;
}

static GScanner*
make_entry_scanner (gchar *input_name)
{
  GScanner *scanner;
  gint i;

  scanner = g_scanner_new (NULL);
  scanner->config->symbol_2_token = TRUE;
  scanner->input_name = input_name;

  for (i = 0; i < nsymbols; i++)
    g_scanner_add_symbol (scanner, symbols[i].name, GINT_TO_POINTER (symbols[i].token));

  return scanner;
}

static GTokenType
parse_entry (GScanner     *scanner,
	     AddressEntry *entry)
{
  GTokenType token;

  token = g_scanner_get_next_token (scanner);
  if (token != G_TOKEN_LEFT_CURLY)
    return G_TOKEN_LEFT_CURLY;

  token = g_scanner_get_next_token (scanner);
  while (token != G_TOKEN_RIGHT_CURLY)
    {
      GTokenType field_token;

      /* inside the entry definition we only accept fieldnames as
       * defined in the `symbols' array, all of which have
       * a token within the limits of FIELD_TOKEN_FIRST and
       * FIELD_TOKEN_LAST.
       */
      if (token < FIELD_TOKEN_FIRST ||
	  token > FIELD_TOKEN_LAST)
	return G_TOKEN_SYMBOL;

      field_token = token;

      token = g_scanner_get_next_token (scanner);
      if (token != G_TOKEN_EQUAL_SIGN)
	return G_TOKEN_EQUAL_SIGN;
      
      if (field_token == TOKEN_BIRTH_DAY)
	{
	  token = g_scanner_get_next_token (scanner);
	  if (token != G_TOKEN_INT)
	    return G_TOKEN_INT;
	  
	  entry->birth_day = CLAMP (scanner->value.v_int, 1, 31);
	  if (entry->birth_day != scanner->value.v_int)
	    g_scanner_warn (scanner, "birth day `%ld' out of range", scanner->value.v_int);
	}
      else if (field_token == TOKEN_BIRTH_MONTH)
	{
	  guint month;
	  
	  token = g_scanner_get_next_token (scanner);
	  if (token != G_TOKEN_IDENTIFIER)
	    return G_TOKEN_IDENTIFIER;

	  month = str_to_month (scanner->value.v_identifier);
	  if (month == 0)
	    g_scanner_warn (scanner, "invalid month specification \"%s\"", scanner->value.v_string);
	  else
	    entry->birth_month = month;
	}
      else
	{
	  token = g_scanner_get_next_token (scanner);
	  if (token != G_TOKEN_STRING)
	    return G_TOKEN_STRING;
	  
	  switch (field_token)
	    {
	    case TOKEN_FIRSTNAME:
	      entry->firstname = g_strdup (scanner->value.v_string);
	      break;
	      
	    case TOKEN_LASTNAME:
	      entry->lastname = g_strdup (scanner->value.v_string);
	      break;
	      
	    case TOKEN_EMAIL:
	      entry->email = g_strdup (scanner->value.v_string);
	      break;
	      
	    case TOKEN_PHONE:
	      entry->phone = g_strdup (scanner->value.v_string);
	      break;
	      
	    case TOKEN_BIRTH_DAY:
	      entry->birth_day = atoi (scanner->value.v_string);
	      break;
	      
	    case TOKEN_ADDRESS:
	      entry->address = g_strdup (scanner->value.v_string);
	      break;
	      
	    case TOKEN_COMMENTS:
	      entry->comments = g_strdup (scanner->value.v_string);
	      break;
	      
	    default:
	      g_assert_not_reached ();
	      break;
	    }
	}

      token = g_scanner_get_next_token (scanner);
    }

  return G_TOKEN_NONE;
}

static gboolean
parse_entries (GScanner *scanner,
	       GList   **result)
{
  GTokenType expected_token;
      
  *result = NULL;

  /* parse until either the end of the input is reached, or
   * until expected_token is not equal to G_TOKEN_NONE, in
   * which case the parsing code encountered an error condition.
   */
  expected_token = G_TOKEN_NONE;
  while (expected_token == G_TOKEN_NONE &&
	 !g_scanner_eof (scanner))
    {
      GTokenType token;

      token = g_scanner_get_next_token (scanner);
      if (token == TOKEN_ENTRY)
	{
	  AddressEntry *new_entry = NULL;
	  
	  new_entry = address_entry_new ();
	  
	  expected_token = parse_entry (scanner, new_entry);
	  if (expected_token == G_TOKEN_NONE)
	    *result = g_list_append (*result, new_entry);
	  else
	    address_entry_free (new_entry);
	}
      else if (token != G_TOKEN_EOF)
	{
	  /* we (currently?) only feature "entry {...}" portions, and
	   * would expect the file's end otherwise.
	   */
	  expected_token = G_TOKEN_EOF;
	}
    }
  
  if (expected_token != G_TOKEN_NONE)
    {
      /* we use identifiers for months only, thus we pass
       * identifier_spec as "month name".
       * we use symbols for field names only, thus we pass
       * symbol_spec as "field name".
       * since we abort on unexpected tokens, we pass is_error
       * as TRUE.
       */
      g_scanner_unexp_token (scanner,
			     expected_token,
			     "month name",
			     "field name",
			     NULL,
			     "aborting...",
			     TRUE);
    }

  return *result != NULL;
}

static void
update_row (GtkCList *clist,
	    gint      row)
{
  AddressEntry *entry;

  entry = gtk_clist_get_row_data (clist, row);

  if (entry)
    {
      gtk_clist_set_text (clist, row, CLIST_COL_FIRSTNAME, entry->firstname ? entry->firstname : "");
      gtk_clist_set_text (clist, row, CLIST_COL_LASTNAME, entry->lastname ? entry->lastname : "");
      gtk_clist_set_text (clist, row, CLIST_COL_PHONE, entry->phone ? entry->phone : "");
      gtk_clist_set_text (clist, row, CLIST_COL_EMAIL, entry->email ? entry->email : "");
    }
}

static void
add_entries (GtkCList *clist,
	     GList    *new_entries,
	     gboolean select)
{
  GList *tmp_list;
  gint row = clist_current_row;
  
  if (!new_entries)
    return;

  gtk_clist_freeze (clist);

  entries = g_list_concat (entries, new_entries);

  if (select)
    {
      gtk_clist_unselect_all (clist);
      gtk_signal_handler_block_by_func (GTK_OBJECT (clist), 
					select_row_cb, 
					NULL);
    }

  tmp_list = new_entries;
  while (tmp_list)
    {
      gchar *data[CLIST_N_COLS];
      AddressEntry *entry = tmp_list->data;
      guint i;

      for (i = 0; i < CLIST_N_COLS; i++)
	data[i] = NULL;
      
      row = gtk_clist_insert (clist, row + 1, data);
      gtk_clist_set_row_data (clist, row, entry);

      update_row (clist, row);

      if (select)
	gtk_clist_select_row (clist, row, 0);
      
      tmp_list = tmp_list->next;
    }

  if (select)
    {
      gtk_signal_handler_unblock_by_func (GTK_OBJECT (clist), 
					  select_row_cb, 
					  NULL);
      gtk_clist_select_row (clist, row, 0);
    }

  gtk_clist_thaw (clist);
}

static void
open_file (gchar *filename)
{
  GList *new_entries;
  gint fd;
  GScanner *scanner;
  
  fd = open (filename, O_RDONLY);
  
  if (fd < 0)
    {
      g_warning ("Can't open %s: %s\n", filename, g_strerror(errno));
      return;
    }
  
  scanner = make_entry_scanner (filename);
  
  g_scanner_input_file (scanner, fd);

  if (parse_entries (scanner, &new_entries))
    {
      entries = NULL;
      gtk_clist_clear (GTK_CLIST (the_clist));

      add_entries (GTK_CLIST (the_clist), new_entries, FALSE);

      if (entries)
	gtk_clist_select_row (GTK_CLIST (the_clist), 0, 0);
    }

  g_scanner_destroy (scanner);
  close (fd);
}

/*************************************
 * Functions for writing out entries *
 *************************************/

static void
append_text_field (GString *string,
		   gchar   *field_name,
		   gchar   *field,
		   gboolean append_quoted)
{
  if (field)
    {
      gchar *ch;

      g_string_append (string, "  ");
      g_string_append (string, field_name);
      g_string_append (string, append_quoted ? " = \"" : " = ");

      /* walk each character of the field's contents and escape
       * special characters
       */
      for (ch = field; *ch; ch++)
	{
	  switch (*ch)
	    {
	    case '\\':
	      g_string_append (string, "\\\\");
	      break;
	    case '\n':
	      g_string_append (string, "\\n");
	      break;
	    default:
	      g_string_append_c (string, *ch);
	    }
	}
  
      g_string_append (string, append_quoted ? "\"\n" : "\n");
    }
}

static void
append_entry (GString      *string,
	      AddressEntry *entry)
{
  g_string_append (string, "entry {\n");

  append_text_field (string, "firstname", entry->firstname, TRUE);
  append_text_field (string, "lastname", entry->lastname, TRUE);
  append_text_field (string, "phone", entry->phone, TRUE);
  append_text_field (string, "email", entry->email, TRUE);
  append_text_field (string, "address", entry->address, TRUE);

  if (entry->birth_day)
    g_string_sprintfa (string, "  birth_day = %d\n", entry->birth_day);

  if (entry->birth_month)
    append_text_field (string, "birth_month", months[entry->birth_month - 1], FALSE);

  append_text_field (string, "comments", entry->comments, TRUE);

  g_string_append (string, "}\n\n");
}

static void
append_selection (GString      *string)
{
  GList *tmp_list;

  tmp_list = GTK_CLIST (the_clist)->selection;
  while (tmp_list)
    {
      gint current_row;
      AddressEntry *entry;
      
      current_row = GPOINTER_TO_INT (tmp_list->data);
      entry = gtk_clist_get_row_data (GTK_CLIST (the_clist), current_row);
      
      append_entry (string, entry);
      
      tmp_list = tmp_list->next;
    }
}

static void
save_file (gchar *file_name)
{
  GList *list;
  FILE *f_out;
  GString *gstring;

  f_out = fopen (file_name, "w");
  if (!f_out)
    {
      g_warning ("failed to open \"%s\": %s", file_name, g_strerror (errno));
      return;
    }

  gstring = g_string_new ("# addressbook data file\n\n");
  for (list = entries; list; list = list->next)
    append_entry (gstring, list->data);

  fputs (gstring->str, f_out);

  g_string_free (gstring, TRUE);

  fclose (f_out);
}

/******************
 * Menu Callbacks *
 ******************/

static void 
do_new (void)
{
}

static void 
do_open (void)
{
}

static void 
do_save (void)
{
  save_file (the_file_name);
}

static void
do_save_as (void)
{
}

/*< begin_listing="do_quit" >*/
/* Callback when the user clicks "Yes" in the "Really Quit?" dialog.
 */
static void
quit_yes_cb (GtkWidget *widget, gboolean *really_quit)
{
  *really_quit = TRUE;
  gtk_main_quit();
}

static void 
do_quit (void)
{
  gboolean really_quit = FALSE;
  GtkWidget *dialog;
  GtkWidget *button;
  GtkWidget *label;
  guint handler_id;

  /* Create a dialog
   */
  dialog = gtk_dialog_new ();
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (window));
  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);

  /* Quit our recursive main loop if the dialog is destroyed.
   */
  handler_id = gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
				   GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
  /* Set 'dialog' to NULL if the dialog is destroyed
   */
  gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
		      GTK_SIGNAL_FUNC (gtk_widget_destroyed), &dialog);

  /* Fill the dialog with a label and yes/no buttons
   */
  label = gtk_label_new ("Really quit?");
  gtk_misc_set_padding (GTK_MISC (label), 10, 10);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), label, 
		      TRUE, TRUE, 0);
  
  button = gtk_button_new_with_label ("Yes");
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area), button,
		      TRUE, FALSE, 0);

  /* call the quit_yes_cb when Yes is clicked. 
   * Which sets the really_quit variable, then quits the main loop
   */
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (quit_yes_cb), &really_quit);

  button = gtk_button_new_with_label ("No");
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area), button,
		      TRUE, FALSE, 0);

  /* When no is clicked, quit the main loop.
   */
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

  gtk_widget_show_all (dialog);

  /* Run a recursive main loop until a button is clicked
   * or the user destroys the dialog through the window mananger */
  gtk_main ();
  
  if (dialog)
    {
      /* Disconnect the handler, so we don't quit the outer main loop
       * when we destroy the dialog */
      gtk_signal_disconnect (GTK_OBJECT (dialog), handler_id);

      gtk_widget_destroy (dialog);
    }

  if (really_quit)
    gtk_main_quit ();
}
/*< end_listing="do_quit" >*/

static void 
do_about (void)
{
}

static void 
do_new_entry (void)
{
}

static void 
do_back (void)
{
}

static void 
do_forward (void)
{
}

static void 
do_cut (void)
{
  GList *tmp_list;

  do_copy (widget, NULL);

  tmp_list = GTK_CLIST (the_clist)->selection;
  while (tmp_list)
    {
      gint current_row;
      AddressEntry *entry;
      
      current_row = GPOINTER_TO_INT (tmp_list->data);

      if (clist_current_row == current_row)
	clist_current_row--;

      entry = gtk_clist_get_row_data (GTK_CLIST (the_clist), current_row);

      tmp_list = tmp_list->next;

      gtk_clist_remove (GTK_CLIST (the_clist), current_row);
      entries = g_list_remove (entries, entry);
    }

  if (entries)
    gtk_clist_select_row (GTK_CLIST (the_clist), 
			  clist_current_row >= 0 ? clist_current_row : 0, 0);
}

/*< begin_listing="do_copy" >*/
static void
do_copy (void)
{
  if (GTK_CLIST (the_clist)->selection)
    {
      if (!clipboard)
	clipboard = g_string_new (NULL);
      else
	g_string_truncate (clipboard, 0);

      append_selection (clipboard);

      gtk_selection_owner_set (the_clist,
			       gdk_atom_intern ("CLIPBOARD", FALSE),
			       get_event_time ());
    }
}
/*< end_listing="do_copy" >*/

/*< begin_listing="do_paste" >*/
static void 
do_paste (void)
{
  gtk_selection_convert (the_clist,
			 gdk_atom_intern ("CLIPBOARD", FALSE),
			 gdk_atom_intern ("_GTK_ADDRESS_BOOK", FALSE),
			 get_event_time());
}
/*< end_listing="do_paste" >*/

static void
do_popup (gpointer callback_data,
	  guint callback_action,
	  GtkWidget *widget)
{
}

static void
select_row_cb (GtkCList       *clist,
	       gint            row,
	       gint            column,
	       GdkEventButton *event)
{
  AddressEntry *entry = gtk_clist_get_row_data (clist, row);
  gchar buffer[32];

  clist_current_row = row;

  if (entry->firstname)
    gtk_entry_set_text (GTK_ENTRY (firstname_entry), entry->firstname);
  else
    gtk_entry_set_text (GTK_ENTRY (firstname_entry), "");

  if (entry->lastname)
    gtk_entry_set_text (GTK_ENTRY (lastname_entry), entry->lastname);
  else
    gtk_entry_set_text (GTK_ENTRY (lastname_entry), "");

  if (entry->email)
    gtk_entry_set_text (GTK_ENTRY (email_entry), entry->email);
  else
    gtk_entry_set_text (GTK_ENTRY (email_entry), "");

  if (entry->phone)
    gtk_entry_set_text (GTK_ENTRY (phone_entry), entry->phone);
  else
    gtk_entry_set_text (GTK_ENTRY (phone_entry), "");

  if (entry->birth_day)
    {
      sprintf(buffer, "%d", entry->birth_day);
      gtk_entry_set_text (GTK_ENTRY (day_spinner), buffer);
    }
  else
    gtk_entry_set_text (GTK_ENTRY (day_spinner), "0");

  if (entry->birth_month)
    gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (month_combo)->entry), 
			months[entry->birth_month-1]);
  else
    gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (month_combo)->entry),"");

  gtk_editable_delete_text (GTK_EDITABLE (address_text), 0, -1);
  if (entry->address)
    {
      gtk_text_insert (GTK_TEXT (address_text), NULL, NULL, NULL,
		       entry->address, -1);
    }

  gtk_editable_delete_text (GTK_EDITABLE (comments_text), 0, -1);
  if (entry->comments)
    {
      gtk_text_insert (GTK_TEXT (comments_text), NULL, NULL, NULL,
		       entry->comments, -1);
    }
}

static gint
delete_callback (GtkWidget *widget, GdkEvent *event, gpointer data)
{
  do_quit (widget, NULL);
  return TRUE;
}

/***********************************************
 * Callbacks for Cut 'n Paste and Drag 'n Drop *
 ***********************************************/

/* Handler called to supply contents of clipboard
 */
/*< begin_listing="selection_get" >*/
void
selection_get (GtkWidget        *widget,
	       GtkSelectionData *selection_data,
	       guint             info,
	       guint             time)
{
  if (clipboard)
    gtk_selection_data_set (selection_data, GDK_SELECTION_TYPE_STRING,
			    8, clipboard->str, clipboard->len);
}
/*< end_listing="selection_get" >*/

/* Handler called when clipboard data is received, or request
 * for data was rejected.
 */
/*< begin_listing="selection_received" >*/
static void
selection_received  (GtkWidget         *widget,
		     GtkSelectionData  *selection_data)
{
  gchar *contents = NULL;
  gint length = 0;
  
  if ((selection_data->length < 0) ||
      (selection_data->type != GDK_SELECTION_TYPE_STRING))
    {
      /* Remote retrieval failed, try using local data */
      
      if (clipboard && clipboard->len > 0)
	{
	  contents = clipboard->str;
	  length = clipboard->len;
	}
    }
  else				/* Success */
    {
      contents = selection_data->data;
      length = selection_data->length;
    }

  if (contents)
    {
      GScanner *scanner;
      GList *new_entries;
      
      scanner = make_entry_scanner ("pasted text");
      
      g_scanner_input_text (scanner, contents, length);
      
      if (parse_entries (scanner, &new_entries))
	add_entries (GTK_CLIST (the_clist), new_entries, TRUE);
      
      g_scanner_destroy (scanner);

      if (entries && !GTK_CLIST (the_clist)->selection)
	gtk_clist_select_row (GTK_CLIST (the_clist), 0, 0);
    }
}
/*< end_listing="selection_received" >*/

/* Handler called to provide contents of a drag
 */
void
drag_data_get (GtkWidget        *widget,
	       GdkDragContext   *context,
	       GtkSelectionData *selection_data,
	       guint             info,
	       guint             time)
{
  GString *result = g_string_new (NULL);

  append_selection (result);

  gtk_selection_data_set (selection_data, GDK_SELECTION_TYPE_STRING,
			  8, result->str, result->len);

  g_string_free (result, TRUE);
}

/* Handler called when a drop is received
 */
void
drag_data_received (GtkWidget        *widget,
		    GdkDragContext   *context,
		    gint              x,
		    gint              y,
		    GtkSelectionData *selection_data,
		    guint             info,
		    guint             time)
{
  GScanner *scanner;
  GList *new_entries;
  
  scanner = make_entry_scanner ("dragged text");
  
  g_scanner_input_text (scanner,
			selection_data->data, 
			selection_data->length);
  
  if (parse_entries (scanner, &new_entries))
    add_entries (GTK_CLIST (the_clist), new_entries, TRUE);
  
  g_scanner_destroy (scanner);
  
  if (entries && !GTK_CLIST (the_clist)->selection)
    gtk_clist_select_row (GTK_CLIST (the_clist), 0, 0);
}

/* Handler called when a drop is received
 */
/*< begin_listing="filename-drop" >*/
void
app_drag_data_received (GtkWidget        *widget,
			GdkDragContext   *context,
			gint              x,
			gint              y,
			GtkSelectionData *selection_data,
			guint             info,
			guint             time)
{
  gchar *text = (gchar *)selection_data->data;
  gchar *filename = NULL;
  size_t length = strlen (text);
  gchar *p;
  gchar *tmp;

  /* Extract the first URI from the uri-list
   */
  if ((p = strchr (text, '\r')))
    length = p - text;
  if ((p = strchr (text, '\n')))
    length = MIN (length, p - text);

  if (strncmp (text, "file://", 7) == 0)
    {
      /* File URL including hostname. We only want to try and
       * load the file if it is a local URL.
       */
      gchar hostname[MAXHOSTNAMELEN+1];
      gethostname (hostname, MAXHOSTNAMELEN);
      hostname[MAXHOSTNAMELEN] = '\0';

      if (strncmp (text + 7, hostname, strlen (hostname)) == 0 &&
	  *(text + 7 + strlen (hostname)) == '/')
	{
	  filename = text + 7 + strlen(hostname) + 1;
	  length -= 7 + strlen(hostname) + 1;
	}
    }
  else if (strncmp (text, "file:/", 6) == 0)
    {
      /* File URL without hostname */
      filename = text + 5;
      length -= 5;
    }

  if (filename)
    {
      /* We have a filename */
      tmp = g_strndup (filename, length);
      open_file (tmp);
      g_free (tmp);
    }
}
/*< end_listing="filename-drop" >*/

/**********************************
 * Creation of the User Interface *
 **********************************/

void
toolbar_append_item (GtkToolbar *toolbar, ToolBarItem *item)
{
  GtkWidget *pwidget = NULL;
/*< begin_listing="create_pixmap" >*/
  GdkPixmap *pixmap;
  GdkBitmap *mask;
  
  pixmap = gdk_pixmap_colormap_create_from_xpm (NULL,
						gtk_widget_get_colormap (GTK_WIDGET (toolbar)),
						&mask,
						NULL,
						item->pixmapfile);
/*< end_listing="create_pixmap" >*/
  if (pixmap)
    {
      pwidget = gtk_pixmap_new (pixmap, mask);
      gdk_pixmap_unref (pixmap);
      gdk_bitmap_unref (mask);
    }

  gtk_toolbar_append_item (toolbar, 
			   item->text, item->tooltip_text, NULL,
			   pwidget, item->callback, NULL);
}

GtkWidget *
make_pair (gchar *labeltext, GtkWidget *widget)
{
  GtkWidget *util_hbox;
  GtkWidget *label;

  util_hbox = gtk_hbox_new (FALSE, 2);

  label = gtk_label_new (labeltext);
  gtk_box_pack_start (GTK_BOX (util_hbox), label,  FALSE, FALSE, 0);

  gtk_box_pack_start (GTK_BOX (util_hbox), widget, TRUE, TRUE, 0);

  return util_hbox;
}

static void
field_entry_apply (GtkEntry *entry_widget,
		   guint     field_offset)
{
  AddressEntry *entry;

  entry = gtk_clist_get_row_data (GTK_CLIST (the_clist), clist_current_row);
  if (entry)
    {
      gchar **field_p = (gchar**) ((gchar*) entry + field_offset);

      g_free (*field_p);
      *field_p = g_strdup (gtk_entry_get_text (entry_widget));

      update_row (GTK_CLIST (the_clist), clist_current_row);
    }
}

void
insert_text_handler (GtkEditable *editable,
                     const gchar *text,
                     gint         length,
                     gint        *position,
                     gpointer     data)
{
  int i;
  gchar *result = g_new (gchar, length);

  for (i=0; i<length; i++)
    result[i] = islower(text[i]) ? toupper(text[i]) : text[i];

  gtk_signal_handler_block_by_func (GTK_OBJECT (editable),
				    GTK_SIGNAL_FUNC (insert_text_handler),
				    data);
  gtk_editable_insert_text (editable, result, length, position);
  gtk_signal_handler_unblock_by_func (GTK_OBJECT (editable),
				      GTK_SIGNAL_FUNC (insert_text_handler),
				      data);

  gtk_signal_emit_stop_by_name (GTK_OBJECT (editable), "insert_text");

  g_free (result);
}

GtkWidget*
make_field_entry (gchar      *labeltext,
		  GtkWidget **entry,
		  guint	      field_offset)
{
  *entry = gtk_entry_new ();

  gtk_signal_connect (GTK_OBJECT (*entry),
		      "activate",
		      GTK_SIGNAL_FUNC (field_entry_apply),
		      GUINT_TO_POINTER (field_offset));
  gtk_signal_connect (GTK_OBJECT (*entry),
		      "insert_text",
		      GTK_SIGNAL_FUNC (insert_text_handler),
		      NULL);

  return make_pair (labeltext, *entry);
}

GtkWidget *
make_text_frame (gchar *labeltext, GtkWidget **text)
{
  GtkWidget *frame;
  GtkWidget *util_vbox;
  GtkWidget *scrollbar;
  GtkWidget *util_hbox;
  GtkWidget *label;

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);

  util_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (frame), util_vbox);
  gtk_container_border_width (GTK_CONTAINER (util_vbox), 5);
  
  label = gtk_label_new (labeltext);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (util_vbox), label, FALSE, FALSE, 0);

  util_hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (util_vbox), util_hbox, TRUE, TRUE, 0);

  *text = gtk_text_new (NULL, NULL);
  gtk_text_set_editable (GTK_TEXT (*text), TRUE);
  gtk_box_pack_start (GTK_BOX (util_hbox), *text, TRUE, TRUE, 0);

  scrollbar = gtk_vscrollbar_new (GTK_TEXT (*text)->vadj);
  gtk_box_pack_start (GTK_BOX (util_hbox), scrollbar, FALSE, FALSE, 0);

  return frame;
}

GtkWidget*
create_clist (void)
{
  guint i;
  /*< begin_listing="adding_targets0" >*/
  static GtkTargetEntry targets[] = {
    { "_GTK_ADDRESS_BOOK", 0, TARGET_ADDRESS_BOOK },
    { "STRING", 0, TARGET_STRING },
    { "TEXT", 0, TARGET_STRING },
  };
  /*< end_listing="adding_targets0" >*/
  static int n_targets = 3;

  /*< begin_listing="binding-set1" >*/
  GtkBindingSet *binding_set;

  /* Add user signals to the CList class for cut, copy and paste */

  gtk_object_class_user_signal_new (gtk_type_class (gtk_clist_get_type ()),
				    "cut_address",
				    GTK_RUN_ACTION,
				    gtk_signal_default_marshaller,
				    GTK_TYPE_NONE /* `void' return.*/ ,
				    0 /* No extra arguments */);

  gtk_object_class_user_signal_new (gtk_type_class (gtk_clist_get_type ()),
				    "copy_address",
				    GTK_RUN_ACTION,
				    gtk_signal_default_marshaller,
				    GTK_TYPE_NONE /* `void' return.*/ ,
				    0 /* No extra arguments */);

  gtk_object_class_user_signal_new (gtk_type_class (gtk_clist_get_type ()),
				    "paste_address",
				    GTK_RUN_ACTION,
				    gtk_signal_default_marshaller,
				    GTK_TYPE_NONE /* `void' return.*/ ,
				    0 /* No extra arguments */);
  /*< end_listing="binding-set1" >*/

  the_clist = gtk_clist_new_with_titles (CLIST_N_COLS, clist_column_names);
  for (i = 0; i < CLIST_N_COLS; i++)
    gtk_clist_set_column_width (GTK_CLIST (the_clist), i, clist_column_widths[i]);

  gtk_clist_set_selection_mode (GTK_CLIST (the_clist), GTK_SELECTION_EXTENDED);
  
  gtk_widget_set_usize (the_clist, 200, -1);

  gtk_widget_set_name (the_clist, "AddressList");

  gtk_signal_connect (GTK_OBJECT (the_clist), "select_row",
		      GTK_SIGNAL_FUNC (select_row_cb), NULL);
  
  /*< begin_listing="binding-set2" >*/
  gtk_signal_connect (GTK_OBJECT (the_clist), "cut_address",
		      GTK_SIGNAL_FUNC (do_cut), NULL);
  gtk_signal_connect (GTK_OBJECT (the_clist), "copy_address",
		      GTK_SIGNAL_FUNC (do_copy), NULL);
  gtk_signal_connect (GTK_OBJECT (the_clist), "paste_address",
		      GTK_SIGNAL_FUNC (do_paste), NULL);

  /* Equivalent to:
   *   binding "AddressList" { bind <control>x "cut-address" } 
   */

  binding_set = gtk_binding_set_new("AddressList");
  gtk_binding_entry_add_signal (binding_set, GDK_x, GDK_CONTROL_MASK,
				"cut-address", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_c, GDK_CONTROL_MASK,
				"copy-address", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_v, GDK_CONTROL_MASK,
				"paste-address", 0);
  
  /* Equivalent to: 
   *   widget "*.AddressList" binding : application "AddressList" 
   */
  gtk_binding_set_add_path (binding_set, GTK_PATH_WIDGET, 
			    "*.AddressList", GTK_PATH_PRIO_APPLICATION);
  /*< end_listing="binding-set2" >*/

  /* Set up handler for clipboard */

  /*< begin_listing="adding_targets1" >*/
  gtk_selection_add_targets (the_clist,
			     gdk_atom_intern ("CLIPBOARD", FALSE),
			     targets, n_targets);

  gtk_signal_connect (GTK_OBJECT (the_clist),
		      "selection_get",
		      GTK_SIGNAL_FUNC (selection_get),
		      NULL);
  /*< end_listing="adding_targets1" >*/

  gtk_signal_connect (GTK_OBJECT (the_clist),
		      "selection_received",
		      GTK_SIGNAL_FUNC (selection_received),
		      NULL);

  /* Enable dnd */

  gtk_drag_source_set (the_clist,
		       GDK_BUTTON1_MASK,
		       targets, n_targets,
		       GDK_ACTION_COPY);
  gtk_drag_dest_set (the_clist,
		     GTK_DEST_DEFAULT_ALL,
		     targets, 1,
		     GDK_ACTION_COPY);

  
  gtk_signal_connect (GTK_OBJECT (the_clist),
		      "drag_data_get",
		      GTK_SIGNAL_FUNC (drag_data_get),
		      NULL);
  gtk_signal_connect (GTK_OBJECT (the_clist),
		      "drag_data_received",
		      GTK_SIGNAL_FUNC (drag_data_received),
		      NULL);

  return the_clist;
}

int
main (int    argc,
      char **argv)
{
  GtkWidget *main_vbox;
  GtkWidget *vbox;
  GtkWidget *hpaned;
  GtkWidget *vpaned;
  GtkWidget *frame;
  GtkWidget *alignment;
  GtkWidget *table;
  GtkAdjustment *adj;
  GtkWidget *util_hbox;
  GtkWidget *util_vbox;
  GtkWidget *toolbar;
  GtkWidget *scrolled_window;

  GtkItemFactory *item_factory;
  GtkAccelGroup *accel_group;

  GList *month_list;

  static GtkTargetEntry app_target = {
    "text/uri-list", 0, 0
  };

  gint i;
  
  gtk_init (&argc, &argv);

  /* gtk_rc_parse ("/home/imain/.gtkrc"); */
  
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), "AddressBook");
  gtk_window_set_policy (GTK_WINDOW (window), TRUE, TRUE, FALSE);
  gtk_signal_connect (GTK_OBJECT (window), "delete_event", 
		      GTK_SIGNAL_FUNC (delete_callback), NULL);

  gtk_drag_dest_set (window, GTK_DEST_DEFAULT_ALL, &app_target, 1, GDK_ACTION_COPY);
  gtk_signal_connect (GTK_OBJECT (window), "drag_data_received",
		      GTK_SIGNAL_FUNC (app_drag_data_received), NULL);
		      
  
  /* Fill in the contents of the main window */

  main_vbox = gtk_vbox_new (FALSE, 0);

  gtk_container_add (GTK_CONTAINER (window), main_vbox);

  /* Create the menu */

  /*< begin_listing="create-menu" >*/
  accel_group = gtk_accel_group_new ();
  item_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR, "<main>", accel_group);
  gtk_item_factory_create_items (item_factory, nmenu_items, menu_items, NULL);

  gtk_window_add_accel_group (GTK_OBJECT (window), accel_group);

  gtk_box_pack_start (GTK_BOX (main_vbox),
		      gtk_item_factory_get_widget (item_factory, "<main>"),
		      FALSE, FALSE, 0);
  /*< end_listing="create-menu" >*/

  hpaned = gtk_hpaned_new ();
  gtk_box_pack_start (GTK_BOX (main_vbox), hpaned, TRUE, TRUE, 0);

  /* Address clist */

  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_add (GTK_CONTAINER (scrolled_window), create_clist ());
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_AUTOMATIC);
  
  gtk_paned_add1 (GTK_PANED (hpaned), scrolled_window);

  /* The details */

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_paned_add2 (GTK_PANED (hpaned), vbox);

  toolbar = gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL,
			     GTK_TOOLBAR_BOTH);
  gtk_toolbar_set_button_relief (GTK_TOOLBAR (toolbar), 
				 GTK_RELIEF_NONE);

  for (i=0; i<ntoolbar_items; i++)
    toolbar_append_item (GTK_TOOLBAR (toolbar), &toolbar_items[i]);

  gtk_box_pack_start (GTK_BOX (vbox), toolbar, FALSE, FALSE, 0);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);

  gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);

  util_vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), util_vbox);
  gtk_container_border_width (GTK_CONTAINER (util_vbox), 10);

  /*     Name */

  frame = gtk_frame_new ("Name");
  gtk_box_pack_start (GTK_BOX (util_vbox), frame, FALSE, FALSE, 0);

  util_hbox = gtk_hbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), util_hbox);
  gtk_container_border_width (GTK_CONTAINER (util_hbox), 5);
  
  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_field_entry ("First:",
					&firstname_entry,
					GTK_STRUCT_OFFSET (AddressEntry, firstname)),
		      TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_field_entry ("Last:",
					&lastname_entry,
					GTK_STRUCT_OFFSET (AddressEntry, lastname)),
		      TRUE, TRUE, 0);

  table = gtk_table_new (1, 3, TRUE);
  gtk_table_set_col_spacings (GTK_TABLE (table), 5);
  gtk_box_pack_start (GTK_BOX (util_vbox), table, FALSE, FALSE, 0);

  /*     Email  */

  gtk_table_attach (GTK_TABLE (table), 
		    make_field_entry ("Email:",
				      &email_entry,
				      GTK_STRUCT_OFFSET (AddressEntry, email)),
		    0, 2,
		    0, 1,
		    GTK_FILL | GTK_EXPAND | GTK_SHRINK,
		    0, 0,
		    0);

  /*     Phone */
  gtk_table_attach (GTK_TABLE (table), 
		    make_field_entry ("Phone:",
				      &phone_entry,
				      GTK_STRUCT_OFFSET (AddressEntry, phone)),
		    2, 3, 0, 1, GTK_FILL | GTK_EXPAND | GTK_SHRINK, 0, 0, 0);
  gtk_widget_set_usize (phone_entry, 100, -1);
  
  /*     Birthday */

  alignment = gtk_alignment_new (0.0, 0.5, 0.0, 0.0);
  gtk_box_pack_start (GTK_BOX (util_vbox), alignment, FALSE, FALSE, 0);

  frame = gtk_frame_new ("Birthday");
  gtk_container_add (GTK_CONTAINER (alignment), frame);

  util_hbox = gtk_hbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), util_hbox);
  gtk_container_border_width (GTK_CONTAINER (util_hbox), 5);

  month_combo = gtk_combo_new();
  month_list = NULL;
  for (i=0; i<12; i++)
    month_list = g_list_append (month_list, months[i]);
  gtk_combo_set_popdown_strings (GTK_COMBO (month_combo), month_list);
  gtk_combo_set_value_in_list (GTK_COMBO (month_combo), TRUE, TRUE);

  g_list_free (month_list);

  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_pair ("Month:", month_combo),
		      TRUE, TRUE, 0);


  adj = GTK_ADJUSTMENT (gtk_adjustment_new (0, 0, 31, 1, 1, 0));
  day_spinner = gtk_spin_button_new (adj, 1, 0);
  gtk_widget_set_usize (day_spinner, 40, -1);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (day_spinner), TRUE);
  gtk_box_pack_start (GTK_BOX (util_hbox), 
		      make_pair ("Day:", day_spinner),
		      TRUE, TRUE, 0);

  vpaned = gtk_vpaned_new ();
  gtk_box_pack_start (GTK_BOX (vbox), vpaned, TRUE, TRUE, 0);

  /*     Address text widget */

  gtk_paned_pack1 (GTK_PANED (vpaned), 
		   make_text_frame ("Address:", &address_text),
		   FALSE, TRUE);
  
  /*     Comments text widget */
  
  gtk_paned_pack2 (GTK_PANED (vpaned), 
		  make_text_frame ("Comments:", &comments_text),
		   TRUE, TRUE);

  gtk_widget_show_all (window);

  open_file (the_file_name);
  

  gtk_main ();

  return 0;
}
