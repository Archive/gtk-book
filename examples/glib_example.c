#include <glib.h>
#include <stdio.h>
#include <ctype.h>

/*< begin_listing="hash_entry" >*/
typedef struct {
  gint     nfound;		/* Count of the number we've found */
  GString *linenos;             /* A string representation of the entries */
} Entry;
/*< end_listing="hash_entry" >*/

GMemChunk *entry_chunk;		/* Memory chunk we allocate the entries from */
GHashTable *hash;		/* Hash table of index entries */

/*< begin_listing="add_word" >*/
/* Add this word/linenumber to the index. Create a new entry if
 * necessary. Uses or frees word.
 */
void
add_word (GString *word, gint lineno)
{
  Entry *entry = g_hash_table_lookup (hash, word->str);
  
  if (!entry)
    {
      /* Create a new entry, and insert it into the hash table  */
      entry = g_mem_chunk_alloc (entry_chunk);
      entry->linenos = g_string_new (NULL);
      entry->nfound = 0;

      /* Reuse the string from the GString */
      g_hash_table_insert (hash, word->str, entry);
      g_string_free (word, FALSE);
    }
  else
    g_string_free (word, TRUE);

  entry->nfound++;
  /* append the line number to the index entry */
  if (entry->linenos->len > 0)
    g_string_append (entry->linenos, ", ");
  g_string_sprintfa (entry->linenos, "%d", lineno);
}
/*< end_listing="add_word" >*/

/*< begin_listing="print_and_free_entry" >*/
/* Print out the contents of an entry, and then free the memory it uses
 */
void
print_and_free_entry (gpointer key, gpointer value, gpointer data)
{
  Entry *entry = value;

  g_print ("%s (%d): %s\n", (gchar *)key, entry->nfound, entry->linenos->str);

  g_string_free (entry->linenos, TRUE);
  g_free (key);
}
/*< end_listing="print_and_free_entry" >*/

/*< begin_listing="process_line" >*/
/* Find all words buffer and add them to the index
 */
void
process_line (gchar *buffer, gint lineno)
{
  GString *word = NULL;
  char *p = buffer;

  while (*p)
    {
      if (isalpha((guchar)*p) || (*p == '_'))
	{
	  if (!word)
	    word = g_string_new(NULL);
	  
	  g_string_append_c (word, *p);
	}
      else 
	{
	  if (word)
	    {
	      add_word (word, lineno);
	      word = NULL;
	    }
	}
      p++;
    }
}
/*< end_listing="process_line" >*/

#define BUFSIZE 1024

int main ()
{
  gchar buffer[BUFSIZE];
  gint lineno = 0;

/*< begin_listing="create_hash" >*/
  hash = g_hash_table_new (g_str_hash, g_str_equal);
  entry_chunk = g_mem_chunk_new ("Entries", sizeof(Entry), 1024, G_ALLOC_ONLY);
/*< end_listing="create_hash" >*/

  /* Read and process the lines from stdin 
   */
  while (fgets (buffer, BUFSIZE-1, stdin) != NULL)
    {
      process_line (buffer, lineno);
      lineno++;
    }

/*< begin_listing="foreach" >*/
  /* Print out all the entries in our hash table */
  g_hash_table_foreach (hash, print_and_free_entry, NULL);
/*< end_listing="foreach" >*/

/*< begin_listing="destroy_hash" >*/
  g_hash_table_destroy (hash);
  g_mem_chunk_destroy (entry_chunk);
/*< end_listing="destroy_hash" >*/
  
  return 0;
}


