#include <gtk/gtk.h>
#include <string.h>

/* Global variables
 */

GtkWidget *list;
GList *lines;

guint idle_id;

/*< begin_listing="timeout_cb" >*/
/* Timeout function called 10 seconds after an EOF
 */
gboolean
quit_cb (gpointer data)
{
  gtk_main_quit();
  return FALSE;			/* Do not execute again */
}
/*< end_listing="timeout_cb" >*/

/* Compare two strings. Used to sort list of lines
 */
gint
line_compare (gconstpointer a, gconstpointer b)
{
  return strcmp(a,b);
}

/* Idle function that sorts the list of lines and
 * fills in the list from it
 */
gboolean
lines_idle (gpointer data)
{
  GList *tmp_list;

  /* Clear out the old list
   */
  gtk_list_clear_items (GTK_LIST (list), 0, -1);

  /* Sort the list of lines alphabetically
   */
  lines = g_list_sort (lines, line_compare);

  /* Iterate through the list of lines, create a GtkListItem for
   * each lines, and add them to the GtkList
   */
  tmp_list = lines;
  while (tmp_list)
    {
      GtkWidget *list_item = gtk_list_item_new_with_label (tmp_list->data);
      gtk_widget_show (list_item);
      
      gtk_container_add (GTK_CONTAINER (list), list_item);
      
      tmp_list = tmp_list->next;
    }

  idle_id = 0;
  
  return FALSE;			/* Do not execute again */
}

/*< begin_listing="io_handler" >*/
/* IO handler invoked when there is data that can be read
 * from stdin
 */
gboolean
io_handler (GIOChannel *source, GIOCondition condition, gpointer data)
{
#define BUF_SIZE 256

  gchar buffer[256];
  guint bytes_read;
  gchar *p;
  gboolean need_idle = FALSE;

  /* String used to store partial lines
   */
  static GString *result = NULL;

  if (!result)
    result = g_string_new (NULL);

  /* Read in one buffers worth of information
   */
  g_io_channel_read (source, buffer, BUF_SIZE - 1, &bytes_read);
  if (bytes_read == 0)
    {
      /* 0 bytes were read; this indicates EOF. So close
       * the source, add a timeout to quit in 10 seconds
       * and return FALSE to remove the io handler
       */
      g_io_channel_close (source);

/*< begin_listing="timeout_add" >*/
      g_timeout_add (10000,
		     (GSourceFunc) gtk_main_quit,
		     NULL);
/*< end_listing="timeout_add" >*/
      
      return FALSE;		/* Remove handler */
    }

  /* We have data. Append it to our string.
   */
  buffer[bytes_read] = '\0';
  g_string_append (result, buffer);

  /* Find partial lines in our buffer, add them to the
   * list of lines, and then remove them from the string
   */
  while (1)
    {
      p = strchr(result->str, '\n');
      if (!p)
	break;

      *p = '\0';
      
      lines = g_list_prepend (lines, g_strdup (result->str));
      g_string_erase (result, 0, p - result->str + 1);

      need_idle = TRUE;
    }

  /* If we've added some more lines, set up an idle
   * to add them to the GtkList at the next opportunity
   */
  if (!idle_id && need_idle)
    idle_id = g_idle_add (lines_idle, NULL);

  return TRUE;			/* Retain handler */
}
/*< end_listing="io_handler" >*/

int main (int argc, char **argv)
{
  GtkWidget *window;
  GtkWidget *scrollwin;
  GIOChannel *channel;
  
  gtk_init (&argc, &argv);

  /* Create a window containing a list
   */
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_usize (window, 200, 400);

  scrollwin = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_add (GTK_CONTAINER (window), scrollwin);
  
  list = gtk_list_new ();
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrollwin), 
					 list);

  gtk_widget_show_all (window);

  /* Create a GIOChannel for stdin, and add a watch callback
   * that will be called whenever there is data ready to
   * read
   */
/*< begin_listing="watch_add" >*/
  channel = g_io_channel_unix_new (0); /* STDIN */
  g_io_add_watch (channel, G_IO_IN, io_handler, NULL);
  g_io_channel_unref (channel);
/*< end_listing="watch_add" >*/

  gtk_main();
  
  return 0;
}
