/*< begin_listing="myphrase-test" >*/
#include "myphrase.h"
#include <gtk/gtk.h>

/*< begin_listing="toggle_phrase" >*/
static void
toggle_phrase (GtkToggleButton *toggle,
	       MyPhrase        *phrase)
{
  my_phrase_set_emphasize (phrase, toggle->active);
}
/*< end_listing="toggle_phrase" >*/

int
main (int   argc,
      char *argv[])
{
  GtkWidget *phrase, *toggle, *label, *window, *vbox;

  gtk_init (&argc, &argv);

  /* create a phrase widget */
  phrase = my_phrase_new ("Accentuated Phrase");
  gtk_widget_show (phrase);

  /* create a toggle button and setup a handler, so it toggles
   * the accentuated state of the phrase widget
   */
  toggle = gtk_toggle_button_new ();
  gtk_signal_connect (GTK_OBJECT (toggle),
		      "toggled",
		      GTK_SIGNAL_FUNC (toggle_phrase),
		      phrase);
  label = gtk_label_new ("Accentuate");
  gtk_container_add (GTK_CONTAINER (toggle), label);

  /* set the initial toggle button state, so by default the phrase
   * is accentuated. this already causes a signal emission on the
   * toggle button that calls the toggle_phrase() handler.
   */
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle), TRUE);

  /* finally, show the widgets in a GtkWindow */
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), "MyPhrase Test");
  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 10);
  gtk_container_add (GTK_CONTAINER (vbox), phrase);
  gtk_container_add (GTK_CONTAINER (vbox), toggle);
  gtk_container_add (GTK_CONTAINER (window), vbox);
  gtk_signal_connect (GTK_OBJECT (window),
		      "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit),
		      NULL);
  gtk_widget_show_all (window);

  gtk_main ();

  return 0;
}
/*< end_listing="myphrase-test" >*/
