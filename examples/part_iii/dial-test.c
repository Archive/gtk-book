#include "gtkdial.h"
#include <gtk/gtk.h>

int
main (int   argc,
      char *argv[])
{
  GtkWidget *dial;

  gtk_init (&argc, &argv);

  dial = gtk_dial_new (NULL);
  gtk_widget_show (dial);

  gtk_widget_new (GTK_TYPE_WINDOW,
		  "title", "GtkDial Test",
		  "child", gtk_widget_new (GTK_TYPE_VBOX,
					   "visible", TRUE,
					   "border_width", 5,
					   "spacing", 5,
					   "child", dial,
					   NULL),
		  "visible", TRUE,
		  "signal::destroy", gtk_main_quit, NULL,
		  NULL);
  gtk_main ();

  return 0;
}
