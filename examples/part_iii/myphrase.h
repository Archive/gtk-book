
/*< begin_listing="macros" >*/
#include <gtk/gtklabel.h>

#define MY_TYPE_PHRASE            (my_phrase_get_type ())
#define MY_PHRASE(object)         (GTK_CHECK_CAST ((object), MY_TYPE_PHRASE, MyPhrase))
#define MY_PHRASE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), MY_TYPE_PHRASE, MyPhraseClass))
#define MY_IS_PHRASE(object)      (GTK_CHECK_TYPE ((object), MY_TYPE_PHRASE))
#define MY_IS_PHRASE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), MY_TYPE_PHRASE))
#define MY_PHRASE_GET_CLASS(obj)  ((MyPhraseClass*) (((GtkObject*) (obj))->klass))
/*< end_listing="macros" >*/

/*< begin_listing="structures" >*/
typedef struct _MyPhrase      MyPhrase;
typedef struct _MyPhraseClass MyPhraseClass;

struct _MyPhrase
{
  GtkLabel parent_object;

  guint emphasize : 1;
};
struct _MyPhraseClass
{
  GtkLabelClass parent_class;
};
/*< end_listing="structures" >*/

/*< begin_listing="prototypes" >*/
GtkType    my_phrase_get_type      (void);
GtkWidget* my_phrase_new           (const gchar *text);
void       my_phrase_set_emphasize (MyPhrase    *phrase,
				    gboolean     emphasize);
/*< end_listing="prototypes" >*/
