


/*< begin_listing="declarations" >*/
#include "myphrase.h"


static void     my_phrase_class_init   (MyPhraseClass  *class);
static void     my_phrase_init         (MyPhrase       *phrase);
static gboolean my_phrase_expose_event (GtkWidget      *widget,
					GdkEventExpose *event);
static void     my_phrase_draw         (GtkWidget      *widget,
					GdkRectangle   *area);

static GtkLabelClass *parent_class = NULL;
/*< end_listing="declarations" >*/


/*< begin_listing="get_type" >*/
GtkType
my_phrase_get_type (void)
{
  static GtkType phrase_type = 0;
  
  if (!phrase_type)
    {
      static const GtkTypeInfo phrase_info =
      {
	"MyPhrase",
	sizeof (MyPhrase),
	sizeof (MyPhraseClass),
	(GtkClassInitFunc) my_phrase_class_init,
	(GtkObjectInitFunc) my_phrase_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      
      phrase_type = gtk_type_unique (GTK_TYPE_LABEL, &phrase_info);
    }
  
  return phrase_type;
}
/*< end_listing="get_type" >*/

/*< begin_listing="struct&class-init" >*/
static void
my_phrase_class_init (MyPhraseClass *class)
{
  GtkWidgetClass *widget_class;
  
  parent_class = gtk_type_class (GTK_TYPE_LABEL);
  widget_class = GTK_WIDGET_CLASS (class);
  
  widget_class->expose_event = my_phrase_expose_event;
  widget_class->draw = my_phrase_draw;
}

static void
my_phrase_init (MyPhrase *phrase)
{
  phrase->emphasize = TRUE;
}
/*< end_listing="struct&class-init" >*/

/*< begin_listing="new" >*/
GtkWidget*
my_phrase_new (const gchar *text)
{
  GtkWidget *phrase;

  phrase = gtk_widget_new (MY_TYPE_PHRASE, NULL);
  if (text)
    gtk_label_set_text (GTK_LABEL (phrase), text);

  return phrase;
}
/*< end_listing="new" >*/

/*< begin_listing="set_emphasize" >*/
void
my_phrase_set_emphasize (MyPhrase *phrase,
		         gboolean  emphasize)
{
  g_return_if_fail (MY_IS_PHRASE (phrase));

  emphasize = emphasize != FALSE;
  if (emphasize != phrase->emphasize)
    {
      phrase->emphasize = emphasize;
      gtk_widget_queue_clear (GTK_WIDGET (phrase));
    }
}
/*< end_listing="set_emphasize" >*/

/*< begin_listing="paint" >*/
static void
my_phrase_paint (MyPhrase *phrase)
{
  GtkWidget *widget = GTK_WIDGET (phrase);
    
  gtk_paint_flat_box (widget->style,
		      widget->window,
		      GTK_STATE_PRELIGHT,
		      GTK_SHADOW_NONE,
		      NULL,
		      widget,
		      NULL,
		      widget->allocation.x,
		      widget->allocation.y,
		      widget->allocation.width,
		      widget->allocation.height);
}
/*< end_listing="paint" >*/

/*< begin_listing="draw&expose" >*/
static gboolean
my_phrase_expose_event (GtkWidget      *widget,
			GdkEventExpose *event)
{
  MyPhrase *phrase;

  phrase = MY_PHRASE (widget);
  if (GTK_WIDGET_DRAWABLE (phrase) && phrase->emphasize)
    my_phrase_paint (phrase);

  /* chain parent class' handler */
  if (GTK_WIDGET_CLASS (parent_class)->expose_event)
    return GTK_WIDGET_CLASS (parent_class)->expose_event (widget, event);
  else
    return TRUE;
}

static void
my_phrase_draw (GtkWidget    *widget,
		GdkRectangle *area)
{
  MyPhrase *phrase;

  phrase = MY_PHRASE (widget);
  if (GTK_WIDGET_DRAWABLE (phrase) && phrase->emphasize)
    my_phrase_paint (phrase);
    
  /* chain parent class' handler */
  if (GTK_WIDGET_CLASS (parent_class)->draw)
    GTK_WIDGET_CLASS (parent_class)->draw (widget, area);
}
/*< end_listing="draw&expose" >*/
