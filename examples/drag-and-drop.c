#include <gtk/gtk.h>

/* The GtkHBox containing the titles */
static GtkWidget *titles_box;

/* The GtkFrame surrounding the titles */
static GtkWidget *titles_frame;

/* An empty GtkHBox inserted into titles_box to indicate the drop location */
static GtkWidget *titles_gap_box = NULL;

/* The tag identifying the timeout for removing the gap in the titles */
static guint titles_remove_tag = 0;

/* The index of the gap in titles_box */
static guint titles_gap_position = -1;

/* TRUE if a drag is currently over titles_box */
static gboolean titles_have_drag = FALSE;

/* The list of iniital titles */
static const char *titles[] =
{
  "Name",
  "Email",
  "Phone",
  "Birthday",
  "Address",
  "Comments"
};
static const int n_titles = sizeof(titles) / sizeof(titles[0]);

/* Array of targets that we drag and drop */
/*< begin_listing="drag-targets" >*/
static GtkTargetEntry targets[] = 
{
  { "_ADDRESS_BOOK_TITLE", GTK_TARGET_SAME_APP, 0 },
};
/*< end_listing="drag-targets" >*/

/* Given an x position, find the index where a dropped title
 * should be inserted.
 */
static gint 
titles_find_position (gint x)
{
  GList *tmp_list;
  GList *children;		/* List of children of titles_box */
  gint width = 0;		/* Accumulated width from the titles we've examined
				 * so far */
  gint result = 0;		/* Result index */

  children = gtk_container_children (GTK_CONTAINER (titles_box));
  tmp_list = children;

  while (tmp_list)
    {
      GtkWidget *child = tmp_list->data;
      
      if (GTK_WIDGET_VISIBLE (child))
	{
	  if (width <= x &&
	      width + child->allocation.width > x)
	    {
	      if (x - width >= child->allocation.width / 2)
		result++;
	      
	      break;
	    }
	  
	  width += child->allocation.width;
	}
      tmp_list = tmp_list->next;
      result++;
    }

  g_list_free (children);

  /* Treat the position after the gap the same as the position before
   * the gap
   */
  if (result == titles_gap_position + 1)
    result--;
  
  return result;
}

/* Callback to remove the gap we use to show where insertion
 * will occur.
 */
static void
titles_remove_gap (void)
{
  if (titles_gap_box)
    {
      gtk_widget_destroy (titles_gap_box);
      titles_gap_box = NULL;
      titles_gap_position = -1;
    }
}

/* Timeout to remove the gap
 */
static gboolean
titles_remove_gap_timeout (gpointer data)
{
  titles_remove_gap ();
  
  return FALSE;
}

typedef struct
{
  gint x, y;
} Point;

/* We remember where the user presses the mouse on our button
 * So we can later set the hot spot correctly.
 */
/*< begin_listing="button-press" >*/
static void
title_button_press (GtkWidget *widget, GdkEventButton *event)
{
  if (event->button == 1)
    {
      Point *point = g_new (Point, 1);
      point->x = event->x;
      point->y = event->y;
      
      gtk_object_set_data_full (GTK_OBJECT (widget), "press_point",
				point, (GtkDestroyNotify)g_free);
    }
}
/*< end_listing="button-press" >*/

/* Callback when a drag is started from a title button
 */
/*< begin_listing="drag-begin" >*/
static void 
title_drag_begin (GtkWidget	 *widget,
		  GdkDragContext *context)
{
  GtkWidget *icon_widget;	/* Toplevel window used as drag icon */
  GtkWidget *button;		/* Button displayed within icon_widget */
  char *text;			/* Text of the title being dragged */
  Point *point;			/* Structure holding the position where
				 * user clicked the button to start the drag
				 */

  /* Hide the widget for the duration of the drag */
  gtk_widget_hide (widget);

  /* Create an icon widget that matches the title button in size
   */
  gtk_label_get (GTK_LABEL (GTK_BIN (widget)->child), &text);

  icon_widget = gtk_window_new (GTK_WINDOW_POPUP);
  button = gtk_button_new_with_label (text);
  gtk_widget_show (button);
  gtk_container_add (GTK_CONTAINER (icon_widget), button);

  /* Match the size of the icon widget to the
   * current size of the title */
  gtk_widget_set_usize (icon_widget,
			widget->allocation.width,
			widget->allocation.height);

  /* FIXME: GTK+ bug */
  gtk_widget_realize (icon_widget);

  /* Retrieve the point where the user started the drag */
  point = gtk_object_get_data (GTK_OBJECT (widget), "press_point");

  gtk_drag_set_icon_widget (context, icon_widget, point->x, point->y);
}
/*< end_listing="drag-begin" >*/

/* Callback when a drag from a title button ends
 */
/*< begin_listing="drag-end" >*/
static void
title_drag_end (GtkWidget *widget,
		GdkDragContext *context)
{
  /* Show the widget again now that the drag is over */
  gtk_widget_show (widget);
}
/*< end_listing="drag-end" >*/

/* Callback to retrieve the data for a drag from a title button
 */
/*< begin_listing="drag-data-get" >*/
static void 
title_drag_data_get (GtkWidget         *widget,
		     GdkDragContext    *context,
		     GtkSelectionData  *selection_data,
		     guint              info,
		     guint              time)
{
  gchar *text;

  gtk_label_get (GTK_LABEL (GTK_BIN (widget)->child), &text);

  gtk_selection_data_set (selection_data,
			  GDK_TARGET_STRING, 8, text, strlen (text));
}
/*< end_listing="drag-data-get" >*/

/* Callback when the drag (with action GDK_ACTION_MOVE) ends
 */
/*< begin_listing="drag-data-delete" >*/
static void 
title_drag_data_delete (GtkWidget        *widget,
			GdkDragContext    *context)
{
  gtk_widget_destroy (widget);
}
/*< end_listing="drag-data-delete" >*/

/* Add a new titles to our titles list
 */
static void
titles_insert_string (gint position, const gchar *string)
{
  GtkWidget *button = gtk_button_new_with_label (string);
  gtk_widget_show (button);
  gtk_box_pack_start (GTK_BOX (titles_box), button, FALSE, FALSE, 0);
  gtk_box_reorder_child (GTK_BOX (titles_box), button, position);

  /* Initialize our button as a drag source
   */
  /*< begin_listing="source-init" >*/
  gtk_drag_source_set (button,
		       GDK_BUTTON1_MASK,
		       targets, 1,
		       GDK_ACTION_MOVE | GDK_ACTION_COPY);
  
  gtk_signal_connect (GTK_OBJECT (button), "button_press_event",
		      GTK_SIGNAL_FUNC (title_button_press), NULL);
  gtk_signal_connect (GTK_OBJECT (button), "drag_begin",
		      GTK_SIGNAL_FUNC (title_drag_begin), NULL);
  gtk_signal_connect (GTK_OBJECT (button), "drag_end",
		      GTK_SIGNAL_FUNC (title_drag_end), NULL);
  gtk_signal_connect (GTK_OBJECT (button), "drag_data_get",
		      GTK_SIGNAL_FUNC (title_drag_data_get), NULL);
  gtk_signal_connect (GTK_OBJECT (button), "drag_data_delete",
		      GTK_SIGNAL_FUNC (title_drag_data_delete), NULL);
  /*< end_listing="source-init" >*/
}

static void
titles_insert_gap (gint position)
{
  if (position != titles_gap_position)
    {
      titles_remove_gap();
      if (titles_gap_box && position > titles_gap_position)
	position--;
      
      titles_gap_box = gtk_hbox_new (FALSE, 0);
      titles_gap_position = position;
  
      gtk_widget_show (titles_gap_box);
      gtk_widget_set_usize (titles_gap_box, 10, -1);
      gtk_box_pack_start (GTK_BOX (titles_box), titles_gap_box, FALSE, FALSE, 0);
      gtk_box_reorder_child (GTK_BOX (titles_box), titles_gap_box, position);
    }
}

/* Callback when the user is dragged over our widget
 */
/*< begin_listing="drag-motion" >*/
static void
titles_drag_motion (GtkWidget	       *widget,
		    GdkDragContext     *context,
		    gint                x,
		    gint                y,
		    guint               time)
{
  gint position;

  /* If we are currently waiting to remove the gap,
   * remove the timeout */
  if (titles_remove_tag)
    {
      gtk_timeout_remove (titles_remove_tag);
      titles_remove_tag = 0;
    }

  /* Highlight the frame */
  if (!titles_have_drag)
    {
      titles_have_drag = TRUE;
      gtk_drag_highlight (titles_frame);
    }

  /* Insert/move the gap to the new position */
  position = titles_find_position (x);
  titles_insert_gap (position);
}
/*< end_listing="drag-motion" >*/

/*< begin_listing="drag-leave" >*/
static void 
titles_drag_leave (GtkWidget	      *widget,
		   GdkDragContext     *context,
		   guint               time)
{
  titles_remove_tag = gtk_timeout_add (1000, titles_remove_gap_timeout, NULL);
  gtk_drag_unhighlight (titles_frame);
  titles_have_drag = FALSE;
}
/*< end_listing="drag-leave" >*/

/* When a drop is received in title list, this handler is invoked.
 */
/*< begin_listing="drag-data-received" >*/
static void 
titles_drag_data_received (GtkWidget          *widget,
			   GdkDragContext     *context,
			   gint                x,
			   gint                y,
			   GtkSelectionData   *selection_data,
			   guint               info,
			   guint               time)
{
  titles_insert_string (titles_find_position (x),
			(gchar *)selection_data->data);
  titles_remove_gap ();
}
/*< end_listing="drag-data-received" >*/

int 
main (int argc, char **argv)
{
  GtkWidget *window;		/* Toplevel window for the application */
  int i;
  
  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

  /* Create a box of buttons as our drag source
   */
  titles_frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (titles_frame), GTK_SHADOW_IN);

  gtk_container_add (GTK_CONTAINER (window), titles_frame);

  titles_box = gtk_hbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (titles_frame), titles_box);

  /* Initialize the titles box as a drop destination
   */
  /*< begin_listing="target-init" >*/
  gtk_drag_dest_set (titles_box,
		     GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP,
		     targets, 1,
		     GDK_ACTION_MOVE);

  gtk_signal_connect (GTK_OBJECT (titles_box), "drag_motion",
		      GTK_SIGNAL_FUNC (titles_drag_motion), NULL);
  gtk_signal_connect (GTK_OBJECT (titles_box), "drag_leave",
		      GTK_SIGNAL_FUNC (titles_drag_leave), NULL);
  gtk_signal_connect (GTK_OBJECT (titles_box), "drag_data_received",
		      GTK_SIGNAL_FUNC (titles_drag_data_received), NULL);
  /*< end_listing="target-init" >*/

  for (i = 0; i < n_titles; i++)
    titles_insert_string (i, titles[i]);

  gtk_widget_show_all (window);
  
  gtk_main ();

  return 0;
}
