#include <gtk/gtk.h>

int main (int argc, char **argv)
{
  GtkWidget *window, *frame, *button;

  gtk_init (&argc, &argv);
 
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  
  frame = gtk_frame_new ("A Nice Button");

  gtk_container_add (GTK_CONTAINER (window), frame);
  
  button = gtk_button_new_with_label ("Pushme!");
  
  gtk_container_add (GTK_CONTAINER (frame), button);

  gtk_widget_show_all (window);

  gtk_main();
  
  return (0);
}
