#include <gtk/gtk.h>

int
main (int argc, char *argv[]) 
{
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *vbox;
  
  /* Initialize GTK */
  gtk_init (&argc, &argv);
  
  /* Create toplevel window */
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  
  /* Create a vertical box (vbox) to place our buttons in.
   * Here we create it with homogeneous set to FALSE, and 3
   * pixels of spacing between each packed widget */
  vbox = gtk_vbox_new (FALSE, 3);
  
  /* Place the vbox inside the window.  GtkWindow inherits from 
   * GtkContainer, so we use the gtk_container_add() function to
   * accomplish this. */
  gtk_container_add (GTK_CONTAINER (window), vbox);

  /* GtkContainer's allow you to set a border size */
  gtk_container_set_border_width (GTK_CONTAINER (window), 3);
  
  
  /* Create a button */
  button = gtk_button_new_with_label ("button 1");
  
  /* Pack it into the box we created.
   * Here we set expand and fill to FALSE, causing the widget not
   * to grow at all.  Padding is set to 0. */
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

  /* Create another button */
  button = gtk_button_new_with_label ("button 2");

  /* In this one, we are setting expand to FALSE, and fill to TRUE.
   * This combination will behave the same as above, for reasons
   * discussed in the text.  We are also setting the padding to
   * 5 pixels. */
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, TRUE, 5);

  /* Yet another button */
  button = gtk_button_new_with_label ("button 3");

  /* This time we are going to set the 'expand' argument to TRUE
   * causing this widget to consume any extra space presented to it.
   * We are going to leave 'fill' as false though so the widget will
   * not grow itself to take up this space. */
  gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, FALSE, 0);

  /* And yet another button */
  button = gtk_button_new_with_label ("button 4");

  /* This time we are going to set the 'expand' and 'fill' arguments 
   * to TRUE.  This will cause it to take up any space left over for it, 
   * and to 'grow' into that space */
  gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);

  /* Show the entire widget tree */
  gtk_widget_show_all (window);
  
  /* And of course, our main function. */
  gtk_main ();

  return(0);
}
