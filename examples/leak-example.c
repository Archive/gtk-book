#include <gtk/gtk.h>

int main (int argc, char **argv)
{
  GtkWidget *window1, *window2, *button;
  
  gtk_init (&argc, &argv);

  window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window1), "Close Me");

  gtk_signal_connect (GTK_OBJECT (window1), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
  
  window2 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  button = gtk_button_new_with_label ("Unused");

  gtk_widget_show_all (window1);
  gtk_widget_show_all (window2);

  gtk_main ();

  return 0;
}
