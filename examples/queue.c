#include <glib.h>
#include <stdio.h>

#define BUFSIZE 256

/* Read a line from infile, return copy in allocated memory, or
 * NULL if we hit the end of the file.
 */
gchar *
get_line(FILE *infile) 
{
  gchar buf[BUFSIZE];
  GString *result = g_string_new (NULL);
  gboolean got_line = FALSE;
  gchar *res_str = NULL;

  while (fgets(buf, BUFSIZE, infile) != NULL)
    {
      got_line = TRUE;
      g_string_append (result, buf);
      /* If the string ends in '\n', we now have an entire line,
       * otherwise, we need to get another chunk
       */
      if (result->str[result->len - 1] == '\n')
	{
	  g_string_truncate (result, result->len - 1);
	  break;
	}
    }

  /* fgets returned NULL, which means we hit the end of the file */

  if (got_line)
    {
      res_str = result->str;
      g_string_free (result, FALSE);
    }
  else
    g_string_free (result, TRUE);
  
  return res_str;
}

int main ()
{
  GSList *first = NULL;
  GSList *last = NULL;
  gchar *line;

  while ((line = get_line (stdin)))
    {
      if (line[0])
	{
	  /* Insert an element at the end */

	  if (last)
	    {
	      g_slist_append (last, line);
	      last = last->next;
	    }
	  else
	    {
	      last = g_slist_append (NULL, line);
	      first = last;
	    }
	}
      else
	{
	  g_free (line);
	  
	  /* Remove an element from the beginning */

	  if (first)
	    {
	      GSList *tmp_list = first;
	      if (last == first)
		last = NULL;
	      
	      first = g_slist_remove_link (first, first);
	      
	      g_print ("%s\n", (gchar *)tmp_list->data);
	      g_free (tmp_list->data);
	      g_slist_free_1 (tmp_list);
	    }
	}
    }
  
  return 0;
}
