#include <gtk/gtk.h>

/* Backing pixmap for drawing area */
static GdkPixmap *pixmap = NULL;
static GdkGC *gc = NULL;

static gint last_x;
static gint last_y;

/* Create a new backing pixmap of the appropriate size */
static gint
configure_event (GtkWidget *widget, GdkEventConfigure *event)
{
/*< begin_listing="create-pixmap" >*/
  if (pixmap)
    gdk_pixmap_unref(pixmap);

  pixmap = gdk_pixmap_new(widget->window,
			  widget->allocation.width,
			  widget->allocation.height,
			  -1);
  gdk_draw_rectangle (pixmap,
		      widget->style->white_gc,
		      TRUE,
		      0, 0,
		      widget->allocation.width,
		      widget->allocation.height);
/*< end_listing="create-pixmap" >*/

/*< begin_listing="create-gc" >*/
  if (!gc)
    {
      GdkGCValues values;

      values.foreground = widget->style->black;
      values.line_width = 10;
      values.join_style = GDK_JOIN_ROUND;
      values.cap_style = GDK_CAP_ROUND;

      gc = gdk_gc_new_with_values (pixmap, &values,
				   GDK_GC_FOREGROUND |
				   GDK_GC_LINE_WIDTH |
				   GDK_GC_JOIN_STYLE |
				   GDK_GC_CAP_STYLE);
    }
/*< end_listing="create-gc" >*/

  return TRUE;
}

/*< begin_listing="expose_event" >*/
/* Redraw the screen from the backing pixmap */
static gint
expose_event (GtkWidget *widget, GdkEventExpose *event)
{
  gdk_draw_drawable (widget->window,
		     widget->style->white_gc,
		     pixmap,
		     event->area.x, event->area.y,
		     event->area.x, event->area.y,
		     event->area.width, event->area.height);

  return FALSE;
}
/*< end_listing="expose_event" >*/

/*< begin_listing="draw_stroke" >*/
/* Draw a rectangle on the screen */
static void
draw_stroke (GtkWidget *widget, gdouble x, gdouble y)
{
  GdkRectangle last_rect, cur_rect, update_rect;
 
  gdk_draw_line (pixmap, gc, last_x, last_y, x, y);

  cur_rect.x = x - 5;
  cur_rect.y = y - 5;
  cur_rect.width = 10;
  cur_rect.height = 10;
  
  last_rect.x = last_x - 5;
  last_rect.y = last_y - 5;
  last_rect.width = 10;
  last_rect.height = 10;

  gdk_rectangle_union (&cur_rect, &last_rect, &update_rect);
  gdk_window_invalidate_rect (widget->window, &update_rect, FALSE);
}
/*< end_listing="draw_stroke" >*/

/*< begin_listing="event-handlers" >*/
static gint
button_press_event (GtkWidget *widget, GdkEventButton *event)
{
  last_x = event->x;
  last_y = event->y;

  draw_stroke (widget, event->x, event->y);

  return TRUE;
}

static gint
button_release_event (GtkWidget *widget, GdkEventButton *event)
{
  if (event->button == 1 && pixmap != NULL)
    draw_stroke (widget, event->x, event->y);

  return TRUE;
}

static gint
motion_notify_event (GtkWidget *widget, GdkEventMotion *event)
{
  if (event->is_hint)
    gdk_window_get_pointer (event->window, NULL, NULL, NULL);
    
  if (event->state & GDK_BUTTON1_MASK && pixmap != NULL)
    draw_stroke (widget, event->x, event->y);

  last_x = event->x;
  last_y = event->y;
  
  return TRUE;
}
/*< end_listing="event-handlers" >*/

int
main (int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *drawing_area;
  GtkWidget *vbox;

  GtkWidget *button;

  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_name (window, "Test Input");

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

  /* Create the drawing area */

  drawing_area = gtk_drawing_area_new ();
  gtk_drawing_area_size (GTK_DRAWING_AREA (drawing_area), 200, 200);
  gtk_box_pack_start (GTK_BOX (vbox), drawing_area, TRUE, TRUE, 0);

  /* Signals used to handle backing pixmap */

  gtk_signal_connect (GTK_OBJECT (drawing_area), "expose_event",
		      (GtkSignalFunc) expose_event, NULL);
  gtk_signal_connect (GTK_OBJECT(drawing_area), "configure_event",
		      (GtkSignalFunc) configure_event, NULL);

  /* Event signals */

  gtk_signal_connect (GTK_OBJECT (drawing_area), "motion_notify_event",
		      (GtkSignalFunc) motion_notify_event, NULL);
  gtk_signal_connect (GTK_OBJECT (drawing_area), "button_press_event",
		      (GtkSignalFunc) button_press_event, NULL);
  gtk_signal_connect (GTK_OBJECT (drawing_area), "button_release_event",
		      (GtkSignalFunc) button_release_event, NULL);

  gtk_widget_set_events (drawing_area, GDK_EXPOSURE_MASK
			 | GDK_LEAVE_NOTIFY_MASK
			 | GDK_BUTTON_PRESS_MASK
			 | GDK_BUTTON_RELEASE_MASK
			 | GDK_POINTER_MOTION_MASK
			 | GDK_POINTER_MOTION_HINT_MASK);

  /* .. And a quit button */
  button = gtk_button_new_with_label ("Quit");
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

  gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy),
			     GTK_OBJECT (window));

  gtk_widget_show_all (window);

  gtk_main ();

  return 0;
}
