#include <gtk/gtk.h>

GtkWidget *
make_widget (const gchar *str)
{
  GtkWidget *frame, *label;

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT);
  
  label = gtk_label_new (str);
  gtk_container_add (GTK_CONTAINER (frame), label);
  
  return frame;
}

int
main (int argc, char **argv)
{
  GtkWidget *window, *table;

  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

  table = gtk_table_new (3, 3, FALSE);
  gtk_container_add (GTK_CONTAINER (window), table);

  gtk_table_attach (GTK_TABLE (table), make_widget ("A"),
		    /* x */                 /* y */
/* attach */	    0, 1,                   0, 1,    
/* options */	    GTK_FILL,               0,       
/* padding */	    0,                      0);      

  gtk_table_attach (GTK_TABLE (table), make_widget (" | \n-B-\n | "),
		    /* x */                 /* y */
/* attach */	    1, 3,                   0, 1,    
/* options */	    GTK_EXPAND | GTK_FILL,  0,       
/* padding */	    0,                      0);      

  gtk_table_attach (GTK_TABLE (table), make_widget (" | \n-C-\n | "),
		    /* x */                 /* y */
/* attach */	    0, 1,                   1, 3,    
/* options */	    0,                      GTK_EXPAND,
/* padding */	    0,                      0);      

  gtk_table_attach (GTK_TABLE (table), make_widget ("D"),
		    /* x */                 /* y */
/* attach */	    1, 2,                   1, 2,    
/* options */       GTK_EXPAND | GTK_FILL,  GTK_EXPAND | GTK_FILL,
/* padding */	    0,                      0);      

  gtk_table_attach (GTK_TABLE (table), make_widget ("E"),
		    /* x */                 /* y */
/* attach */	    2, 3,                   2, 3,    
/* options */	    0,                      0,       
/* padding */	    0,                      0);      

  gtk_widget_show_all (window);

  gtk_main();

  return 0;
}
