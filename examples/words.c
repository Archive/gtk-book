#include <gtk/gtk.h>

/*< begin_listing="print_words_ascii" >*/
void
print_words_ascii (const char *input_text)
{
  const char *p = input_text;
  const char *end;
  char *word;

  while (TRUE)
    {
      /* Find the start of a word */
      while (*p && *p == ' ')
	p++;
      if (!*p)
	break;

      /* Find the end of the word */
      end = p;
      while (*end && *end != ' ')
	end++;

      word = g_strndup (p, end - p);
      g_print ("%s\n", word);
      g_free (word);

      p = end;
    }
}
/*< end_listing="print_words_ascii" >*/

/*< begin_listing="print_words_utf8" >*/
void
print_words_utf8 (const char *input_text)
{
  const char *p = input_text;
  const char *end;
  char *word;

  while (TRUE)
    {
      /* Find the start of a word */
      while (*p && g_unichar_isspace (g_utf8_get_char (p)))
	p = g_utf8_next_char (p);
      if (!*p)
	break;

      /* Find the end of the word */
      end = p;
      while (*end && !g_unichar_isspace (g_utf8_get_char (end)))
	end = g_utf8_next_char (end);

      word = g_strndup (p, end - p);
      g_print ("%s\n", word);
      g_free (word);

      p = end;
    }
}
/*< end_listing="print_words_utf8" >*/

/*< begin_listing="print_words_pango" >*/
void
print_words_pango (const char *input_text)
{
  int offset = 0;
  const char *p = input_text;
  const char *end;
  char *word;

  int n_chars = g_utf8_strlen (input_text, -1);
  PangoLogAttr *log_attrs = g_new (PangoLogAttr, n_chars);
  pango_get_log_attrs (input_text, -1, -1, "en", log_attrs);

  while (TRUE)
    {
      /* Find the start of a word */
      while (*p && !log_attrs[offset].is_word_start)
	{
	  p = g_utf8_next_char (p);
	  offset++;
	}
      if (!*p)
	break;

      /* Find the end of the word */
      end = p;
      while (*end && !log_attrs[offset].is_word_end)
	{
	  end = g_utf8_next_char (end);
	  offset++;
	}

      word = g_strndup (p, end - p);
      g_print ("%s\n", word);
      g_free (word);

      p = end;
    }

  g_free (log_attrs);
}
/*< end_listing="print_words_pango" >*/

int 
main (int argc, char **argv)
{
  const char *text = "Mary had a little lamb";
  
  g_type_init (0);

  print_words_ascii (text);
  print_words_utf8 (text);
  print_words_pango (text);
}
