       
Using 'you' to refer to reader, 'We' to refer to authors.

Signal notation should always use object::signal_name, or ::signal_name.

When describing a hierarchy of functions for a class, use eg. 'gtk_widget_ functions' 
instead of 'gtk_widget functions'

OWT: uhhh, OK, lets discuss this again tomorrow. 'gtk_widget_ functions'
is gross. If you want to talk about the names, say 'functions whose
names start with <literal>gtk_widget_</literal>'. If you just want
to talk about the functions, say "functions from the <classname>GtkWidget</classname>
class", or _maybe_ "<classname>GtkWidget</classname> functions."

Wording
=======

	TIMJ: "object oriented" or "object-oriented"?
	OWEN: "object-oriented" and "object orientation", Standard english usage.
	compound adjectives are hyphenated; compound nouns never.

DocBook Markup
==============


Enumeration
 
 <symbol>GTK_MOD1_MASK</symbol> (???)

Function
 
  <function>gtk_widget_new()</function>
  (Note the ()'s)

Function Arguments/Parameter
   
  ...the <parameter>widget</parameter> argument...
  (don't use parameter in plain text)

Function Groups

	...most widgets have a
	<function>gtk_<replaceable>classname</replaceable>_new()</function>
	function...
       
GtkObject

  <classname>GtkWidget</classname>

Signal name

  <literal>GtkWidget::expose_event</literal>

Structure (including GDK "objects" like GdkWindow)

  <structname>GdkWindow</structname>

Stucture field

  <structfield>window</structfield>

Function Macro

  <function>GTK_WIDGET()</function>
  
Symbolic Constant

  <symbol>GTK_WIN_POS_CENTER</symbol>

Program listings
  
  <programlisting>

widget = gtk_button_new();

  </programlisting>
  
Lists

  <itemizedlist mark="bullet">
    <listitem>
      <para>
        Widgets are created with a call to a
      </para>
    </listitem>
  </itemizedlist>

Coding Style:
=============

 - The coding style employed is generally that of GTK+

 - Open questions:

   gint, gchar, etc, or not? (OWT: I'd prefer not)
   always have include guards on header files?

   always forward declare structs or inline typedefs?
