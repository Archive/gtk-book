Overview
========
This directory contains a partially completed book about the 
GTK+ toolkit by Tim Janik, Ian Main, and Owen Taylor.

Work on this book was sponsored by O'Reilly and Associates,
who have generously allowed the completed portions to be made 
available under a free license.

The original work was done between January 1998 and June 2001;
the version of GTK+ covered is basically the GTK+-1.3 development
version as of early 2001. GTK+-2.0 addititions such as
GtkTextView, GtkTreeView, and GtkDialog are generally not
treated, but the object system portions have been updated
to correspond to an early version of GObject.

Legal Information
=================
Copyright Tim Janik, Ian Main, and Owen Taylor, 2003.

All rights reserved. 

(We expect to shortly place this work under the 
GNU Free Documentation License.)

Scope
=====
The original plan was for a 3 part book:

 Part 1: Writing GTK+ applications
    Tutorial
 Part 2: Digging deeper with GTK+ and GDK
    Advanced topics in GDK, Pango, and GTK+
 Part 3: Writing widgets

No prior experience with GTK+ is assumed, but the reader is
expected to be comfortable with C programming. Although the
book is not a reference manual, topics are covered with 
sufficient depth and detail that a reader would be expected
to refer back to the book as needed, rather than read it
once and never open it again.

Technical Details
=================
The text here is marked up in standard DocBook SGML. 

In order to make sure that the included function prototypes
and program listings are correct, there is a preprocessing
step that scans the SGML source for special comments and
makes updates. For instance, 

  <!-- begin_listing="addressbook-main.c#msgbox-example" -->
  <!-- end_listing -->

Means to scan the example program address-main.c, and copy
the portion between:

      /*< begin_listing="msgbox-example" >*/
      /*< end_listing="msgbox-example" >*/

Into the SGML source immediately after the <!-- begin_listing -->
special comment. Similarly, 

  <!-- begin_proto="gtklabel.h#gtk_label_set_text" -->
  <!-- end_proto="gtklabel.h#gtk_label_set_text" -->

Means to pull the prototype for gtk_label_set_text() from gtklabel.h,
format it in SGML, and insert it immediately after the 
<!-- begin_proto --> comment. The <!-- end_listing --> and 
<!-- end_proto --> comments allow previous insertions to 
be safely deleted.

Because this project was originally intended for print, the source
for all figures and images are .eps files. (For screenshots, these
are generally manually converted versions of .png files in
the graphics/ directory, for figures, the original source is generally
a .fig file in the graphics/ directory.)

The pstopnm program in the tools directory is used to convert
the .eps files to .gif files for HTML output.

Files and Directories
=====================
README: This file
PROGRESS: Obsolete file tracking work on the book
outline: Text outline of book
*.sgml: The main source of the book
*.eps: Figures and screenshots

examples: Examples used in text, in compilable form
examples/addressbook: The main example for Part I
examples/part_iii: Examples for Part III

graphics: Source files for screenshots and figures

tools: Utility programs
 convert-latex.pl: Tool used when converting the
   original TeX sources to SGML.
 pstopnm: Covert a PS file into a PNM image
 update-listings: Tool for updating examples and
   prototypes in the SGML source. (Described above)
 wc-nocomments: Count the words in a SGML file,
   ignoring comments.

Acknowledgements
================
The earliest work on the book was done as a project with
Red Hat; Ed Bailey and Carole Williams provided editing
and assistance.

Andy Oram was our editor at O'Reilly and provided extensive
suggestions, guidance, and support.

The original outline of the book is largely Shawn Amundson's 
work.
