GIFS=\
	addressbook-geo-whole.gif	\
	boxpack.gif			\
	dnd-screenshot.gif 		\
	extents.gif 			\
	gc-function.gif 		\
	geometry-main.gif 		\
	geometry-a.gif 			\
	geometry-b.gif 			\
	geometry-cd.gif 		\
	geometry-e.gif 			\
	geometry-f.gif			\
	gtklibs.gif			\
	layout.gif 			\
	request-alloc.gif		\
	linestyles.gif 			\
	line-bbox.gif			\
	mainloop.gif 			\
	boxpack.gif 			\
	nested2.gif 			\
	table.gif			\
	table-needed.gif		\
	table-ss.gif

CHAPTERS=\
	preface.sgml 			\
	introduction.sgml 		\
	getting-started.sgml 		\
        interacting.sgml                \
	advanced-packing.sgml 		\
	more-widgets.sgml 		\
	keyboard.sgml 			\
	rc-files.sgml 			\
	main-loop.sgml 			\
	debugging.sgml			\
	communication.sgml 		\
	gdk-intro.sgml 			\
	gtk-basics.sgml 		\
	events.sgml    			\
	drawing.sgml 			\
	more-drawing.sgml 		\
	advanced-gdk.sgml 		\
	internationalization.sgml 	\
	glib-chapter.sgml 		\
	signals-in-depth.sgml 		\
	widget-system.sgml 		\
	widget-writing-basics.sgml	\
	writing-gtkdial.sgml


SOURCES=\
	examples/glib_example.c 	\
	examples/eventbox.c 		\
	examples/addressbook.c 		\
	examples/addressbook1.c 	\
	examples/layout.c 		\
	examples/scribble1.c 		\
	examples/scribble2.c 		\
	examples/scribble3.c 		\
	examples/queue.c 		\
	examples/main-loop-ex.c 	\
	examples/boxpack.c

all:	main.ps

clean:
	rm -rf html *.dvi main.ps *.log *.aux *.tex *.ppm *.gif pstopnm-* s-update-listings

%.ppm: %.eps
	tools/pstopnm -portrait -xmax 1000 -ymax 1500 $<

%.gif: %.ppm
	pnmscale 0.5 $< | pnmgamma 0.8 | ppmtogif > $@

# Rules for building HTML

html: main.sgml $(CHAPTERS) s-update-listings $(GIFS)
	rm -rf html-tmp
	mkdir html-tmp
	cd html-tmp && jade -i html -t sgml -d ../gtkbook.dsl#html ../main.sgml
	for i in $(GIFS) ; do \
		cp $$i html-tmp ;\
	done
	rm -rf html
	mv html-tmp html


# Rules for building print output

main.tex: gtkbook.dsl main.sgml $(CHAPTERS) s-update-listings
	jade -t tex -d gtkbook.dsl#print main.sgml

main.dvi: main.tex
	for try in 1 2 3 ; do \
		jadetex main.tex | tee jadetex.log || break ; \
		grep "There were undefined references" jadetex.log > /dev/null || break ; \
	done
	rm -f jadetex.log

main.ps: main.dvi
	dvips -o main.ps main.dvi

# Rules to make sure that example programs included in the SGML
# files are correct

s-update-listings: $(SOURCES) $(CHAPTERS) tools/update-listings
	tools/update-listings $(CHAPTERS)
	touch s-update-listings

