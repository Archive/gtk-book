<chapter id="styles-themes">
  <title>Styles and Themes</title>

  <para>
    The functionality of a &gtk; application is completely under the
    control of the application's programmer. However, the appearance
    of the application is a combination of the programmer's design,
    the appearance of the &gtk; widgets and the user's settings. In
    fact, although &gtk; has a default appearance for its widgets, the
    user is able to download and install <firstterm>themes</firstterm>
    --- new sets of configuration files and code modules that can
    drastically change the appearance of &gtk;. In this chapter, we'll
    describe how applications can modify the way widgets appear and
    how users configure their setup to customize the appearance of
    &gtk;.
  </para>
  <para>
    Each &gtk; widget has an associated <firstterm>style</firstterm> which
    is a structure of type <structname>GtkStyle</structname> and
    encapsulates information about the way that the widget is drawn.
    The style includes the set of colors and font used to draw the widget,
    background pixmaps for the widget if they exist, and a pointer
    to the <firstterm>style class</firstterm>, a set of functions used
    for drawing the widget. The style class includes functions for drawing
    flat boxes, boxes with relief, arrows, check indicators, and many
    other common graphical elements used when drawing widgets.
    The style class can be &gtk;'s default style class or it can
    come from a <firstterm>theme engine</firstterm>: a loadable module
    containing code for drawing widgets. The theme engine can also attach
    data to the style that is specific to that theme engine.
  </para>
  <para>
    The style for a particular widget is determined by merging
    together a set of <firstterm>RC styles</firstterm> that
    apply to a given widget. RC styles are partial style descriptions.
    While a widget's style will include colors for every part
    of a widget in every state, a RC style might, for example,
    only say that the background of the widget in the insensitive
    state is blue. The ``RC'' in the name ``RC Style'' stands for
    ``resource configuration'' and refers to the fact that RC
    styles are usually defined and attached to particular widgets
    in <firstterm>resource configuration</firstterm> files.
    It is, however, also possible to define and attach RC styles in
    an application's code, and this is the mechanism that a programmer
    would use to set a particular color for a widget.
    The style for a given widget is determined by taking all
    RC styles that apply to the widget, merging them together,
    with RC styles that are more tightly bound to the widget
    taking precedence over less tightly bound styles, and finally
    using default values for any style options that are not
    set by any of the RC styles.
  </para>

  <sect1>
    <title>RC Files</title>
    <para>
      RC, or resource configuration, files allow the user to set
      options for &gtk;. When you call
      <function>gtk_init()</function>, &gtk; looks in a couple of
      locations for RC files to parse and reads all the files it
      finds. It checks for a file called <filename>.gtkrc</filename>
      in the user's home directory and for a system-wide
      <filename>gtkrc</filename> file. The location of the system-wide
      RC file depends on the way &gtk; was configured and installled;
      usually the location is <filename>/usr/local/etc/gtk/gtkrc</filename>
      or <filename>/etc/gtk/gtkrc</filename>. It is also possible
      for an application to add to the list of gtkrc files that &gtk;
      looks for using:
      <!-- begin_proto="gtkrc.h#gtk_rc_add_default_file" -->
<funcsynopsis>
  <funcdef>void <function>gtk_rc_add_default_file</function></funcdef>
  <paramdef>const gchar *<parameter>filename</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->
      In our addressbook, we add the file <filename>.addressbook.gtkrc</filename>
      in the user's home directory to this list of files.
      <programlisting>
	<!-- begin_listing="addressbook-main.c#gtk_rc_add_default_file" -->
  gchar *rc_file = g_strconcat (g_get_home_dir(), "/", ".addressbook.gtkrc", NULL);
  gtk_rc_add_default_file (rc_file);
  g_free (rc_file);
	<!-- end_listing -->
      </programlisting>
      This uses some &glib; functionality to construct the filename: first
      we retrieve the user's home directory using <function>g_get_home_dir()</function>
      and then we append together the home directory and filename using
      <function>g_strconcat</function>.
    </para>
    <para>
      In addition to being read directly, RC files can also be included
      from other RC files using a <literal>include</literal> statement.
      This is, in fact, how prepackaged themes work. Each theme is
      distributed as a RC file together with any pixmaps or other
      data that the theme needs. The user's RC file then simply has
      an <literal>include</literal> statement for the RC file of the selected
      theme. Typically, changing the user's gtkrc file in this way is
      done by a graphical tool, so the user doesn't need to actually
      edit their RC file by hand.
    </para>
    <para>
      There are a number of other things that statements in RC files can do: they
      can set global &gtk; options, they can can define new RC styles,
      they can bind certain RC files to certain widgets, and they can
      set up key bindings. We'll discuss key bindings in the next
      chapter. The statements allowable in a RC file that deal with styles
      and themes are:
    </para>
    <variablelist>
      <varlistentry>
	<term><literal>include "<replaceable>file</replaceable>"</literal></term>
	<listitem>
	  <para>
	    Parse another file.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><literal>pixmap_path "<replaceable>path</replaceable>"</literal></term>
	<listitem>
	  <para>
	    Set a path (a list of directories separated
	    by colons) that will be searched for pixmaps referenced in
	    RC files.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><literal>
	    style "<replaceable>name</replaceable>" [ = "<replaceable>parent</replaceable>" ] { ... }
	  </literal></term>
	<listitem>
	  <para>
	    Declare a RC style.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><literal>
	    widget "<replaceable>path</replaceable>" style "<replaceable>name</replaceable>"
	  </literal></term>
	<listitem>
	  <para>
	    Specify a RC style for a particular
	    group of widgets by matching on the widget path.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><literal>
	    widget_class "<replaceable>path</replaceable>" style "<replaceable>name</replaceable>"
	  </literal></term>
	<listitem>
	  <para>
	    Specify a RC style for a particular
	    group of widgets by matching on the class path.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><literal>
	    class "<replaceable>classname</replaceable>" style "<replaceable>name</replaceable>"
	  </literal></term>
	<listitem>
	  <para>
	    Specify a RC style for a particular branch of the inheritance tree.
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
    <sect2>
      <title>Defining RC Styles</title>
      <para>
	The <literal>style</literal> directive groups together
	a number of options for a widget to define a RC style.
	For instance, we can define a style called
	<literal>"gray_background"</literal> with the following
	lines:

	<programlisting>
style "gray_background" {
  font = "-adobe-courier-medium-r-*-*-*-120-*-*-*-*-iso8859-1"
  base[NORMAL] = { 0.9, 0.9, 0.9 }
  base[SELECTED] = { 0.5, 0.0, 0.0 }
}
	</programlisting>

	the options that this style sets are the font to be
	used for the widget and the <firstterm>base</firstterm>
	color, which is is the color used for the background
	of widgets such as text entry widgets. We set this
	color for two different states of the widget - the
	normal state and the selected state.
	
      </para>
      <para>
	Some of the options that can be set are:
	<variablelist>
	  <varlistentry>
	    <term><literal>bg</literal></term>
	    <listitem>
	      <para>
		Color used for the background of most widgets.
	      </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry>
	    <term><literal>fg</literal></term>
	    <listitem>
	      <para>
		Color used for drawing strings on top of the bg color.
	      </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry>
	    <term><literal>base</literal></term>
	    <listitem>
	      <para>
		Color used for the background of areas displaying editable
		text. The Editable widgets (Text, Entry, SpinButton),
		Lists, and ListItems, use this as their background.
	      </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry>
	    <term><literal>text</literal></term>
	    <listitem>
	      <para>
		Color used to draw strings on top of the base color.
	      </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry>
	    <term><literal>bg_pixmap</literal></term>
	    <listitem>
	      <para>
		Pixmap to be used as a background instead of 
		the ``bg'' color. (For the Text widget, it is
		used in place of the ``base'' color)
	      </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry>
	    <term><literal>font</literal></term>
	    <listitem>
	      <para>
		The font used to display strings. (For the Text widget,
		this is the default font when no other font is
		specified
	      </para>
	    </listitem>
	  </varlistentry>
	</variablelist>
      </para>
      <!-- The options not covered here are "fontset" and "engine" -->

      <para>
	Each color above can be specified differently for different
	<firstterm>states</firstterm>. Roughly, these state values
	correspond to different states of an entire
	widget - for instance, a button widget is set to the
	<literal>PRELIGHT</literal> state when the mouse is
	over it, and to the <literal>INSENSITIVE</literal> state
	when it is disabled using <function>gtk_widget_set_sensitive()</function>.
	<!-- FIXME? XREF -->. However, it is probably better to think of
	them as simply different color variants. Some widgets are drawn
	using the colors for multiple states simultanously. For instance, the
	the area of a scrollbar behind the thumb is drawn in
	using the <literal>ACTIVE</literal> color. The possible
	state values are:
	 
	<variablelist>
	  <varlistentry>
	    <term><literal>NORMAL</literal></term>
	    <listitem>
	      <para>
		A color used for a widget in its normal state.
	      </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry>
	    <term><literal>ACTIVE</literal></term>
	    <listitem>
	      <para>
		A variant of the NORMAL color used when the
		widget is in the ACTIVE state, and also for
		the trough of a ScrollBar, tabs of a NoteBook
		other than the current tab, and similar areas.
		This color should be a darker variant of the NORMAL color.
	      </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry>
	    <term><literal>PRELIGHT</literal></term>
	    <listitem>
	      <para>
		A color used for widgets in the prelight state. This
		state is used for buttons and menu items
		that have the mouse cursor over them, and for 
		their children.
	      </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry>
	    <term><literal>SELECTED</literal></term>
	    <listitem>
	      <para>
		A color used to highlight information
		selected by the user. For instance, it is used for the 
		selected <classname>GtkListItem</classname> widget's within a
		<classname>GtkList</classname> widget, and for the
		selection in an <classname>GtkEditable</classname> widget.
	      </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry>
	    <term><literal>INSENSITIVE</literal></term>
	    <listitem>
	      <para>
		A color used for widgets that have been set insenstive
		with <function>gtk_widget_set_sensitive()</function>.
	      </para>
	    </listitem>
	  </varlistentry>
	</variablelist>
      </para>
      <para>
	It is also possible for one style to be defined as a variant
	as another style. We can take the style above and add a
	a setting for the foreground color:
	
	<programlisting>
style "green_foregound" = "gray_background" {
  text[NORMAL] = { 0.8, 0.0, 0.0 }
}
	</programlisting>
      </para>
    </sect2>
    <sect2>
      <title>Attaching RC styles to widgets</title>
      <para>
	The above examples define RC styles, but until they are
	attached to some widgets, they won't actually have any effect
	on the appearance of an application. The
	<literal>class</literal>, <literal>widget</literal>, and
	<literal>widget_class</literal> statements are used to attach
	RC styles to widgets.  The <literal>class</literal>
	declaration indicates that the RC style applies to all widgets
	of a particular class, including widgets whose class derives
	from the given class.

	<programlisting>
class "GtkEditable" style "gray_background"
	</programlisting>

	indicates that the <literal>"gray_background"</literal> style
	applies to all widgets in classes deriving from <classname>GtkEditable</classname>,
	such as <classname>GtkEntry</classname>, <classname>GtkText</classname>,
	and <classname>GtkSpinButton</classname>.
      </para>
      <para>
	The <literal>class</literal> statement attaches a style to widgets
	without regard to the widgets location in the layout. Sometimes,
	we might want to make a widget appear differently depending
	on the class of its parent widget, or only apply a style
	to widgets in a particular toplevel. In these cases, we use
	the <literal>widget_class</literal> and <literal>widget</literal>
	statements. The <literal>widget_class</literal> statement
	attaches a RC style to widgets by matching against the widget's
	<literal>class path</literal>. The class path is formed by joining the names of
	the widget's parents and the widget with periods.
	A <classname>GtkEntry</classname> widget inside a <classname>GtkFrame</classname>
	widget inside a <classname>GtkHBox</classname> widget inside a
	<classname>GtkWindow</classname> widget, has the path:
	
	<programlisting>	    
GtkWindow.GtkHBox.GtkFrame.GtkButton
	</programlisting>
	
	This would be matched by such statements as:
	<programlisting>
widget_class "GtkWindow.*.GtkEntry" style "gray_background"
widget_class "GtkWindow.Gtk?Box.*" style "gray_background"
	</programlisting>
	The matching here uses shell-style `*' and `?' wildcards.
	(`*' matches any string of 0 or more characters, `?` matches exactly
	one of any character.) A common use of the
	<literal>widget_class</literal> statement is when you want to
	modify the color of the text inside buttons. Since the text
	is actually drawn by a label widget inside the button widget,
	the obvious statement:

	<programlisting>
class "GtkButton" style "my_button_text_style"
	</programlisting>

	does not work; instead you need to use:

	<programlisting>
widget_class "*.GtkButton.GtkLabel" style "my_button_text_style"
	</programlisting>

	The <literal>widget</literal> statement is similar, except
	that the application can set the name that is used for
	a particular widget using:
	<!-- begin_proto="gtkwidget.h#gtk_widget_set_name" -->
<funcsynopsis>
  <funcdef>void <function>gtk_widget_set_name</function></funcdef>
  <paramdef>GtkWidget *<parameter>widget</parameter></paramdef>
  <paramdef>const gchar *<parameter>name</parameter></paramdef>
</funcsynopsis>
	<!-- end_proto -->
	If we set the name of the <classname>GtkWindow</classname> widget
	in the above example with:
	
	<programlisting>
gtk_widget_set_name (window, "MainWindow");	  
	</programlisting>

	then we can attach our style to only entry widgets in that
	window, using statements such as:
	<programlisting>
widget_class "MainWindow.*.GtkEntry" style "gray_background"
	</programlisting>
      </para>
    </sect2>
  </sect1>
  <sect1>
    <title>Modifying the appearance of widgets in an application</title>

    <para>
      Sometimes, it is desirable to set the appearance of a widget
      from within an application. For instance, it might be useful
      to set the color of some text that we want to draw the
      user's attention to red. You should use such techniques sparingly.
      If the user has set a theme, the colors you choose may not
      be legible, and not all all choices of colors will be
      distinguishable to all users. There are two approaches to
      modifying the appearance of a widget. One approach is to
      set the style of a widget directly using <function>gtk_widget_set_style()</function>.
      However, if we do this, we'll completely override the user's
      settings from the RC file. So, a better approach is to
      create a RC style in the application and attach it to the
      widget. In addition to the mechanisms for attaching
      RC styles to widgets described in the last section, we can
      a RC style directly to a widget using: 
      
      <!-- begin_proto="gtkwidget.h#gtk_widget_modify_style" -->
<funcsynopsis>
  <funcdef>void <function>gtk_widget_modify_style</function></funcdef>
  <paramdef>GtkWidget *<parameter>widget</parameter></paramdef>
  <paramdef>GtkRcStyle *<parameter>style</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      An RC style is represented by a <structname>GtkRcStyle</structname>
      style:
      
      <programlisting>
struct _GtkRcStyle
{
  gchar *font_name;
  gchar *fontset_name;
  gchar *bg_pixmap_name[5];

  GtkRcFlags color_flags[5];
  GdkColor   fg[5];
  GdkColor   bg[5];
  GdkColor   text[5];
  GdkColor   base[5];

  /* other fields that should not be modified */

};
      </programlisting>

      The fields in this structure correspond closely
      to the parts of a widget's style that can be 
      modified within a RC file. An empty <structname>GtkRcStyle</structname>
      structure is created with  

      <!-- begin_proto="gtkrc.h#gtk_rc_style_new" -->
<funcsynopsis>
  <funcdef>GtkRcStyle *<function>gtk_rc_style_new</function></funcdef>
  <void>
</funcsynopsis>
      <!-- end_proto -->

      The <structfield>font_name</structfield>, <structfield>fontset_name</structfield>, 
      and <structfield>bg_pixmap_name</structfield> fields of this structure
      are filled in by assigning a string to the field.
      Since &gtk; will later free these strings
      with <function>g_free()</function>, these strings must be allocated from
      &glib; with a function like <function>g_malloc()</function>
      or <function>g_strdup()</function>. The difference between
      a RC style and a style is that a RC style only sets some
      options and leaves other unset, so for each option, we need
      a way specify if it set or not. For the fields which are
      strings, value of <symbol>NULL</symbol> means that they are
      not set.
    </para>
    <para>
      A slightly different approach is taken to represent
      whether the color fields are set or not. To set a
      color in the GtkRcStyle, one sets the appropriate
      GdkColor member of the structure, then sets the
      appropriate flag bit in <structfield>color_flags</structfield>. Each
      of the fields in this array corresponds to a different
      state of the widget, and has a value which is 
      the bitwise or of the constants <symbol>GTK_RC_FG</symbol>,
      <symbol>GTK_RC_BG</symbol>, <symbol>GTK_RC_TEXT</symbol>, and 
      <symbol>GTK_RC_BASE</symbol>. This indicates whether which
      of the colors <structfield>fg</structfield>,
      <structfield>bg</structfield>, <structfield>text</structfield>,
      and <structfield>base</structfield>.
    </para>
    <para>
      For instance, creating a button whose background will
      always be red looks like:

      <programlisting>
GtkWidget *button;
GtkRcStyle *rc_style;

rc_style = gtk_rc_style_new ();

/* Set the background to red */
rc_style->bg[GTK_STATE_NORMAL].red = 0xFFFF;
rc_style->bg[GTK_STATE_NORMAL].green = 0x0;
rc_style->bg[GTK_STATE_NORMAL].blue = 0x0;

/* Foreground to white */
rc_style->fg[GTK_STATE_NORMAL].red = 0xFFFF;
rc_style->fg[GTK_STATE_NORMAL].green = 0xFFFF;
rc_style->fg[GTK_STATE_NORMAL].blue = 0xFFFF;

rc_style->color_flags[GTK_STATE_NORMAL] = GTK_RC_FG | GTK_RC_BG;

gtk_widget_modify_style (widget, rc_style);
gtk_rc_style_unref (rc_style);
      </programlisting>

      Note that we unreference the RC style at the end,
      because we've set it for the widget, and no longer
      need to keep it around ourselves.
    </para>
<!-- FIXME: the following doesn't make much sense without Part II -->    
    <para>
      In a program, we can access the colors and fonts of a widget's
      style via the <structfield>style</structfield> member of the
      widget structure. We must make sure that the widget has its
      style assigned before accessing these elements, which we do by
      calling:

      <!-- begin_proto="gtkwidget.h#gtk_widget_ensure_style" -->
<funcsynopsis>
  <funcdef>void <function>gtk_widget_ensure_style</function></funcdef>
  <paramdef>GtkWidget *<parameter>widget</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      This is useful if we want to match graphics we draw
      ourselves to the widget's appearance. The GtkStyle
      structure that the <structfield>style</structfield> field
      points to looks, in part, like:

      <programlisting>
\begin{verbatim}
struct _GtkStyle
{
  GdkColor fg[5];
  GdkColor bg[5];
  GdkColor light[5];
  GdkColor dark[5];
  GdkColor mid[5];
  GdkColor text[5];
  GdkColor base[5];
  
  GdkColor black;
  GdkColor white;
  GdkFont *font;
  
  GdkGC *fg_gc[5];
  GdkGC *bg_gc[5];
  GdkGC *light_gc[5];
  GdkGC *dark_gc[5];
  GdkGC *mid_gc[5];
  GdkGC *text_gc[5];
  GdkGC *base_gc[5];
  GdkGC *black_gc;
  GdkGC *white_gc;
  
  GdkPixmap *bg_pixmap[5];

  /* More private fields */
}
      </programlisting>
    </para>
    <para>
      In addition, if we want to use the pixel values of the colors,
      we must make sure the widget is realized first. (Looking ahead
      to <xref linkend="drawing">, the style also includes graphics
      contexts corresponding to each color - for instance,
      <literal>style->bg_gc[GTK_STATE_NORMAL]</literal>. The widget
      must also be realized before accessing these members.)
    </para>
  </sect1>
  <sect1>
    <title>Themes and Theme Engines</title>
    <para>
      Fully describing how to write a theme engine, or describing the
      details of writing themes to go with a particular theme engine
      is beyond the scope of this book, but we'll give an overview
      of how themes and theme engines work in this chapter.
    </para>
    <para>
      As mentioned above, a theme is no more than a prepackaged RC
      file that the users can include into their own RC files. The
      simplest themes simply use the statements described above to
      set colors and background pixmaps. However, more ambitious
      themes tend to be based on theme engines that provide custom
      drawing code. This drawing code may be particular to the
      theme, or it may be shared between multiple themes. As an example
      of such a shared theme engine, many themes for &gtk; are
      based on the Pixmap theme engine. This theme engine, which
      is distributed as part of the standard <literal>gtk-engines</literal>
      package draws all parts of widgets by scaling and tiling
      pixmaps that are specified in the RC file.
    </para>
    <para>
      A theme engine is implemented as a dynamically loaded shared
      object. The theme engine used for a particular style is
      determined by the <literal>engine</literal> key word in the RC
      file. The <literal>engine</literal> statement gives the name of
      a theme engine to load for a particular style: for instance, we
      might have:
      <programlisting>
style "checkbutton" {
  engine "pixmap" {
    image 
      {
        function        = FLAT_BOX
	recolorable     = TRUE
	file            = "checkbutton.png"
	border          = { 3, 3, 3, 3 }
	stretch         = TRUE
      }
  }
}
class "GtkCheckButton" style "checkbutton"
      </programlisting>
      This declares a new RC style, the "checkbutton" style. Instead
      of setting colors or pixmaps for the style, it says to load
      the "pixmap" theme engine for the style, and then gives some
      details about the pixmaps to load which are particular to
      that theme engine. When &gtk; sees this declaration, it looks
      for a loadable module named <literal>pixmap</literal>, and if it is not
      already loaded, loads and initializes it.
    </para>
    <para>
      The shared object exports two functions:

<funcsynopsis>
  <funcdef>void <function>theme_init</function></funcdef>
  <paramdef>GtkThemeEngine *<parameter>engine</parameter></paramdef>
  <funcdef>void <function>theme_exit</function></funcdef>
  <paramdef>void</paramdef>
</funcsynopsis>

      the first is called when the theme engine is loaded, and is
      responsible for intializing the <structname>GtkThemeEngine</structname>
      structure passed in with pointers to various operations within
      the theme. The second function is called before the theme engine
      is unloaded, and is responsible for freeing any resources
      that the theme engine may be using.
    </para>
    <para>
      The <structname>GtkThemeEngine</structname> structure that
      is to be filled in looks like:
<programlisting>
struct _GtkThemeEngine {
  /* Fill in engine_data pointer in a GtkRcStyle by parsing contents
   * of brackets. Returns G_TOKEN_NONE if succesfull, otherwise returns
   * the token it expected but didn't get.
   */
  guint (*parse_rc_style)    (GScanner *scanner, GtkRcStyle *rc_style);
  
  /* Combine RC style data from src into dest. If 
   * dest->engine_data is NULL, it should be initialized to default
   * values.
   */
  void (*merge_rc_style)    (GtkRcStyle *dest,     GtkRcStyle *src);

  /* Fill in style->engine_data from rc_style->engine_data */
  void (*rc_style_to_style) (GtkStyle   *style, GtkRcStyle *rc_style);

  /* Duplicate engine_data from src to dest. The engine_data will
   * not subsequently be modified except by a call to realize_style()
   * so if realize_style() does nothing, refcounting is appropriate.
   */
  void (*duplicate_style)   (GtkStyle *dest,       GtkStyle *src);

  /* If style needs to initialize for a particular colormap/depth
   * combination, do it here. style->colormap/style->depth will have
   * been set at this point, and style itself initialized for 
   * the colormap
   */
  void (*realize_style) (GtkStyle   *new_style);

  /* If style needs to clean up for a particular colormap/depth
   * combination, do it here. 
   */
  void (*unrealize_style) (GtkStyle   *new_style);

  /* Clean up rc_style->engine_data before rc_style is destroyed */
  void (*destroy_rc_style)  (GtkRcStyle *rc_style);

  /* Clean up style->engine_data before style is destroyed */
  void (*destroy_style)     (GtkStyle   *style);

  /* Set the background of the window based on the style and state type */
  void (*set_background) (GtkStyle     *style,
			  GdkWindow    *window,
			  GtkStateType  state_type);
};
</programlisting>
      These functions here are responsible for manipulating styles
      associated with this theme engine. After loading the engine in
      response to an <literal>engine</literal> statement, &gtk; calls
      the <function>parse_rc_style()</function> function to parse the
      engine-specific information in the RC file. The theme engine
      then can attach information it parses from the RC to the
      RF Cfile.
    </para>
    <!-- IAN: RF Cfile ? -->
    <para>
      Engine-specific information is attached to a RC styles and styles
      <!-- IAN:                                            ^ -->
      using the <structfield>engine_data</structfield> pointer in
      the <structname>GtkRcStyle</structname> and
      <structname>GtkStyle</structname>. This is a generic pointer
      that can be used by the theme engine for whatever it wants.
      Generally, a theme engine will store a pointer to a structure
      holding its internal data in this location, though some theme
      <!-- IAN: it may be correct, but 'in' sounds odd above.. maybe at ? -->
      engines may not require any storage beyond the normal
      <structname>GtkStyle</structname> fields and thus just leave
      the <structfield>engine_data</structfield> fields unused.
    </para>
    <para>
      The remaining functions that the theme engine provides are
      responsible for handling the theme engines side of style
      manipulations. For instance, when &gtk; merges RC styles
      together to form a style, it first calls
      <function>merge_rc_style()</function> to merge the
      engine-specific information from the RC styles together, then
      calls <function>rc_style_to_style()</function> to copy the
      engine-specific information from the merged RC style into
      the style.
    </para>
    <para>
      The functions in <structname>GtkThemeEngine</structname> are used
      to handle manipulations of styles and RC styles, but not directly
      used for drawing. The drawing functions are stored in a different
      virtual table, the <structname>GtkStyleClass</structname> structure.
      As mentioned earlier in this chapter, each style structure contains
      a pointer to one of these <structname>GtkStyleClass</structname>
      structures. This structure contains pointers to functions to draw many
      different types of graphical elements. For instance, there is
      a pointer to a <function>draw_shadow()</function> which draws
      a rectangular shadow, such as the shadow around a <classname>GtkFrame</classname>
      widget. The prototype for this function, looks like:

      <funcsynopsis>
	<funcdef>void <function>draw_shadow</function></funcdef>
	<paramdef>GtkStyle *<parameter>style</parameter></paramdef>
	<paramdef>GtkStateType *<parameter>state_type</parameter></paramdef>
	<paramdef>GtkShadowType *<parameter>shadow_type</parameter></paramdef>
	<paramdef>GdkWindow *<parameter>window</parameter></paramdef>
	<paramdef>GdkRectangle *<parameter>area</parameter></paramdef>
	<paramdef>GtkWidget *<parameter>widget</parameter></paramdef>
	<paramdef>gchar *<parameter>detail</parameter></paramdef>
	<paramdef>gint<parameter>x</parameter></paramdef>
	<paramdef>gint<parameter>y</parameter></paramdef>
	<paramdef>gint<parameter>width</parameter></paramdef>
	<paramdef>gint<parameter>height</parameter></paramdef>
      </funcsynopsis>

      Most of the arguments here directly tell the theme engine how to
      draw the shadow. The <parameter>window</parameter> argument
      gives a <structname>GdkWindow</structname> on which to draw the
      shadow, the <parameter>state_type</parameter>, and
      <parameter>shadow_type</parameter> parameters indicate the state
      for the shadow, and the type of shadow, and the
      <parameter>x</parameter>, <parameter>y</parameter>,
      <parameter>width</parameter>, and <parameter>height</parameter>
      arguments indicate the geometry for the shadow. The
      <parameter>area</parameter> argument is a <firstterm>clip
      region</firstterm>. The theme engine must draw only the portion
      of the shadow inside the rectangle, even if the total size of
      the shadow is bigger. Finally, the <parameter>widget</parameter>
      and <parameter>detail</parameter> arguments allow the theme engine
      to go beyond just knowing to draw a shadow, and to do things
      differently depending on how the shadow is used. Both of
      these parameters are optional and may be <symbol>NULL</symbol>.
      If present, the <parameter>widget</parameter> argument is the
      widget which is being drawn, and the <parameter>detail</parameter>
      argument is a text string that gives some extra information
      about what part of the widget is being drawn. For instance,
      when drawing the background of an entry widget, the detail
      used is "entry_bg".
    </para>
    <para>
      This is only one of 21 separate drawing functions; if you are
      writing a theme engine, you should see the reference
      documentation for complete information about all these
      functions. To list them briefly, they are: draw_hline,
      draw_vline, draw_shadow, draw_polygon, draw_arrow, draw_diamond,
      draw_oval, draw_string, draw_box, draw_flat_box, draw_check,
      draw_option, draw_cross, draw_ramp, draw_tab, draw_shadow_gap,
      draw_box_gap, draw_extension, draw_focus, draw_slider, and
      draw_handle. Each of these functions draws a different type
      of graphical element.
    </para>
    <para>
      While we've described the drawing functions from the point of
      view of the theme engine above, it is also necessary to be aware
      of these functions when creating your own widgets, since if you
      want your widgets to appear properly with themes, you should be
      drawing the widget with the appropriate functions from the
      style. There is a function in the GTK+ API corresponding to each
      of the above drawing functions. For instance, corresponding to
      the <function>draw_shadow()</function> function in the
      <structname>GtkStyleClass</structname> structure, there is the
      function:
      
      <!-- begin_proto="gtkstyle.h#gtk_paint_shadow" -->
<funcsynopsis>
  <funcdef>void <function>gtk_paint_shadow</function></funcdef>
  <paramdef>GtkStyle *<parameter>style</parameter></paramdef>
  <paramdef>GdkWindow *<parameter>window</parameter></paramdef>
  <paramdef>GtkStateType <parameter>state_type</parameter></paramdef>
  <paramdef>GtkShadowType <parameter>shadow_type</parameter></paramdef>
  <paramdef>GdkRectangle *<parameter>area</parameter></paramdef>
  <paramdef>GtkWidget *<parameter>widget</parameter></paramdef>
  <paramdef>const gchar *<parameter>detail</parameter></paramdef>
  <paramdef>gint <parameter>x</parameter></paramdef>
  <paramdef>gint <parameter>y</parameter></paramdef>
  <paramdef>gint <parameter>width</parameter></paramdef>
  <paramdef>gint <parameter>height</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->
      
      which looks up the <function>draw_shadow() </function> function
      from the <structname>GtkStyleClass</structname> attached to the
      <parameter>style</parameter>, and calls that with the provided
      parameters.
    </para>
    <para>
      We've now covered described how to modify the appearance of
      your program from RC files and directly in the programs
      code, and briefly gone over how theme engines work. At this
      point we should reemphasize that the most attractive and
      most useable program will generally be the program that sticks
      close to the standard &gtk; appearance. Changing the appearance
      of your widgets is a technique that is sure to get the
      a reaction from your users, but unless you are quite careful,
      that reaction will be ``what an ugly program.''
    </para>
    <!-- IAN: reemphasize that they should use the rc method if they do
              want to change the appearance of a part of an application,
	      or to give the ability for the user to do so. -->
  </sect1>
</chapter>
<!-- Local Variables: -->
<!-- sgml-parent-document: ("main.sgml" "part" "chapter")  -->
<!-- End: -->

