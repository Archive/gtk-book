<chapter>
  <title>Advanced &gdk; techniques</title>
  
  <sect1>
    <title>Extended Input Devices</title>
    <para>
      One unique feature that &gdk; offers is support
      for input devices with capabilities beyond the
      standard keyboard and mouse. On the X platform,
      &gdk; provides capability through the X Input Extension.
      (Not to be be confused with the X Input Method
      Extension, covered in <xref linkend="i18n">).
      An input device, such as a graphics tablet,
      may provide:
      <itemizedlist>
	<listitem>
	  <para>
	    Position information with resolution greater
	    than 1 pixel.
	  </para>
	  <para>
	    Information about the pressure that the 
	    user is exerting on the device.
	  </para>
	  <para>
	    Information about the tilt of the device.
	  </para>
	</listitem>
      </itemizedlist>
      The event structures that &gdk; provides for such
      events as <structname>GdkEventMotion</structname>
      and <structname>GdkEventButton</structname> have
      been modified to take in account this additional
      information. The <structfield>x</structfield>
      and <structfield>y</structfield> fields in
      these structures are not integers, as might
      be expected, but doubles. Additional
      <structfield>pressure</structfield>
      <structfield>xtilt</structfield> and
      <structfield>ytilt</structfield> fields are also provided.
    </para>
    <para>
      There is also another type of information that is provided fore
      extended input devcies; information about the device itself. In
      an application such as a drawing program, it is convenient to
      assign different tools or colors to different devices, which
      allows the user to switch between presets merely by using a
      different device. In fact, graphics tablets may provided a
      stylus that appears as two separate devices, one for the tip and
      one for the eraser. This information is supported by two
      additional fields in the event structure, the
      <structfield>source</structfield> and
      <structfield>deviceid</structfield> fields.  The
      <structfield>source</structfield> field contains an enumeration
      value describing the type of device - it can be one of
      <symbol>GDK_SOURCE_MOUSE</symbol>,
      <symbol>GDK_SOURCE_PEN</symbol>,
      <symbol>GDK_SOURCE_ERASER</symbol>, or
      <symbol>GDK_SOURCE_CURSOR</symbol>. The deviceid contains an
      integer ID that identifies the device among available devices.
    </para>
    <para>
      Extended device support is not enabled by default, for
      several reasons. There is a small amount of
      extra processing involved in supporting extended
      input information, and, more importantly, on some
      systems, the integration between extended input
      devices and the standard mouse pointer is not complete,
      so an application may need to draw its own cursor
      to fully support all systems. Extended device
      suppport is turned on for a window using:

      <!-- begin_proto="gdkinput.h#gdk_input_set_extension_events" -->
<funcsynopsis>
  <funcdef>void <function>gdk_input_set_extension_events</function></funcdef>
  <paramdef>GdkWindow *<parameter>window</parameter></paramdef>
  <paramdef>gint <parameter>mask</parameter></paramdef>
  <paramdef>GdkExtensionMode <parameter>mode</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      The <parameter>mask</parameter> parameter is
      a bitmask of values of type <structname>GdkEventMask</structname>
      <!-- FIXME: What do we use for enumerations??? -->, indicating
      which extension events the application is interested
      in receiving, while <parameter>mode</parameter> is
      one of <symbol>GDK_EXTENSION_EVENTS_NONE</symbol>,
      <symbol>GDK_EXTENSION_EVENTS_ALL</symbol>,
      <symbol>GDK_EXTENSION_EVENTS_CURSOR</symbol>.
      Both <symbol>GDK_EXTENSION_EVENTS_ALL</symbol>, and
      <symbol>GDK_EXTENSION_EVENTS_CURSOR</symbol> indicate
      that the application is interested in receiving
      extension events, but if the latter is specified,
      then extension events are turned on only for devices
      where turning them on would not require the application
      to draw its own cursor.
    </para>
    <para>

      At the widget level, turning on extension events can be
      done with:

      <!-- begin_proto="gtkwidget.h#gtk_widget_set_extension_events" -->
<funcsynopsis>
  <funcdef>void <function>gtk_widget_set_extension_events</function></funcdef>
  <paramdef>GtkWidget *<parameter>widget</parameter></paramdef>
  <paramdef>GdkExtensionMode <parameter>mode</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      Doing it this way has the advantage that the widget->window
      need not exist. If the widget is not realized,
      then the extension mode will be stored and applied
      when the widget is realized. The mask when using this
      function is the same as the normal event mask set
      with <function>gdk_widget_set_events</function>.
    </para>
    <para>
      We'll extend our example program in two ways
      to support extended input devices. First, we'll
      modulate the size of the brush we use according
      to the reported pressure. GTK+ reports a pressure
      between 0.0 and 1.0, with 0.5 reported for
      input from non-pressure-supporting devices,
      such as a mouse. So we'll change the hard
      coded brush size of 10 to <literal>20 * pressure</literal>.
      Second, when the device is reported as a
      eraser device, we'll draw in white instead
      of in the color the user has selected. The
      <function>draw_brush()</function> now takes
      additional <parameter>pressure</parameter> and
      <parameter>source</parameter> parameters, and
      looks like:
    </para>
    <programlisting>
      <!-- begin_listing="scribble4.c#draw_brush" -->
/* Draw a rectangle on the screen */
static void
draw_brush (GtkWidget     *widget,
            gdouble        x,
            gdouble        y,
            gdouble        pressure,
            GdkInputSource source)
{
  GdkRectangle update_rect;
  gint diameter = pressure * 10;

  update_rect.x = x - diameter;
  update_rect.y = y - diameter;
  update_rect.width = 2 * diameter;
  update_rect.height = 2 * diameter;
  gdk_draw_rectangle (pixmap,
                      source == GDK_SOURCE_ERASER ? 
                           widget-&gt;style-&gt;white_gc : 
                           draw_gc,
                      TRUE,
                      update_rect.x, update_rect.y,
                      update_rect.width, update_rect.height);
  gdk_window_invalidate_rect (widget-&gt;window, &amp;update_rect, FALSE);
}
      <!-- end_listing -->
    </programlisting>
    <para>
      There are a number of things that a user can configure
      for an input device; to provide an interface for
      this configuration, GTK+ provides a standard dialog
      for configuring input devices, the
      <classname>GtkInputDialog</classname> widget. Our
      example program provides a button for bringing
      up this widget, which is attached to the following
      function:
    </para>
    <programlisting>
      <!-- begin_listing="scribble4.c#create_input_dialog" -->
void
create_input_dialog (void)
{
  static GtkWidget *inputd = NULL;

  if (!inputd)
    {
      inputd = gtk_input_dialog_new();

      gtk_signal_connect (GTK_OBJECT(inputd), "destroy",
                          GTK_SIGNAL_FUNC (gtk_widget_destroyed),
                          &amp;inputd);
      gtk_signal_connect_object (GTK_OBJECT(GTK_INPUT_DIALOG(inputd)-&gt;close_button),
                                 "clicked",
                                 GTK_SIGNAL_FUNC (gtk_widget_hide),
                                 GTK_OBJECT(inputd));
      gtk_widget_hide (GTK_INPUT_DIALOG(inputd)-&gt;save_button);
    }

  gtk_widget_show (inputd);
}
      <!-- end_listing -->
    </programlisting>
    <para>
      A more sophisticated program would not just allow
      the user to edit the settings, but would also allow
      the user to save settings for future use.
    </para>
  </sect1>

  <!-- OWT: The following section most certainly does
            not belong in the "advanced" chapter -->
  <sect1>
    <title>Loading Pixmaps</title>
    <para>
      In <xref linkend="drawing"> we described how to
      create empty pixmaps and bitmaps that we could
      draw upon. In other circumstances, we may be
      interested in loading up pixmaps and bitmaps
      with pre-created image data - either from a file
      on disk or from inline data in our application.
      &gdk; provides facilities to do this as well.
    </para>
    <para>
      To create a color image from a pixmap on disk
      in XPM format <!-- FIXME: give a reference
      on this format --> we can use:
      <!-- begin_proto="gdkpixmap.h#gdk_pixmap_colormap_create_from_xpm" -->
<funcsynopsis>
  <funcdef>GdkPixmap *<function>gdk_pixmap_colormap_create_from_xpm</function></funcdef>
  <paramdef>GdkDrawable *<parameter>drawable</parameter></paramdef>
  <paramdef>GdkColormap *<parameter>colormap</parameter></paramdef>
  <paramdef>GdkBitmap **<parameter>mask</parameter></paramdef>
  <paramdef>GdkColor *<parameter>transparent_color</parameter></paramdef>
  <paramdef>const gchar *<parameter>filename</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->
      The word ``colormap'' in the name here refers to the
      fact that when creating a color image, we need
      to provide &gdk; with a colormap that it can use
      for the colors in the image. (&gdk; will allocate colors
      in this colormap which will be unreferenced and
      possibly released when the pixmap is destroyed).
      There are two ways to provide a colormap. We can
      either provide a <structname>GdkWindow</structname>
      whose colormap will be used, or we can alternatively
      provide a colormap directly.
      Because we can obtain the colormap for a widget without
      requiring that it be realized, the latter is most
      often the more convenient approach.
    </para>
    <para>
      XPM's can have transparent portions, and the 
      <parameter>mask</parameter> and
      <parameter>transparent_color</parameter> parameters
      relate to this. <parameter>mask</parameter> gives
      a location to store a bitmap that will be created
      to hold the transparency information; this mask
      can later be used as a shape-mask for a window
      (see <xref linkend="shaped-windows">) or as a
      the clip mask for a graphics context. If
      <parameter>mask</parameter> is <symbol>NULL</symbol>,
      then no such bitmap will be created. But, even
      if <parameter>mask</parameter> is present, &gdk;
      still needs some color to use for the portions
      of the returned pixmap corresponding to transparent
      portions of the XPM. This is what
      <parameter>transparent_color</parameter> is for.
      It can also be <symbol>NULL</symbol> in which
      case white will be used.
    </para>
    <para>
      Our addressbook program uses pixmaps in XPM format
      for its toolbar. We load up each icon using the
      following command:
      <programlisting>
	<!-- begin_listing="addressbook.c#create_pixmap" -->
  GdkPixmap *pixmap;
  GdkBitmap *mask;
  
  pixmap = gdk_pixmap_colormap_create_from_xpm (NULL,
                                                gtk_widget_get_colormap (GTK_WIDGET (toolbar)),
                                                &amp;mask,
                                                NULL,
                                                item-&gt;pixmapfile);
	<!-- end_listing="addressbook.c#create_pixmap" -->
      </programlisting>
      Here we leave <parameter>window</parameter>
      <symbol>NULL</symbol> and provide the colormap
      explicitely.
    </para>
    <para>
      If we want to load up XPM data from an XPM included
      in our source code (an XPM is actually formatted
      as valid C source code) then we can use
      <function>gdk_pixmap_colormap_create_from_xpm_d()</function>.
      We can also create bitmaps directly from data
      using <function>gdk_bitmap_create_from_data</function>.
      The data here is simply the contents of the bitmap
      stored at 1 bit per pixel. <!-- OWT: Do we need
      need to say the endianness? What is it? Should
      we reference the XBM spec here? -->
     </para>
  </sect1>
  
  <sect1 id="shaped-windows">
    <title>Shaped Windows</title>

    <para>
      So far, we've been dealing with rectangular windows. On
      X Servers supporting the X Shape Extension, it is also possible
      to have arbitrarily shaped windows. To set the shape on
      a window, we use:

      <!-- begin_proto="gdkwindow.h#gdk_window_shape_combine_mask" -->
<funcsynopsis>
  <funcdef>void <function>gdk_window_shape_combine_mask</function></funcdef>
  <paramdef>GdkWindow *<parameter>window</parameter></paramdef>
  <paramdef>GdkBitmap *<parameter>mask</parameter></paramdef>
  <paramdef>gint <parameter>x</parameter></paramdef>
  <paramdef>gint <parameter>y</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      This function specifies then window's shape via a bitmap
      mask. Only the pixels in the window corresponding to 1's
      in the shape mask will be visible on screen. Where the
      bitmap has a 0, whatever is underneath the window will
      show through. Similarly, events will only be trapped
      on portions of the window corresponding to 1's.
      The <parameter>offset_x</parameter>
      and <parameter>offset_y</parameter> parameters specifiy
      an offset of the shape mask with respect to the origin
      of the window.
    </para>
    <para>
      For widgets, the preferred way to set a shape is using
      
      <!-- begin_proto="gtkwidget.h#gtk_widget_shape_combine_mask" -->
<funcsynopsis>
  <funcdef>void <function>gtk_widget_shape_combine_mask</function></funcdef>
  <paramdef>GtkWidget *<parameter>widget</parameter></paramdef>
  <paramdef>GdkBitmap *<parameter>shape_mask</parameter></paramdef>
  <paramdef>gint <parameter>offset_x</parameter></paramdef>
  <paramdef>gint <parameter>offset_y</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      This can be called before the widget is realized, and, in
      addition, interacts better with themes. Along with setting the
      shape for the widget's window, it also sets a flag indicating
      that the shape of the widget has been set by the
      application, instead of by the theme. When the theme
      is switched, GTK+ goes through and resets the shapes
      of all widgets that don't have this flag set, so if
      you set the shape of your applications window
      using <function>gdk_window_shape_combine_mask</function>
      directly, that shape will be lost if the user switches
      the current theme.
    </para>
  </sect1>
</chapter>
  
<!-- Local Variables: -->
<!-- sgml-parent-document: ("main.sgml" "part" "chapter")  -->
<!-- End: -->


