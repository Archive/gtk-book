<!-- TODO: The information on GdkRGB needs to be first, this chapter
           is really intimidating needs to gradually lead the user
           up to Visuals/Colormaps. Or does it? Perhaps that can
           be relegated to the reference -->

<chapter>
  <title>More on Drawing</title>

  <sect1>
    <title>Working with Visuals</title>
    <para>
      The X Window System works on many different types of graphics
      displays. A particular type of graphics display is identified by
      a <structname>GdkVisual</structname> structure, which contains
      information about how images are represented on the screen.  For
      example, a <structname>GdkVisual</structname> structure contains
      information about how many bits are stored for each pixel. It
      also contains the general visual type, which is accessed as
      <literal>visual->type</literal>. For a list of all visual types,
      see <xref linkend="visualtypes">.
    </para>
    <figure id="visualtypes">
      <title>Visual Types</title>
      <informaltable>
	<tgroup cols="1">
	  <colspec align="left">
	  <tbody>
	    <row><entry><symbol>GDK_VISUAL_STATIC_GRAY</symbol></entry></row>
	    <row><entry><symbol>GDK_VISUAL_GRAYSCALE</symbol></entry></row>
	    <row><entry><symbol>GDK_VISUAL_STATIC_COLOR</symbol></entry></row>
	    <row><entry><symbol>GDK_VISUAL_PSEUDO_COLOR</symbol></entry></row>
	    <row><entry><symbol>GDK_VISUAL_TRUE_COLOR</symbol></entry></row>
	    <row><entry><symbol>GDK_VISUAL_DIRECT_COLOR</symbol></entry></row>
	  </tbody>
	</tgroup>
      </informaltable>
    </figure>
    <para>
      The most common types of visuals are true-color and pseudo-color
      visuals.  A true-color visual display is capable of showing the
      full range of colors on screen at once.  A pseudo-color display
      can only display a limited palette of colors at once and the
      colors are selected by the application.
    </para>
    <para>
      Direct-color visuals are like true-color visuals, except that
      applications can define a mapping between the values in memory
      and the displayed pixels.  Static-gray visuals are like
      true-color visuals, but in monochrome.  Grayscale visuals are
      like direct-color visuals in monochrome.  Static-color visuals,
      like pseudo-color visuals, have a limited set of colors, but
      static-color visuals' colors are determined in advance by the X
      server, and not chosen by the application.
    </para>
    <para>
      Some X servers may support multiple visuals on the screen at
      once.  Commonly, a workstation will be able to display both a
      pseudo-color visual with 8 bits per pixel and a true-color
      visual with 24 bits per pixel at the same time.
    </para>
    <para>
      &gdk; includes a wide range of functions to query what visuals are
      available. If your application does not explicitly change things, the
      system's default visual will be used.  The system's default visual can be
      directly obtained with:

      <!-- begin_proto="gdkvisual.h#gdk_visual_get_system" -->
<funcsynopsis>
  <funcdef>GdkVisual *<function>gdk_visual_get_system</function></funcdef>
  <void>
</funcsynopsis>
      <!-- end_proto -->

      To obtain the ``best'' visual, use the following call:

      <!-- begin_proto="gdkvisual.h#gdk_visual_get_best" -->
<funcsynopsis>
  <funcdef>GdkVisual *<function>gdk_visual_get_best</function></funcdef>
  <void>
</funcsynopsis>
      <!-- end_proto -->

      If true-color visuals are available, the best visual will be the
      true-color visual with the most bits per pixel.  The number of
      bits per pixel is called the <firstterm>depth</firstterm> of a
      visual, so the best visual is the true-color visual with the
      greatest depth.
    </para>
  </sect1>
  <sect1>
    <title>Colors and Color Handling</title>
    <para>
      A color in &gdk; is represented by a structure of type GdkColor:
      <programlisting>
struct _GdkColor
{
  gulong  pixel;
  gushort red;
  gushort green;
  gushort blue;
};
      </programlisting>

      The <structfield>red</structfield>,
      <structfield>green</structfield> and
      <structfield>blue</structfield> members of the structure give
      the red, green, and blue components of a color, ranging from
      0--65535. For example, a pure white is represented with
      <structfield>red</structfield>, <structfield>green</structfield>
      and <structfield>blue</structfield> all equal to 65535.
    </para>
    <para>
      The <structfield>pixel</structfield> member will be filled in
      when the color is initially
      allocated. <structfield>pixel</structfield> is the pixel value
      that will be stored into display memory. For a pseudo-color
      visual, it provides an index into the colormap; for a true-color
      visual, it contains the red, green, and blue values combined.
      Normally, you won't need to directly access this value.
    </para>
    <para>
      Colors are allocated from colormaps. A true-color visual will
      typically have only a single colormap.  Allocating a color in
      that case simply means finding the closest approximation to the
      requested color (although colors are specified with 16 bits of
      precision per component, most displays can only store five, six,
      or eight bits per component).
    </para>
    <para>
      For a pseudo-color visual, allocating a color is a more complex
      procedure.  Typically, a default system colormap exists, but
      applications can also create their own private colormaps. This
      ability is useful if the system colormap has already been used
      up by other applications, and the application wants to allocate
      a group of colors.
    </para>
    <para>
      However, except for some high-end workstations, most hardware is
      only capable of displaying one colormap at a time.  If an
      application uses a private colormap, the current colormap will
      change as the user moves the pointer into or out of the
      application's window. When the pointer is inside the
      application's window, all other applications will be displayed
      with incorrect and often garish colors (a phenomenon known as
      ``technicolor''), and vice-versa.
    </para>
    <para>
      Another twist on allocating colors in pseudo-color visuals:
      colors can be allocated either read-only or read-write.  Once a
      read-only color is allocated, it cannot be changed.  If multiple
      applications request the same color read-write, the allocated
      space in the colormap will be shared.
    </para>
    <para>
      On the other hand, colors allocated read-write can be changed
      with <function>gdk_colormap_change_color()</function>. But
      because they can be changed at any time, they cannot be shared
      between applications. Read-write allocation of colors is
      occasionally useful for special effects.  For example, changing
      the colormap of an image is a very cheap way of displaying a
      motion effect.  However, in most cases, read-only allocation
      should be preferred because it allows color sharing.
    </para>
    <para>
      Similarly to how visuals work, the default colormap for the
      system can be obtained with the following function:

      <!-- begin_proto="gdkcolor.h#gdk_colormap_get_system" -->
<funcsynopsis>
  <funcdef>GdkColormap *<function>gdk_colormap_get_system</function></funcdef>
  <void>
</funcsynopsis>
      <!-- end_proto -->

      If you are using a non-default visual, you have to create your
      colormap yourself with:

      <!-- begin_proto="gdkcolor.h#gdk_colormap_new" -->
<funcsynopsis>
  <funcdef>GdkColormap *<function>gdk_colormap_new</function></funcdef>
  <paramdef>GdkVisual *<parameter>visual</parameter></paramdef>
  <paramdef>gboolean <parameter>allocate</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      For pseudo-color visuals, if the <parameter>allocate</parameter>
      parameter is true, then all colors in the colormap will be
      immediately allocated as read-write.
    </para>
    <para>
      To allocate a color, you first fill in the red, green, and blue
      components of the GdkColor structure, and then call:
      
      <!-- begin_proto="gdkcolor.h#gdk_colormap_alloc_color" -->
<funcsynopsis>
  <funcdef>gboolean <function>gdk_colormap_alloc_color</function></funcdef>
  <paramdef>GdkColormap *<parameter>colormap</parameter></paramdef>
  <paramdef>GdkColor *<parameter>color</parameter></paramdef>
  <paramdef>gboolean <parameter>writeable</parameter></paramdef>
  <paramdef>gboolean <parameter>best_match</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      If <parameter>writeable</parameter> is true, &gdk; will
      try to allocate the color read-write, otherwise it will allocate
      it read-only. If the color cannot be allocated, and
      <parameter>best_match</parameter> is true, then &gdk; will fall
      back to the closest matching color it can allocate.
      <parameter>writeable</parameter> and
      <parameter>best_match</parameter> cannot be specified
      simultaneously. If the color cannot be allocated, &gdk; returns
      <symbol>FALSE</symbol>. The function
      <function>gdk_colormap_alloc_colors()</function> allocates a
      number of colors at once.
    </para>
    <para>
      When you're finished using a color, you can free it with:

      <!-- begin_proto="gdkcolor.h#gdk_colormap_free_colors" -->
<funcsynopsis>
  <funcdef>void <function>gdk_colormap_free_colors</function></funcdef>
  <paramdef>GdkColormap *<parameter>colormap</parameter></paramdef>
  <paramdef>GdkColor *<parameter>colors</parameter></paramdef>
  <paramdef>gint <parameter>ncolors</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      This function takes a pointer to an array of colors. To free a
      single color, you can call the function as:
      <programlisting>
gdk_colormap_free_colors (cmap, &amp;color, 1);
      </programlisting>
    </para>
    <para>
      <xref linkend="coloralloc"> shows some code that we'll use to change the
      color we draw with in Scribble.
      <!-- 
      CW(editor): do we want to include code as a figure here?  Previously, 
      code has always been inserted directly into the text.
      -->

      <figure id="coloralloc" float="1">
	<title>Allocating a color</title>
	<programlisting>
	  <!-- begin_listing="scribble3.c#change_color0"-->
static GdkColor *draw_color = NULL;
static GdkGC *draw_gc = NULL;
	  <!-- end_listing -->

	  <!-- begin_listing="scribble3.c#change_color1"-->
/* Change the current color and graphics context */
void
change_color (GtkWidget *widget, GdkColor *color)
{
  GdkColormap *cmap = gtk_widget_get_colormap (widget);
  
  if (draw_color)
    gdk_colors_free (cmap, &amp;draw_color-&gt;pixel, 1, 0);
  else
    draw_color = g_new (GdkColor, 1);

  *draw_color = *color;

  gdk_color_alloc (cmap, draw_color);
  
  if (!draw_gc)
    draw_gc = gdk_gc_new (widget-&gt;window);

  gdk_gc_set_foreground (draw_gc, draw_color);
}
	  <!-- end_listing="scribble3.c#change_color1"-->
	</programlisting>
      </figure>
    </para>
  </sect1>
  <sect1>
    <title>Colormaps, Visuals and &gtk;</title>
    <para>
      Each widget has an associated colormap and visual. The
      colormap and visual cannot be changed during the lifetime
      of a widget. &gtk;'s default colormap and visual can be
      set with <function>gtk_widget_set_default_visual()</function> and
      <function>gtk_widget_set_default_colormap()</function>.
    </para>
    <para>
      The default colormap and visual can be temporarily overriden by
      calling <function>gtk_widget_push_visual()</function> and
      <function>gtk_widget_push_colormap()</function>.  The visual and
      colormap set by these functions are used for all widgets created
      until <function>gtk_widget_pop_visual()</function> and
      <function>gtk_widget_pop_colormap()</function> are called. Calls
      to these functions can be nested.
    </para>
  </sect1>
  <sect1>
    <title>GdkRGB</title>
    <para>
      Even if you absorbed and understood all of the information
      provided in the previous sections about colormaps and visuals,
      you'll still need to know a lot more in order to take, for
      example, a picture represented as 24 bit data and draw it into a
      window. To do this efficiently, you need to know how the data is
      stored on the server (in one of many different possible
      formats). Also, if only a few colors are available, it would be
      nice to be able to dither them to approximate other colors.
      Luckily, &gdk; provides functions to do this for you.
      
      <funcsynopsis>
	<funcdef>void <function>gdk_draw_rgb_image</function></funcdef>
	<paramdef>GdkDrawable *<parameter>drawable</parameter></paramdef>
	<paramdef>GdkGC *<parameter>gc</parameter></paramdef>
	<paramdef>gint <parameter>x</parameter></paramdef>
	<paramdef>gint <parameter>y</parameter></paramdef>
	<paramdef>gint <parameter>width</parameter></paramdef>
	<paramdef>gint <parameter>height</parameter></paramdef>
	<paramdef>GdkRgbDither <parameter>dith</parameter></paramdef>
	<paramdef>guchar * <parameter>rgb_buf</parameter></paramdef>
	<paramdef>gint <parameter>rowstride</parameter></paramdef>
      </funcsynopsis>

      This function takes a pointer to raw RGB data (that is, the
      image is represented as a sequence of bytes, with 3 bytes ---
      red, green and blue --- for each pixel), and then renders the
      data into a drawable.
    </para>
    <para>
      <parameter>x</parameter> and <parameter>y</parameter> give a
      location within the drawable to render the data.
      <parameter>width</parameter> and <parameter>height</parameter>
      provide the size of the area to draw.
      <parameter>rgb_buf</parameter> points to the data to draw.
      <parameter>rowstride</parameter> gives the number of bytes
      necessary to go from one row to another.
    </para>
    <para>
      The following lines provide a short example of how to use
      GDK_RGB:
      <programlisting>      
#define WIDTH 100
#define HEIGHT 100

static guchar big_image[HEIGHT][3*WIDTH] = {
  [...]
}

static gint
expose_event (GtkWidget *widget, GdkEventExpose *event)
{
  gdk_draw_rgb_image (widget->window,
		      widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
 		      event->area.x, event->area.y,
		      event->area.width, event->area.height,
                      GDK_RGB_DITHER_NORMAL,
                      big_image + 3*(WIDTH+event->area.y 
                                + event->area.x), 
		      3*WIDTH);

  return FALSE;
}
	</programlisting>
      Note that since we are drawing some sub-portion of a larger
      image, we set <parameter>rgb_buf</parameter> to point to the
      offset in the drawn image where the data we want to draw begins
      (instead of to the beginning of the drawn image).
    </para>
    <para>
      For pseudo-color visuals, &gdk; needs to have a <firstterm>color
      cube</firstterm> with which to draw.  Because of this, the first
      time <function>gdk_draw_rgb_image()</function> is called for a
      drawable with a particular colormap, it tries to find an
      existing color cube in that colormap.  If it can't find one, it
      allocates additional colors in the colormap to make one.
    </para>
  </sect1>
  <sect1>
    <title>Fonts and Drawing Strings</title> 
    <para>
      Fonts in &gdk; are identified by X Logical Font Descriptors
      (XLFDs). XLFDs are strings with options separated by
      <literal>'-'</literal> (hyphen) characters. You can load a font
      with a particular XLFD using the following function:

      <!-- begin_proto="gdkfont.h#gdk_font_load" -->
<funcsynopsis>
  <funcdef>GdkFont *<function>gdk_font_load</function></funcdef>
  <paramdef>const gchar *<parameter>font_name</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      This function creates a font with a reference count of one. The
      font can subsequently be referenced and unreferenced with
      <function>gdk_font_ref()</function> and
      <function>gdk_font_unref()</function>. As always, when the
      reference count for a font goes to zero, the font is freed.
    </para>
    <para>
      Fonts, not suprisingly, are used to draw strings. The
      simplest way to draw a string is to use:

      <!-- begin_proto="gdkdrawable.h#gdk_draw_string" -->
<funcsynopsis>
  <funcdef>void <function>gdk_draw_string</function></funcdef>
  <paramdef>GdkDrawable *<parameter>drawable</parameter></paramdef>
  <paramdef>GdkFont *<parameter>font</parameter></paramdef>
  <paramdef>GdkGC *<parameter>gc</parameter></paramdef>
  <paramdef>gint <parameter>x</parameter></paramdef>
  <paramdef>gint <parameter>y</parameter></paramdef>
  <paramdef>const gchar *<parameter>string</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->
    </para>
    <para>
      When drawing a string, we may want to know the dimensions of the
      text so we can align it properly. The following dimensions can
      be accessed:
      <variablelist>
	<varlistentry>
	  <term>ascent</term>
	  <listitem><para>
	      The distance the drawn string extends above the origin.
	  </para></listitem>
	</varlistentry>
	<varlistentry>
	  <term>descent</term>
	  <listitem><para>
	      The distance the drawn string extends below the origin.
	  </para></listitem>
	</varlistentry>
	<varlistentry>
	  <term>rbearing</term>
	  <listitem><para>
	      The distance the drawn string extends to the right of the origin.
	  </para></listitem>
	</varlistentry>
	<varlistentry>
	  <term>lbearing</term>
	  <listitem><para>
	      The distance the drawn string extends to the left of the origin.
	  </para></listitem>
	</varlistentry>
	<varlistentry>
	  <term>width</term>
	  <listitem><para>
	      The horizontal distance from the origin to where the
	      character in the string after the last character would be drawn (if
	      there were one).
	  </para></listitem>
	</varlistentry>
      </variablelist>
      If all of the characters fit neatly inside a box, then
      we'd have an rbearing of zero and an lbearing equal to
      the width. However, a character like an italic `f' extends
      outside of its box, as illustrated in <xref linkend="extents">.
      These dimensions are obtained by calling:

      <!-- begin_proto="gdkfont.h#gdk_string_extents" -->
<funcsynopsis>
  <funcdef>void <function>gdk_string_extents</function></funcdef>
  <paramdef>GdkFont *<parameter>font</parameter></paramdef>
  <paramdef>const gchar *<parameter>string</parameter></paramdef>
  <paramdef>gint *<parameter>lbearing</parameter></paramdef>
  <paramdef>gint *<parameter>rbearing</parameter></paramdef>
  <paramdef>gint *<parameter>width</parameter></paramdef>
  <paramdef>gint *<parameter>ascent</parameter></paramdef>
  <paramdef>gint *<parameter>descent</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->
      <figure id="extents" float="1">
	<title>
	  The extents of a text string.
	</title>
	<graphic fileref="extents" format="gif"></graphic>
      </figure>
    </para>
  </sect1>
  <sect1>
    <title>Regions</title>
    <para>
      &gdk; has a <firstterm>region</firstterm> datatype, which represents an
      arbitrarily shaped area on the screen. Calculations can
      be done on regions.
    </para>
    <para>
      An empty region can be created with
      <function>gdk_region_new()</function>, or a region can be
      created from a polygon with
      <function>gdk_region_polygon()</function>. A region is destroyed
      with <function>gdk_region_destroy()</function>.
    </para>
    <para>
      Various tests on a region can be made with
      <function>gdk_region_get_clipbox()</function>,
      <function>gdk_region_empty()</function>,
      <function>gdk_region_equal()</function>,
      <function>gdk_region_point_in()</function> and
      <function>gdk_region_rect_in()</function>.
    </para>
    <para>
      An existing region can be modified with the functions
      <function>gdk_region_offset()</function> and
      <function>gdk_region_shrink()</function> (this function scales
      the region so it is an amount <parameter>dx</parameter> smaller
      in the x direction and an amount <parameter>dy</parameter>
      smaller in the y direction).
    </para>
    <para>
      Finally, various combinations of existing regions can be taken
      with the functions <function>gdk_regions_intersect()</function>,
      <function>gdk_regions_union()</function>,
      <function>gdk_regions_subtract()</function>, and
      <function>gdk_regions_xor()</function>. All of these functions
      take two regions as arguments and return a new region which is
      the combination of the two given regions under the specified
      operator. The <firstterm>intersection</firstterm> of two regions
      is all points in both regions.  The <firstterm>union</firstterm>
      of two regions is all the points that are in one region and not
      the other.  The <firstterm>xor</firstterm> (exclusive or) of two
      regions is all points that are in one region, but not
      both. <function>gdk_regions_subtract()</function> returns all
      points in the first region but not in the second.  All of these
      functions return a newly allocated region.
    </para>
    <para>
      The function <function>gdk_region_union_with_rect()</function>
      returns the union of a region with a given rectangle.
    </para>
    <para>
      Once we have a region, we can perform computations with the
      region or we can set the region as a clip rectangle for
      a graphics context with
      <function>gdk_gc_set_clip_region()</function>.
    </para>
  </sect1>
  <sect1>
    <title>The Cursor</title>
    <para>
      You can set the mouse cursor for a particular window using the following
      function:

      <!-- begin_proto="gdkwindow.h#gdk_window_set_cursor" -->
<funcsynopsis>
  <funcdef>void <function>gdk_window_set_cursor</function></funcdef>
  <paramdef>GdkWindow *<parameter>window</parameter></paramdef>
  <paramdef>GdkCursor *<parameter>cursor</parameter></paramdef>
</funcsynopsis>
      <!-- end_proto -->

      The function takes a pointer to a GdkCursor as an
      argument. First, you must create the cursor. The simplest way to
      do this is to use one of the predefined cursors identified by
      the GdkCursorType enumeration, with the following function:

<!-- begin_proto="gdkcursor.h#gdk_cursor_new" -->
<funcsynopsis>
  <funcdef>GdkCursor *<function>gdk_cursor_new</function></funcdef>
  <paramdef>GdkCursorType <parameter>cursor_type</parameter></paramdef>
</funcsynopsis>
<!-- end_proto -->

      You may also define your own cursors using
      <function>gdk_cursor_new_from_pixmap()</function>.  
      To change the cursor for our drawing area in Scribble from the
      standard cursor to a pencil, we use:

      <programlisting>
	<!-- begin_listing="scribble3.c#cursor" -->
  gtk_widget_realize (drawing_area);
  cursor = gdk_cursor_new (GDK_PENCIL);
  gdk_window_set_cursor (drawing_area-&gt;window, cursor);
  
  gdk_cursor_unref (cursor);
	<!-- end_listing -->
      </programlisting>
      
      Since X will remember that the cursor is in use for
      our window and keep a copy around, we don't need to
      keep a copy around, and immediately call:

<!-- begin_proto="gdkcursor.h#gdk_cursor_unref" -->
<funcsynopsis>
  <funcdef>void <function>gdk_cursor_unref</function></funcdef>
  <paramdef>GdkCursor *<parameter>cursor</parameter></paramdef>
</funcsynopsis>
<!-- end_proto -->
    </para>
  </sect1>
</chapter>

<!-- Local Variables: -->
<!-- sgml-parent-document: ("main.sgml" "part" "chapter")  -->
<!-- End: -->
